//
//  DatabaseManager.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 13/01/20.
//

import Foundation
import FMDB
final class DatabaseManager {

        static let databaseFileName = "Expense.db"
        static var database:FMDatabase!
        static let shared: DatabaseManager = {
        let instance = DatabaseManager()
            return instance
            }()
    
        func createDatabse()  {
     
                let bundlePath = Bundle.main.path(forResource: "Expense", ofType: ".db")
                print(bundlePath ?? "", "\n") //prints the correct path
                let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
                let fileManager = FileManager.default
                let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("Expense.db")
                let fullDestPathString = fullDestPath!.path
    
                if fileManager.fileExists(atPath: fullDestPathString) {
                        print("File is available")
                        DatabaseManager.database = FMDatabase(path: fullDestPathString)
                        openDataBase()
                        print(fullDestPathString)
                }else{
                do{
                    try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
                    if fileManager.fileExists(atPath: fullDestPathString) {
                    DatabaseManager.database = FMDatabase(path: fullDestPathString)
                    openDataBase()
                    addCategoriesDefault()
                    addAccountDefault()
                    print("File is copy")
                    }else {
                    print("File is not copy")
                    }
                }catch{
                    print("\n")
                    print(error)
                }
        }
    }
    
    func openDataBase() {
        if DatabaseManager.database != nil {
            DatabaseManager.database.open()
            //deleteTran()
        }else {
            DatabaseManager.shared.createDatabse()
        }
    }
    
    func closeDataBase() {
        if DatabaseManager.database != nil {
            DatabaseManager.database.close()
        }else {
            
        }
    }
    
    func addCategory(withParametrs parametrs: [String:Any],
                    completion:@escaping (Bool,String)->()) {
        if DatabaseManager.database != nil {
             DatabaseManager.database.executeUpdate("INSERT INTO tbl_category (name,icn_name,type,chart_color) VALUES (:name ,:icn_name ,:type ,:chart_color)", withParameterDictionary: parametrs)
            completion(true,"Add data")
        }else {
            completion(false, "Databse close")
        }
    }
   
    func UpdateCategory(withParametrs parametrs: [String:Any],strId:String,
                    completion:@escaping (Bool,String)->()) {
        
        if DatabaseManager.database != nil {
            let strName = parametrs["name"] as! String
            let icn_name = parametrs["icn_name"] as! String
            let type = parametrs["type"]!
            let chart_color = parametrs["chart_color"] as! String
            let query = "update tbl_category set \("name")=?, \("icn_name")=?, \("type")=?, \("chart_color")=? where id =?"
            
            do {
                try DatabaseManager.database.executeUpdate(query, values: [strName, icn_name, type,chart_color,strId])
                 completion(true,"Add data")
            }
            catch {
                print(error.localizedDescription)
                completion(false, "Databse close")
            }
            
        }
    }
    func addAccount(withParametrs parametrs: [String:Any],
                    completion:@escaping (Bool,String)->()) {
        if DatabaseManager.database != nil {
            
           
            
             DatabaseManager.database.executeUpdate("INSERT INTO tbl_account (name,chart_color,hide_futuretra,time_period,is_carryoveron,is_positiveonly,is_budgeton,amount,is_includeincome) VALUES (:name ,:chart_color ,:hide_futuretra ,:time_period ,:is_carryoveron ,:is_positiveonly ,:is_budgeton ,:amount ,:is_includeincome)", withParameterDictionary: parametrs)
            completion(true,"Add data")
        }else {
            completion(false, "Databse close")
        }
    }
   
    func addTransaction(withParametrs parametrs: [String:Any],
                       completion:@escaping (Bool,String)->()) {
           if DatabaseManager.database != nil {
                DatabaseManager.database.executeUpdate("INSERT INTO tbl_transaction (cat_id,acc_id,transaction_date,amount,is_repeating,every,repeating,end_date,notes,type,par_tra_id,cat_name,icn_name) VALUES (:cat_id ,:acc_id ,:transaction_date ,:amount ,:is_repeating ,:every ,:repeating ,:end_date ,:notes ,:type ,:par_tra_id ,:cat_name ,:icn_name)", withParameterDictionary: parametrs)
               completion(true,"Add data")
           }else {
               completion(false, "Databse close")
           }
       }
//UpdateTransactionSpecificValue
    func UpdateTransactionRepeatingSpecificValue(withParametrs parametrs: [String:Any],strId:String,
                       completion:@escaping (Bool,String)->()) {
           
//["cat_id":cat_id,"acc_id":accID,"amount":intAmount!,"notes":self.notes,"type":type,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
        if DatabaseManager.database != nil {
                       let cat_id = parametrs["cat_id"]!
                       let acc_id = parametrs["acc_id"]!
                       
                       let amount = parametrs["amount"]!
                      
                       let notes = parametrs["notes"]!
                       let type = parametrs["type"]!
                       
                       let cat_name = parametrs["cat_name"]!
                       let icn_name = parametrs["icn_name"]!
                   
                   
                      let query = "update tbl_transaction set \("cat_id")=?, \("acc_id")=?, \("amount")=?, \("notes")=?, \("type")=?, \("cat_name")=?, \("icn_name")=? where par_tra_id =?"
                      
                      do {
                          try DatabaseManager.database.executeUpdate(query, values: [cat_id, acc_id,amount,notes,type,cat_name,icn_name,strId])
                           completion(true,"Add data")
                      }
                      catch {
                          print(error.localizedDescription)
                          completion(false, "Databse close")
                      }
                      
                  }
       }
    func UpdateTransactionSpecificValue(withParametrs parametrs: [String:Any],strId:String,
                           completion:@escaping (Bool,String)->()) {
               
    //["cat_id":cat_id,"acc_id":accID,"amount":intAmount!,"notes":self.notes,"type":type,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
            if DatabaseManager.database != nil {
                           let cat_id = parametrs["cat_id"]!
                           let acc_id = parametrs["acc_id"]!
                           
                           let amount = parametrs["amount"]!
                          
                           let notes = parametrs["notes"]!
                           let type = parametrs["type"]!
                           
                           let cat_name = parametrs["cat_name"]!
                           let icn_name = parametrs["icn_name"]!
                       
                       
                          let query = "update tbl_transaction set \("cat_id")=?, \("acc_id")=?, \("amount")=?, \("notes")=?, \("type")=?, \("cat_name")=?, \("icn_name")=? where id =?"
                          
                          do {
                              try DatabaseManager.database.executeUpdate(query, values: [cat_id, acc_id,amount,notes,type,cat_name,icn_name,strId])
                               completion(true,"Add data")
                          }
                          catch {
                              print(error.localizedDescription)
                              completion(false, "Databse close")
                          }
                          
                      }
           }
    func UpdateTransaction(withParametrs parametrs: [String:Any],strId:String,
                    completion:@escaping (Bool,String)->()) {
        

     if DatabaseManager.database != nil {
                    let cat_id = parametrs["cat_id"]!
                    let acc_id = parametrs["acc_id"]!
                    let transaction_date = parametrs["transaction_date"]!
                    let amount = parametrs["amount"]!
                    let is_repeating = parametrs["is_repeating"]!
                    let every = parametrs["every"]!
                    let repeating = parametrs["repeating"]!
                    let end_date = parametrs["end_date"]!
                    let notes = parametrs["notes"]!
                    let type = parametrs["type"]!
                    let par_tra_id = parametrs["par_tra_id"]!
                    let cat_name = parametrs["cat_name"]!
                    let icn_name = parametrs["icn_name"]!
                
                
                   let query = "update tbl_transaction set \("cat_id")=?, \("acc_id")=?, \("transaction_date")=?, \("amount")=?, \("is_repeating")=?, \("every")=?, \("repeating")=?, \("end_date")=?, \("notes")=?, \("type")=?, \("par_tra_id")=?, \("cat_name")=?, \("icn_name")=? where id =?"
                   
                   do {
                       try DatabaseManager.database.executeUpdate(query, values: [cat_id, acc_id, transaction_date,amount,is_repeating,every,repeating,end_date,notes,type,par_tra_id,cat_name,icn_name,strId])
                        completion(true,"Add data")
                   }
                   catch {
                       print(error.localizedDescription)
                       completion(false, "Databse close")
                   }
                   
               }
    }

    func UpdateAccount(withParametrs parametrs: [String:Any],strId:String,
                           completion:@escaping (Bool,String)->()) {
   
        
        let name = parametrs["name"]!
         let chart_color = parametrs["chart_color"] ?? ""
         let hide_futuretra = parametrs["hide_futuretra"]!
         let time_period = parametrs["time_period"]!
         let is_carryoveron = parametrs["is_carryoveron"]!
         let is_positiveonly = parametrs["is_positiveonly"]!
         let is_budgeton = parametrs["is_budgeton"]!
         let amount = parametrs["amount"]!
         let is_includeincome = parametrs["is_includeincome"]!
       
         let query = "update tbl_account set \("name")=?, \("chart_color")=?, \("hide_futuretra")=?, \("time_period")=?, \("is_carryoveron")=?, \("is_positiveonly")=?, \("is_budgeton")=?, \("amount")=?, \("is_includeincome")=? where id =?"
        
        do {
            try DatabaseManager.database.executeUpdate(query, values: [name,chart_color,hide_futuretra,time_period,is_carryoveron,is_positiveonly,is_budgeton,amount,is_includeincome,strId])
             completion(true,"Add data")
        }
        catch {
            print(error.localizedDescription)
            completion(false, "Databse close")
        }
    }
    
    func UpdateRepeatingTransaction(withParametrs parametrs: [String:Any],strId:String,
                    completion:@escaping (Bool,String)->()) {
        
        if DatabaseManager.database != nil {
             let cat_id = parametrs["cat_id"]!
             let acc_id = parametrs["acc_id"]!
             //let transaction_date = parametrs["transaction_date"]!
             let amount = parametrs["amount"]!
             let is_repeating = parametrs["is_repeating"]!
             let every = parametrs["every"]!
             let repeating = parametrs["repeating"]!
             let end_date = parametrs["end_date"]!
             let notes = parametrs["notes"]!
             let type = parametrs["type"]!
             let par_tra_id = parametrs["par_tra_id"]!
             let cat_name = parametrs["cat_name"]!
             let icn_name = parametrs["icn_name"]!
         
         
            let query = "update tbl_transaction set \("cat_id")=?, \("acc_id")=?, \("amount")=?, \("is_repeating")=?, \("every")=?, \("repeating")=?, \("end_date")=?, \("notes")=?, \("type")=?, \("par_tra_id")=?, \("cat_name")=?, \("icn_name")=? where par_tra_id =?"
            
            do {
                try DatabaseManager.database.executeUpdate(query, values: [cat_id, acc_id,amount,is_repeating,every,repeating,end_date,notes,type,par_tra_id,cat_name,icn_name,strId])
                 completion(true,"Add data")
            }
            catch {
                print(error.localizedDescription)
                completion(false, "Databse close")
            }
            
        }
    }
    func deleteTran(){
        let query = "delete from tbl_transaction"
         let values = [Any]()
        try! DatabaseManager.database.executeQuery(query, values: values)
    }
    func deleteRepeatingTransaction(withParametrs strID:String,
                     completion:@escaping (Bool,String)->()) {
        let query = "delete from tbl_transaction where par_tra_id == " + strID//"\()"
        if DatabaseManager.database != nil {
            do {
                let values = [Any]()
                let data = DatabaseManager.database.executeStatements(query)
                
               // try DatabaseManager.database.executeUpdate(query, values: [strID])
//               let data =  try DatabaseManager.database.executeQuery(query, values:values)
//                while data.next() {
//                    print(data.string(forColumn: "par_tra_id"))
//                }
                if data == true {
                    completion(true,"Add data")
                }else {
                    completion(false,"Add data")
                }
                
            }
            catch {
                print(error.localizedDescription)
                completion(false, "Databse close")
            }
        }
    }
    func deleteTransaction(withParametrs strID:String,
                         completion:@escaping (Bool,String)->()) {
            let query = "delete from tbl_transaction where id =? " //+ "\(strID)"
            if DatabaseManager.database != nil {
                do {
                    let values = [Any]()
                    try DatabaseManager.database.executeUpdate(query, values: [strID])
   
                    completion(true,"Add data")
                }
                catch {
                    print(error.localizedDescription)
                    completion(false, "Databse close")
                }
            }
        }
    func addBudget(withParametrs parametrs: [String:Any],
                    completion:@escaping (Bool,String)->()) {
        if DatabaseManager.database != nil {
             DatabaseManager.database.executeUpdate("INSERT INTO tbl_budget (id,acc_id,amount,is_includeincome) VALUES (:id ,:acc_id ,:amount ,:is_includeincome)", withParameterDictionary: parametrs)
            completion(true,"Add data")
        }else {
            completion(false, "Databse close")
        }
    }
    
    func addCarryover(withParametrs parametrs: [String:Any],
                       completion:@escaping (Bool,String)->()) {
           if DatabaseManager.database != nil {
                DatabaseManager.database.executeUpdate("INSERT INTO tbl_carryover (id,acc_id,is_on,is_positiveonly) VALUES (:id ,:acc_id ,:is_on ,:is_positiveonly)", withParameterDictionary: parametrs)
           
               completion(true,"Add data")
           }else {
               completion(false, "Databse close")
           }
       }
    
    func checkCategoryData(withParametrs parametrs: [String:Any],strCatname:String,
                     completion:@escaping (Bool,String,Int)->()) {
        
        let query = "select count(*) as cat_id from tbl_transaction WHERE cat_id == " + "\(strCatname)"
        if DatabaseManager.database != nil {
            do {
                let values = [Any]()
               let data =  try DatabaseManager.database.executeQuery(query, values:values)
                while data.next() {
                    let count = data.int(forColumn: "cat_id")
                    completion(true,"Add data",Int(count))
                }
               
            }
            catch {
                print(error.localizedDescription)
                completion(false, "Databse close",1)
            }
        }else {
            completion(false, "Databse close",1)
        }
    }
    func checkAccountData(withParametrs parametrs: [String:Any],strCatname:String,
                     completion:@escaping (Bool,String,Int)->()) {
        
        let query = "select count(*) as acc_id from tbl_transaction WHERE acc_id == " + "\(strCatname)"
        if DatabaseManager.database != nil {
            do {
                let values = [Any]()
               let data =  try DatabaseManager.database.executeQuery(query, values:values)
                while data.next() {
                    let count = data.int(forColumn: "acc_id")
                    completion(true,"Add data",Int(count))
                }
               
            }
            catch {
                print(error.localizedDescription)
                completion(false, "Databse close",1)
            }
        }else {
            completion(false, "Databse close",1)
        }
    }
    func deleteCategory(withParametrs strID:String,
                     completion:@escaping (Bool,String)->()) {
        let query = "delete from tbl_category where id == '\(strID)'"
        if DatabaseManager.database != nil {
            do {
                let values = [Any]()
               let data =  try DatabaseManager.database.executeQuery(query, values:values)
                completion(true,"Add data")
            }
            catch {
                print(error.localizedDescription)
                completion(false, "Databse close")
            }
        }
    }
    func deleteAccount(withParametrs strID:String,
                        completion:@escaping (Bool,String)->()) {
//           let query = "delete from tbl_account where id == '\(strID)'" //+
//           if DatabaseManager.database != nil {
//               do {
//                   let values = [Any]()
//                  let data =  try DatabaseManager.database.executeQuery(query, values:values)
//                   completion(true,"Add data")
//               }
//               catch {
//                   print(error.localizedDescription)
//                   completion(false, "Databse close")
//               }
//           }
        
        let query = "delete from tbl_account where id =? " //+ "\(strID)"
                  if DatabaseManager.database != nil {
                      do {
                         
                          try DatabaseManager.database.executeUpdate(query, values: [strID])
         
                          completion(true,"Add data")
                      }
                      catch {
                          print(error.localizedDescription)
                          completion(false, "Databse close")
                     
                    }
        }
    }
    //MARK: get Data
    func getLastTransaction(withParametrs strDate:String,
                    completion:@escaping (Bool,String,[TransactionData])->()) {
        if DatabaseManager.database != nil {
             let select = "SELECT * FROM tbl_transaction where transaction_date == '\(strDate)'"
           
            if DatabaseManager.database != nil {
                let values = [Any]()
                
                let data = try! DatabaseManager.database.executeQuery(select, values: values)
                var arrTraData:[TransactionData] = []
                while data.next() {
                   
                    
                    let strId = data.string(forColumn: "id") ?? ""
                    let strCatId = data.string(forColumn: "cat_id") ?? ""
                    let strAccId = data.string(forColumn: "acc_id") ?? ""
                    let traData = data.string(forColumn: "transaction_date") ?? ""
                    let amount = data.double(forColumn: "amount")
                    let isRepeating = data.int(forColumn: "is_repeating")
                    let every = data.int(forColumn: "every")
                    let repeating = data.int(forColumn: "repeating")
                    let end_date = data.string(forColumn: "end_date") ?? ""
                    let notes = data.string(forColumn: "notes") ?? ""
                    let type = data.int(forColumn: "type")
                    let parentId = data.string(forColumn: "par_tra_id") ?? ""
                    let cat_name = data.string(forColumn: "cat_name") ?? ""
                    let icn_name = data.string(forColumn: "icn_name") ?? ""
                    
                    let obj = TransactionData(strId: strId, strCatID: strCatId, strAccId: strAccId, doTransactioDate: traData, doAmount: amount, isRepeating: Int(isRepeating), intEvery: Int(every), intRepeating: Int(repeating), doEndDate: end_date, strNotes: notes, intType: Int(type), intParentid: parentId, strCatName: cat_name, strCatIcon: icn_name)
                    arrTraData.append(obj)
                    
                }
                 completion(true,"Add data",arrTraData)
            }else {
                completion(false, "Databse close",[])
                
            }
        }else {
            completion(false, "Databse close",[])
        }
    }
    func getCategory(withParametrs parametrs: [String:Any],
                          completion:@escaping (Bool,String,[CategoryData])->()) {
              if DatabaseManager.database != nil {
                    let select = "SELECT * FROM tbl_category"
                if DatabaseManager.database != nil {
                    var arrCatData:[CategoryData] = []
                    let values = [Any]()
                    let data = try! DatabaseManager.database.executeQuery(select, values: values)
                    //print(data.resultDictionary)
                    while data.next() {
                       
                        let strID = data.string(forColumn: "id")
                        let strName = data.string(forColumn: "name")  ?? ""
                        let strIcnName = data.string(forColumn: "icn_name") ?? ""
                        let intType = data.int(forColumn: "type")
                        let strColor = data.string(forColumn: "chart_color") ?? ""
                        let obj = CategoryData(strId: strID, strName: strName, strIconName: strIcnName, intType: Int(intType), strChartColor: strColor)
                        arrCatData.append(obj)
                        
                    }
                    completion(true,"Add data",arrCatData)
                }else {
                    completion(false,"Databse close",[])
                }
                  
              }else {
                  completion(false, "Databse close",[])
              }
    }
    func getExpenseCategory(withParametrs parametrs: [String:Any],
                          completion:@escaping (Bool,String,[CategoryData])->()) {
              if DatabaseManager.database != nil {
                    let select = "SELECT * FROM tbl_category WHERE type = 1"
                if DatabaseManager.database != nil {
                    var arrCatData:[CategoryData] = []
                    let values = [Any]()
                    let data = try! DatabaseManager.database.executeQuery(select, values: values)
                    //print(data.resultDictionary)
                    while data.next() {
                       
                        let strID = data.string(forColumn: "id")
                        let strName = data.string(forColumn: "name")  ?? ""
                        let strIcnName = data.string(forColumn: "icn_name") ?? ""
                        let intType = data.int(forColumn: "type")
                        let strColor = data.string(forColumn: "chart_color") ?? ""
                        let obj = CategoryData(strId: strID, strName: strName, strIconName: strIcnName, intType: Int(intType), strChartColor: strColor)
                        arrCatData.append(obj)
                        
                    }
                    completion(true,"Add data",arrCatData)
                }else {
                    completion(false,"Databse close",[])
                }
                  
              }else {
                  completion(false, "Databse close",[])
              }
    }
    func getIncomeCategory(withParametrs parametrs: [String:Any],
                          completion:@escaping (Bool,String,[CategoryData])->()) {
              if DatabaseManager.database != nil {
                    let select = "SELECT * FROM tbl_category WHERE type = 2"
                if DatabaseManager.database != nil {
                    var arrCatData:[CategoryData] = []
                    let values = [Any]()
                    let data = try! DatabaseManager.database.executeQuery(select, values: values)
                    //print(data.resultDictionary)
                    while data.next() {
                       
                        let strID = data.string(forColumn: "id")
                        let strName = data.string(forColumn: "name")  ?? ""
                        let strIcnName = data.string(forColumn: "icn_name") ?? ""
                        let intType = data.int(forColumn: "type")
                        let strColor = data.string(forColumn: "chart_color") ?? ""
                        let obj = CategoryData(strId: strID, strName: strName, strIconName: strIcnName, intType: Int(intType), strChartColor: strColor)
                        arrCatData.append(obj)
                        
                    }
                    completion(true,"Add data",arrCatData)
                }else {
                    completion(false,"Databse close",[])
                }
                  
              }else {
                  completion(false, "Databse close",[])
              }
    }
    
    func getAccount(withParametrs parametrs: [String:Any],
                    completion:@escaping (Bool,String,[AccountData])->()) {
        if DatabaseManager.database != nil {
             let select = "SELECT * FROM tbl_account"
            
            if DatabaseManager.database != nil {
                var arrAccData:[AccountData] = []
                let values = [Any]()
                let data = try! DatabaseManager.database.executeQuery(select, values: values)
                
                while data.next() {
                 
                    let strId = data.string(forColumn: "id") ?? ""
                    let strName = data.string(forColumn: "name") ?? ""
                    let strChartColor = data.string(forColumn: "chart_color") ?? ""
                    let isHide = data.int(forColumn: "hide_futuretra")
                    let intTimePeriod = data.int(forColumn: "time_period")
                    let isCarryoveron = data.int(forColumn: "is_carryoveron")
                    let isPositive = data.int(forColumn: "is_positiveonly")
                    let isBudgeton = data.int(forColumn: "is_budgeton")
                    let budgetAmount = data.double(forColumn: "amount")
                    let isIncludeincome = data.int(forColumn: "is_includeincome")
                    
                    let obj = AccountData(strId: strId, strName: strName, strChartColor: strChartColor, intHideFuturetra: Int(isHide), intTimePeriod: Int(intTimePeriod), intIsCarryoveron: Int(isCarryoveron), intIsPositiveonly: Int(isPositive), intIsBudgeton: Int(isBudgeton), doAmount: budgetAmount, intIsIncludeincome: Int(isIncludeincome))
                    arrAccData.append(obj)
                }
                 completion(true, "Databse close",arrAccData)
            }
        }else {
            completion(false, "Databse close",[])
        }
    }
    func getTransaction(withParametrs parametrs: [String:Any],
                    completion:@escaping (Bool,String,[TransactionData])->()) {
        if DatabaseManager.database != nil {
             let select = "SELECT * FROM tbl_transaction"
           
            if DatabaseManager.database != nil {
                let values = [Any]()
                
                let data = try! DatabaseManager.database.executeQuery(select, values: values)
                var arrTraData:[TransactionData] = []
                while data.next() {
                   
                    
                    let strId = data.string(forColumn: "id") ?? ""
                    let strCatId = data.string(forColumn: "cat_id") ?? ""
                    let strAccId = data.string(forColumn: "acc_id") ?? ""
                    let traData = data.string(forColumn: "transaction_date") ?? ""
                    let amount = data.double(forColumn: "amount")
                    let isRepeating = data.int(forColumn: "is_repeating")
                    let every = data.int(forColumn: "every")
                    let repeating = data.int(forColumn: "repeating")
                    let end_date = data.string(forColumn: "end_date") ?? ""
                    let notes = data.string(forColumn: "notes") ?? ""
                    let type = data.int(forColumn: "type")
                    let parentId = data.string(forColumn: "par_tra_id") ?? ""
                    let cat_name = data.string(forColumn: "cat_name") ?? ""
                    let icn_name = data.string(forColumn: "icn_name") ?? ""
                    
                    let obj = TransactionData(strId: strId, strCatID: strCatId, strAccId: strAccId, doTransactioDate: traData, doAmount: amount, isRepeating: Int(isRepeating), intEvery: Int(every), intRepeating: Int(repeating), doEndDate: end_date, strNotes: notes, intType: Int(type), intParentid: parentId, strCatName: cat_name, strCatIcon: icn_name)
                    arrTraData.append(obj)
                    
                }
                 completion(true,"Add data",arrTraData)
            }else {
                completion(false, "Databse close",[])
                
            }
        }else {
            completion(false, "Databse close",[])
        }
    }
    
    func getTransactionByCat(withParametrs catID:String,
                    completion:@escaping (Bool,String,[TransactionData])->()) {
        if DatabaseManager.database != nil {
             let select = "SELECT * FROM tbl_transaction WHERE cat_id = " + catID
           
            if DatabaseManager.database != nil {
                let values = [Any]()
                
                let data = try! DatabaseManager.database.executeQuery(select, values: values)
                var arrTraData:[TransactionData] = []
                while data.next() {
                   
                    
                    let strId = data.string(forColumn: "id") ?? ""
                    let strCatId = data.string(forColumn: "cat_id") ?? ""
                    let strAccId = data.string(forColumn: "acc_id") ?? ""
                    let traData = data.string(forColumn: "transaction_date") ?? ""
                    let amount = data.double(forColumn: "amount")
                    let isRepeating = data.int(forColumn: "is_repeating")
                    let every = data.int(forColumn: "every")
                    let repeating = data.int(forColumn: "repeating")
                    let end_date = data.string(forColumn: "end_date") ?? ""
                    let notes = data.string(forColumn: "notes") ?? ""
                    let type = data.int(forColumn: "type")
                    let parentId = data.string(forColumn: "par_tra_id") ?? ""
                     let cat_name = data.string(forColumn: "cat_name") ?? ""
                    let icn_name = data.string(forColumn: "icn_name") ?? ""
                    let obj = TransactionData(strId: strId, strCatID: strCatId, strAccId: strAccId, doTransactioDate: traData, doAmount: amount, isRepeating: Int(isRepeating), intEvery: Int(every), intRepeating: Int(repeating), doEndDate: end_date, strNotes: notes, intType: Int(type), intParentid: parentId, strCatName: cat_name, strCatIcon: icn_name)
                    arrTraData.append(obj)
                    
                }
                 completion(true,"Add data",arrTraData)
            }else {
                completion(false, "Databse close",[])
                
            }
        }else {
            completion(false, "Databse close",[])
        }
    }
    func getSingleTransaction(withParametrs catID:String,
                    completion:@escaping (Bool,String,[TransactionData])->()) {
        if DatabaseManager.database != nil {
             let select = "SELECT * FROM tbl_transaction WHERE id = " + catID
           
            if DatabaseManager.database != nil {
                let values = [Any]()
                
                let data = try! DatabaseManager.database.executeQuery(select, values: values)
                var arrTraData:[TransactionData] = []
                while data.next() {
                   
                    
                    let strId = data.string(forColumn: "id") ?? ""
                    let strCatId = data.string(forColumn: "cat_id") ?? ""
                    let strAccId = data.string(forColumn: "acc_id") ?? ""
                    let traData = data.string(forColumn: "transaction_date") ?? ""
                    let amount = data.double(forColumn: "amount")
                    let isRepeating = data.int(forColumn: "is_repeating")
                    let every = data.int(forColumn: "every")
                    let repeating = data.int(forColumn: "repeating")
                    let end_date = data.string(forColumn: "end_date") ?? ""
                    let notes = data.string(forColumn: "notes") ?? ""
                    let type = data.int(forColumn: "type")
                    let parentId = data.string(forColumn: "par_tra_id") ?? ""
                     let cat_name = data.string(forColumn: "cat_name") ?? ""
                    let icn_name = data.string(forColumn: "icn_name") ?? ""
                    let obj = TransactionData(strId: strId, strCatID: strCatId, strAccId: strAccId, doTransactioDate: traData, doAmount: amount, isRepeating: Int(isRepeating), intEvery: Int(every), intRepeating: Int(repeating), doEndDate: end_date, strNotes: notes, intType: Int(type), intParentid: parentId, strCatName: cat_name, strCatIcon: icn_name)
                    arrTraData.append(obj)
                    
                }
                 completion(true,"Add data",arrTraData)
            }else {
                completion(false, "Databse close",[])
                
            }
        }else {
            completion(false, "Databse close",[])
        }
    }
    func getTransactionType(withParametrs accId:String,strType:String,
                    completion:@escaping (Bool,String,[TransactionData])->()) {
        if DatabaseManager.database != nil {
            var select = ""
            //'\(server_project_id)' "
            if strType == "day" {
                  select = "SELECT tbl_transaction.*,tbl_category.name,tbl_category.icn_name FROM tbl_transaction LEFT JOIN tbl_category ON (tbl_category.id = tbl_transaction.cat_id) WHERE transaction_date == '\(accId)'"
                //"SELECT tbl_transaction.*,tbl_category.name,tbl_category.icn_name FROM tbl_transaction LEFT JOIN tbl_category ON (tbl_category.id = tbl_transaction.cat_id) WHERE transaction_date >= '\(strStartDate)' and transaction_date <= '\(strEndDate)'"
            }else {
                  select = "SELECT * FROM tbl_transaction WHERE acc_id == " + accId
            }
            
           
            if DatabaseManager.database != nil {
                let values = [Any]()
                //values.append(accId)
                
                let data = try! DatabaseManager.database.executeQuery(select, values: values)
                var arrTraData:[TransactionData] = []
                while data.next() {
                   
                    
                    let strId = data.string(forColumn: "id") ?? ""
                    let strCatId = data.string(forColumn: "cat_id") ?? ""
                    let strAccId = data.string(forColumn: "acc_id") ?? ""
                    let traData = data.string(forColumn: "transaction_date") ?? ""
                    let amount = data.double(forColumn: "amount")
                    let isRepeating = data.int(forColumn: "is_repeating")
                    let every = data.int(forColumn: "every")
                    let repeating = data.int(forColumn: "repeating")
                    let end_date = data.string(forColumn: "end_date") ?? ""
                    let notes = data.string(forColumn: "notes") ?? ""
                    let type = data.int(forColumn: "type")
                    let parentId = data.string(forColumn: "par_tra_id") ?? ""
                    let cat_name = data.string(forColumn: "cat_name") ?? ""
                    let icn_name = data.string(forColumn: "icn_name") ?? ""
                    let obj = TransactionData(strId: strId, strCatID: strCatId, strAccId: strAccId, doTransactioDate: traData, doAmount: amount, isRepeating: Int(isRepeating), intEvery: Int(every), intRepeating: Int(repeating), doEndDate: end_date, strNotes: notes, intType: Int(type), intParentid: parentId, strCatName: cat_name, strCatIcon: icn_name)
                    arrTraData.append(obj)
                    
                }
                 completion(true,"Add data",arrTraData)
            }else {
                completion(false, "Databse close",[])
                
            }
        }else {
            completion(false, "Databse close",[])
        }
    }
    
    func getTransactionByCalenderType(withParametrs strStartDate:String,strEndDate:String,
                       completion:@escaping (Bool,String,[TransactionData])->()) {
           if DatabaseManager.database != nil {
               var select = ""
                       //
                    select = "SELECT tbl_transaction.*,tbl_category.name,tbl_category.icn_name FROM tbl_transaction LEFT JOIN tbl_category ON (tbl_category.id = tbl_transaction.cat_id) WHERE transaction_date >= '\(strStartDate)' and transaction_date <= '\(strEndDate)'"
             
              // SELECT * FROM tbl_transaction JOIN tbl_category ON (tbl_category.id = tbl_transaction.cat_id)  WHERE transaction_date >= '2020-02-01' and transaction_date <= '2020-02-29'
            //SELECT tbl_transaction.*,tbl_category.name,tbl_category.icn_name
          //  FROM tbl_transaction LEFT JOIN tbl_category ON (tbl_category.id = tbl_transaction.cat_id)
            //    WHERE transaction_date >= '2020-02-01' and transaction_date <= '2020-02-29'
            
               if DatabaseManager.database != nil {
                   let values = [Any]()
                do {
                    let data = try DatabaseManager.database.executeQuery(select, values: values)
                    var arrTraData:[TransactionData] = []
                    while data.next() {
                       
                        
                        let strId = data.string(forColumn: "id") ?? ""
                        let strCatId = data.string(forColumn: "cat_id") ?? ""
                        let strAccId = data.string(forColumn: "acc_id") ?? ""
                        let traData = data.string(forColumn: "transaction_date") ?? ""
                        let amount = data.double(forColumn: "amount")
                        let isRepeating = data.int(forColumn: "is_repeating")
                        let every = data.int(forColumn: "every")
                        let repeating = data.int(forColumn: "repeating")
                        let end_date = data.string(forColumn: "end_date") ?? ""
                        let notes = data.string(forColumn: "notes") ?? ""
                        let type = data.int(forColumn: "type")
                        let parentId = data.string(forColumn: "par_tra_id") ?? ""
                        let cat_name = data.string(forColumn: "name") ?? ""
                      let icn_name = data.string(forColumn: "icn_name") ?? ""
                     let obj = TransactionData(strId: strId, strCatID: strCatId, strAccId: strAccId, doTransactioDate: traData, doAmount: amount, isRepeating: Int(isRepeating), intEvery: Int(every), intRepeating: Int(repeating), doEndDate: end_date, strNotes: notes, intType: Int(type), intParentid: parentId, strCatName: cat_name, strCatIcon: icn_name)
                        arrTraData.append(obj)
                        
                    }
                     
                     completion(true,"Add data",arrTraData)
                }catch {
                    print(error.localizedDescription)
                    completion(false, "Databse close",[])
                }
                   
                   
               }else {
                   completion(false, "Databse close",[])
                   
               }
           }else {
               completion(false, "Databse close",[])
           }
       }
    func getTotalExpense(withParametrs strStartDate:String,strEndDate:String,type:String,
                    completion:@escaping (Bool,String,Double)->()) {
        if DatabaseManager.database != nil {
            var select = ""
                    
                 select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date >= '\(strStartDate)' and transaction_date <= '\(strEndDate)' and type == " + type
          
           
            if DatabaseManager.database != nil {
                let values = [Any]()
                //values.append(accId)
                
                let data = try! DatabaseManager.database.executeQuery(select, values: values)
                var arrTraData:[TransactionData] = []
                var intAmount = 0.0
                while data.next() {
                    let amount:Double = data.double(forColumn: "sum(amount)")
                        intAmount = amount
                    
                }
                 completion(true,"Add data",intAmount)
            }else {
                completion(false, "Databse close",0.0)
                
            }
        }else {
            completion(false, "Databse close",0.0)
        }
    }
    func getTotalExpenseByAccount(withParametrs strStartDate:String,strEndDate:String,type:String,strAccId:String,
                    completion:@escaping (Bool,String,Double)->()) {
        if DatabaseManager.database != nil {
            var select = ""
                    
                 select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date >= '\(strStartDate)' and transaction_date <= '\(strEndDate)' and type == '\(type)' and acc_id == '\(strAccId)'"
          
           
            if DatabaseManager.database != nil {
                let values = [Any]()
                //values.append(accId)
                
                let data = try! DatabaseManager.database.executeQuery(select, values: values)
                var arrTraData:[TransactionData] = []
                var intAmount = 0.0
                while data.next() {
                    let amount:Double = data.double(forColumn: "sum(amount)")
                        intAmount = amount
                    
                }
                 completion(true,"Add data",intAmount)
            }else {
                completion(false, "Databse close",0.0)
                
            }
        }else {
            completion(false, "Databse close",0.0)
        }
    }
    
    func getTotalExpenseByCarryOver(withParametrs strStartDate:String,strEndDate:String,type:String,strAccId:String,
                       completion:@escaping (Bool,String,Double)->()) {
           if DatabaseManager.database != nil {
               var select = ""
                       
                    select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date <= '\(strEndDate)' and type == '\(type)' and acc_id == '\(strAccId)'"
             
              
               if DatabaseManager.database != nil {
                   let values = [Any]()
                   //values.append(accId)
                   
                   let data = try! DatabaseManager.database.executeQuery(select, values: values)
                   var arrTraData:[TransactionData] = []
                   var intAmount = 0.0
                   while data.next() {
                       let amount:Double = data.double(forColumn: "sum(amount)")
                           intAmount = amount
                   }
                    completion(true,"Add data",intAmount)
               }else {
                   completion(false, "Databse close",0.0)
                   
               }
           }else {
               completion(false, "Databse close",0.0)
           }
       }
    
    func getTotalExpenseOneDaybyAccount(withParametrs strStartDate:String,type:String,strAccId:String,
                       completion:@escaping (Bool,String,Double)->()) {
           if DatabaseManager.database != nil {
               var select = ""
                       
                    select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date == '\(strStartDate)' and type == '\(type)' and acc_id == '\(strAccId)'" //+ "\()"
             
              
               if DatabaseManager.database != nil {
                   let values = [Any]()
                   //values.append(accId)
                   
                   let data = try! DatabaseManager.database.executeQuery(select, values: values)
                   var arrTraData:[TransactionData] = []
                   var intAmount = 0.0
                   while data.next() {
                       let amount:Double = data.double(forColumn: "sum(amount)")
                           intAmount = amount
                   }
                    completion(true,"Add data",intAmount)
               }else {
                   completion(false, "Databse close",0.0)
                   
               }
           }else {
               completion(false, "Databse close",0.0)
           }
       }
    func getTotalExpenseOneDay(withParametrs strStartDate:String,type:String,
                    completion:@escaping (Bool,String,Double)->()) {
        if DatabaseManager.database != nil {
            var select = ""
                    
                 select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date == '\(strStartDate)' and type == " + type
          
           
            if DatabaseManager.database != nil {
                let values = [Any]()
                //values.append(accId)
                
                let data = try! DatabaseManager.database.executeQuery(select, values: values)
                var arrTraData:[TransactionData] = []
                var intAmount = 0.0
                while data.next() {
                    let amount:Double = data.double(forColumn: "sum(amount)")
                        intAmount = amount
                }
                 completion(true,"Add data",intAmount)
            }else {
                completion(false, "Databse close",0.0)
                
            }
        }else {
            completion(false, "Databse close",0.0)
        }
    }
    func getTotalExpenseOneDaybyCategory(withParametrs strStartDate:String,type:String,strAccId:String,
                       completion:@escaping (Bool,String,Double)->()) {
           if DatabaseManager.database != nil {
               var select = ""
                       
            if strAccId == "" {
                select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date == '\(strStartDate)' and cat_name == '\(type)'" 
            }else {
                select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date == '\(strStartDate)' and cat_name == '\(type)' and acc_id == '\(strAccId)'"
            }
                    
             
              
               if DatabaseManager.database != nil {
                   let values = [Any]()
                   //values.append(accId)
                do {
                    let data = try DatabaseManager.database.executeQuery(select, values: values)
                                      
                                      var intAmount = 0.0
                                      while data.next() {
                                          let amount:Double = data.double(forColumn: "sum(amount)")
                                              intAmount = amount
                                      }
                                       completion(true,"Add data",intAmount)
                }catch {
                    print(error.localizedDescription)
                    completion(false,error.localizedDescription,0.0)
                }
                  
               }else {
                   completion(false, "Databse close",0.0)
                   
               }
           }else {
               completion(false, "Databse close",0.0)
           }
       }
    func getTotalIncomeOneDaybyCategory(withParametrs strStartDate:String,type:String,strAccId:String,
                    completion:@escaping (Bool,String,Double)->()) {
        if DatabaseManager.database != nil {
            var select = ""
            if strAccId == "" {
                select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date == '\(strStartDate)' and cat_name == '\(type)'"
            }else {
                select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date == '\(strStartDate)' and cat_name == '\(type)' and acc_id == '\(strAccId)'"
            }
                 
          
           
            if DatabaseManager.database != nil {
                let values = [Any]()
                //values.append(accId)
                do {
                    let data = try DatabaseManager.database.executeQuery(select, values: values)
                                   
                        var intAmount = 0.0
                        while data.next() {
                        let amount:Double = data.double(forColumn: "sum(amount)")
                                    intAmount = amount
                        }
                        completion(true,"Add data",intAmount)
                }catch {
                    print(error.localizedDescription)
                    completion(false,error.localizedDescription,0.0)
                }
               
            }else {
                completion(false, "Databse close",0.0)
                
            }
        }else {
            completion(false, "Databse close",0.0)
        }
    }
    func getTotalExpenseByCategory(withParametrs strStartDate:String,strEndDate:String,type:String,accId:String,
                    completion:@escaping (Bool,String,Double)->()) {
        if DatabaseManager.database != nil {
            var select = ""
            if accId == "" {
                select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date >= '\(strStartDate)' and transaction_date <= '\(strEndDate)' and cat_name == '\(type)'" 
            }else {
                select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date >= '\(strStartDate)' and transaction_date <= '\(strEndDate)' and cat_name == '\(type)' and acc_id == '\(accId)'"
            }
                 //+  //+ type
          
           
            if DatabaseManager.database != nil {
                let values = [Any]()
                //values.append(accId)
                
                let data = try! DatabaseManager.database.executeQuery(select, values: values)
                var arrTraData:[TransactionData] = []
                var intAmount = 0.0
                while data.next() {
                    let amount:Double = data.double(forColumn: "sum(amount)")
                        intAmount = amount
                }
                 completion(true,"Add data",intAmount)
            }else {
                completion(false, "Databse close",0.0)
                
            }
        }else {
            completion(false, "Databse close",0.0)
        }
    }
    func getTotalIncomeByCategory(withParametrs strStartDate:String,strEndDate:String,type:String,strAccID:String,
                       completion:@escaping (Bool,String,Double)->()) {
           if DatabaseManager.database != nil {
               var select = ""
            if strAccID == "" {
                select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date >= '\(strStartDate)' and transaction_date <= '\(strEndDate)' and cat_name == '\(type)'"
            }else {
                
                select = "SELECT sum(amount) FROM tbl_transaction WHERE transaction_date >= '\(strStartDate)' and transaction_date <= '\(strEndDate)' and cat_name == '\(type)' and acc_id == '\(strAccID)'"
            }
                    
              
              
               if DatabaseManager.database != nil {
                   let values = [Any]()
                   //values.append(accId)
                   
                   let data = try! DatabaseManager.database.executeQuery(select, values: values)
                   var arrTraData:[TransactionData] = []
                   var intAmount = 0.0
                   while data.next() {
                       let amount:Double = data.double(forColumn: "sum(amount)")
                           intAmount = amount
                   }
                    completion(true,"Add data",intAmount)
               }else {
                   completion(false, "Databse close",0.0)
                   
               }
           }else {
               completion(false, "Databse close",0.0)
           }
       }

   func getTotalExapenseCategory(withParametrs strStartDate:String,strEndDate:String,type:String,

                    completion:@escaping (Bool,String,[String])->()) {
        if DatabaseManager.database != nil {
            var select = ""
                    
                 select = "SELECT DISTINCT cat_name FROM tbl_transaction WHERE transaction_date >= '\(strStartDate)' and transaction_date <= '\(strEndDate)' and type == '\(type)'"
          
           
            if DatabaseManager.database != nil {
                let values = [Any]()
                //values.append(accId)
                

                do {
                    let data = try DatabaseManager.database.executeQuery(select, values: values)
                    var arrTraData:[String] = []
                    
                    while data.next() {
                         let amount = data.string(forColumn: "cat_name") ?? ""
                        arrTraData.append(amount)
                    }
                     if arrTraData.count > 0 {
                        completion(true,"Add data",arrTraData)
                    }else {
                        completion(false,"Add data",arrTraData)
                    }
                }catch {
                    print(error.localizedDescription)
                    completion(false,"error.localizedDescription",[])
                }
                

            }else {
                completion(false, "Databse close",[])
                
            }
        }else {
            completion(false, "Databse close",[])
        }
    }
    func getTotalExpenseCategoryOneDay(withParametrs strStartDate:String,type:String,strAccID:String,
                    completion:@escaping (Bool,String,[String])->()) {
        if DatabaseManager.database != nil {
            var select = ""
                    //SELECT DISTINCT cat_name FROM tbl_transaction WHERE transaction_date >= '2051-01-01' and transaction_date <= '2051-01-31' and type == 1
            if strAccID == "" {
                 select = "SELECT DISTINCT cat_name FROM tbl_transaction WHERE transaction_date == '\(strStartDate)' and type == '\(type)'" 
            }else {
                 select = "SELECT DISTINCT cat_name FROM tbl_transaction WHERE transaction_date == '\(strStartDate)' and type == '\(type)' and acc_id == '\(strAccID)'"
            }
                
          
           
            if DatabaseManager.database != nil {
                let values = [Any]()
                //values.append(accId)

                do {
                    let data = try DatabaseManager.database.executeQuery(select, values: values)
                    var arrTraData:[String] = []
                    
                    while data.next() {
                         let amount = data.string(forColumn: "cat_name") ?? ""
                        arrTraData.append(amount)
                    }
                    if arrTraData.count > 0 {
                         completion(true,"Add data",arrTraData)
                    }else {
                         completion(false,"Add data",arrTraData)
                    }
                }catch {
                    print(error.localizedDescription)
                    completion(false,error.localizedDescription,[])
                }

            }else {
                completion(false, "Databse close",[])
                
            }
        }else {
            completion(false, "Databse close",[])
        }
    }

    func getTotalExpenseCategoryByaccountOneDay(withParametrs strStartDate:String,accId:String,type:String,
                       completion:@escaping (Bool,String,[String])->()) {
           if DatabaseManager.database != nil {
               var select = ""
                       //SELECT DISTINCT cat_name FROM tbl_transaction WHERE transaction_date >= '2051-01-01' and transaction_date <= '2051-01-31' and type == 1
                    select = "SELECT DISTINCT cat_name FROM tbl_transaction WHERE transaction_date == '\(strStartDate)' and acc_id == '\(accId)' and type == '\(type)'"
             
              
               if DatabaseManager.database != nil {
                   let values = [Any]()
                   //values.append(accId)
                   do {
                       let data = try DatabaseManager.database.executeQuery(select, values: values)
                       var arrTraData:[String] = []
                       
                       while data.next() {
                            let amount = data.string(forColumn: "cat_name") ?? ""
                           arrTraData.append(amount)
                       }
                       if arrTraData.count > 0 {
                            completion(true,"Add data",arrTraData)
                       }else {
                            completion(false,"Add data",arrTraData)
                       }
                   }catch {
                       print(error.localizedDescription)
                       completion(false,error.localizedDescription,[])
                   }
               }else {
                   completion(false, "Databse close",[])
                   
               }
           }else {
               completion(false, "Databse close",[])
           }
       }
    func getTotalExapenseCategoryByAccount(withParametrs strStartDate:String,strEndDate:String,accId:String,type:String,
                       completion:@escaping (Bool,String,[String])->()) {
           if DatabaseManager.database != nil {
               var select = ""
                       
                    select = "SELECT DISTINCT cat_name FROM tbl_transaction WHERE transaction_date >= '\(strStartDate)' and transaction_date <= '\(strEndDate)' and acc_id == '\(accId)' and type == '\(type)'"
             
              
               if DatabaseManager.database != nil {
                   let values = [Any]()
                   //values.append(accId)
                   
                   do {
                       let data = try DatabaseManager.database.executeQuery(select, values: values)
                       var arrTraData:[String] = []
                       
                       while data.next() {
                            let amount = data.string(forColumn: "cat_name") ?? ""
                           arrTraData.append(amount)
                       }
                        if arrTraData.count > 0 {
                           completion(true,"Add data",arrTraData)
                       }else {
                           completion(false,"Add data",arrTraData)
                       }
                   }catch {
                       print(error.localizedDescription)
                       completion(false,"error.localizedDescription",[])
                   }
                   
               }else {
                   completion(false, "Databse close",[])
                   
               }
           }else {
               completion(false, "Databse close",[])
           }
       }
//
//    func deleteTransaction(withParametrs transactionID:String,
//                                 completion:@escaping (Bool,String)->()) {
//        if DatabaseManager.database != nil {
//            let query = "delete from tbl_transaction where id =" + transactionID
//                 // try!DatabaseManager.database.executeUpdate(query, values: [transactionID])
//            DatabaseManager.database.executeStatements(query)
//                completion(true,"Delete")
//        }else {
//            completion(false, "Databse close")
//        }
//    }
    
    func addAccountDefault() {
        //id,name,chart_color,hide_futuretra,time_period,is_carryoveron,is_positiveonly,is_budgeton,amount,is_includeincome
        let arraAccountName = ["Default","Bank","Cash"]
        
        for i in arraAccountName {
            let param = ["name":i,"chart_color":"","hide_futuretra":0,"time_period":3,"is_carryoveron":0,"is_positiveonly":0,"is_budgeton":0,"amount":0,"is_includeincome":0] as [String : Any]
            addAccount(withParametrs: param) { (sucess, message) in
                if sucess == true {
                    print(message)
                }
            }
        }
        
        
    }
    func addCategoriesDefault() {
        //id,name,icn_name,type,chart_color
        let arrCateName = ["Clothes","Gifts","Holidays","Eating Out","Sports","Entertainment","General","Kids","Travel","Shopping","Fuel","Call"]
        let arrIcon = ["16","1","5","14","7","18","4","22","26","12","32","21","13","21"]
        let arrIcnName = ["Clothes","Gifts","Holidays","Eating Out","Sports","Entertainment","General","Kids","Travel","Shopping","Fuel","Call"]
        
        for i in 0..<arrIcnName.count{
            let param = ["name":arrIcnName[i],"icn_name":arrIcon[i],"type":1,"chart_color":""] as [String : Any]
            addCategory(withParametrs: param) { (success, message) in
                if success == true {
                    print(message)
                }
            }
        }
        let arrIncome = ["Salary","Transfer"]
        let arrIncomeIcn = ["13","35"]
        
        for i in 0..<arrIncome.count {
            let param = ["name":arrIncome[i],"icn_name":arrIncomeIcn[i],"type":2,"chart_color":""] as [String : Any]
            addCategory(withParametrs: param) { (success, message) in
                if success == true {
                    print(message)
                }
            }
        }
    }
    
}

