//
//  allKey.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 13/01/20.
//

import Foundation
import UIKit

//Category table field name
let strCatId = "id"
let strCatName = "name"
let strCatIcnname = "icn_name"
let strCatType = "type"
let strCatchartcolor = "chart_color"

// Account table  Field name
let strAccId = "id"
let strAccName = "name"
let strAccChartColor = "chart_color"
let strAccHideFuturetra = "hide_futuretra"
let strAccTimePeriod = "time_period"
let strAccIsCarryOn = "is_carryoveron"
let strAccIsPositiveonly = "is_positiveonly"
let strAccIsBudgeton = "is_budgeton"
let strAccBudgetAmount = "amount"
let strAccIsIncludeincome = "is_includeincome"



// Transction table  Field name
let strTraId = "id"
let strTraCatId = "cat_id"
let strTraAccId = "acc_id"
let strTraDate = "transaction_date"
let strTraAmount = "amount"
let strTraIsRepeating = "is_repeating"
let strTraEvery = "every"
let strTraRepeating = "repeating"
let strTraEndDate = "end_date"
let strTraNotes = "notes"
let strTraType = "type"
let strParentId = "par_tra_id"
 

// Carryover table  Field name
let strCarrId = "id"
let strCarrAccId = "acc_id"
let strCarrIsOn = "is_on"
let strCarrIsPositiveonly = "is_positiveonly"

// Budget table  Field name

let strBudId = "id"
let strBudAccID = "acc_id"
let strBudAmount = "amount"
let strBudIsIncludeincome = "is_includeincome"

var strCalenderType = "calenderType"
var strCurrentSortType = "sortType"
var strCurrentSelectCat = "selectCat"
var strCurrentSelectedAcoount = "slectedAcc"
let strCurrencySymbols = "currencySymbols"
let strAllAccountHide = "allAccountHide"
