//
//  IncomeCategoriesListCell.swift
//  Expenses
//
//  
//

import UIKit

class IncomeCategoriesListCell: UITableViewCell {

    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var icnCategories: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblLine.backgroundColor = catTextColor
        lblName.textColor = catTextColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
