//
//  OverviewIncomeCell.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 20/01/20.
//

import UIKit

class OverviewIncomeCell: UITableViewCell {

    @IBOutlet weak var expHeight: NSLayoutConstraint!
    @IBOutlet weak var incomeLine: UIView!
    @IBOutlet weak var expLine: UIView!
    @IBOutlet weak var expBackView: UIView!
    @IBOutlet weak var btnAddExpense: UIButton!
    @IBOutlet weak var lblExpAmount: UILabel!
    @IBOutlet weak var lblExpense: UILabel!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.textColor = titleGrayColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
