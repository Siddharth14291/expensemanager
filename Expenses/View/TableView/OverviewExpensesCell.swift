//
//  OverviewExpensesCell.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 20/01/20.
//

import UIKit

class OverviewExpensesCell: UITableViewCell {

    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var tblCategory: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblTitle.textColor = blackColor
        backView.backgroundColor = textWhiteColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setUi() {
    
    }
}
