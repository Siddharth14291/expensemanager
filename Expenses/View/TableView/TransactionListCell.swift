//
//  TransactionListCell.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 21/01/20.
//

import UIKit

class TransactionListCell: UITableViewCell {

    @IBOutlet weak var icnCat: UIImageView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCatName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblCatName.textColor = catGrayColor
        lblDate.textColor = dateColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
