//
//  CategoriesSelectionListCell.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 20/01/20.
//

import UIKit

class CategoriesSelectionListCell: UITableViewCell {

    @IBOutlet weak var icnCategories: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblName.textColor = catTextColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
