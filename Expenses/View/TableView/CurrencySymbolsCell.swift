//
//  CurrencySymbolsCell.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 01/02/20.
//

import UIKit

class CurrencySymbolsCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSymbol: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
