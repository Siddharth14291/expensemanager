//
//  TransactionHeaderCell.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 28/01/20.
//

import UIKit

class TransactionHeaderCell: UITableViewCell {

    @IBOutlet var lblExpense: UILabel!
    @IBOutlet var lblIncome: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
