//
//  ExpensesCategoriesListCell.swift
//  Expenses
//
//  
//

import UIKit

class ExpensesCategoriesListCell: UITableViewCell {

    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var icnCategories: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblName.textColor = catTextColor
        lblLine.backgroundColor = catTextColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
