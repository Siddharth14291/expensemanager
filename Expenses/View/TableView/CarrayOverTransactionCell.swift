//
//  CarrayOverTransactionCell.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 04/02/20.
//

import UIKit

class CarrayOverTransactionCell: UITableViewCell {

    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
