//
//  BudgetModeCell.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 03/02/20.
//

import UIKit

class BudgetModeCell: UITableViewCell {

    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
