//
//  OverviewIncomeExpenseCategoryCell.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 20/01/20.
//

import UIKit

class OverviewIncomeExpenseCategoryCell: UITableViewCell {

    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblCategoryName.textColor = catGrayColor
        backView.backgroundColor = UIColor.clear
        progress.tintColor = progressColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
