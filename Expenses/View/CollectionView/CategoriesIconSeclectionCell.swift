//
//  CategoriesIconSeclectionCell.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 21/01/20.
//

import UIKit

class CategoriesIconSeclectionCell: UICollectionViewCell {
    
    @IBOutlet var icnCategories: UIImageView!
}
