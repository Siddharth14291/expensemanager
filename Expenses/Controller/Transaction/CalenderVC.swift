//
//  CalenderVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 22/01/20.
//

import UIKit
import FSCalendar
@objc protocol SelectedDate : NSObjectProtocol
{
    @objc optional func getDate(_ APIDate:String,_ strDisplayDate:String,_ endDate:Date)
}

class CalenderVC: UIViewController,FSCalendarDelegate {

    @IBOutlet weak var calenderView: FSCalendar!
    var delegate:SelectedDate?
    var strSelectedDate = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = bgGrayColor
    
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if strSelectedDate != "" {
            let selectedDate = getStaringDate(strDate: strSelectedDate)
            //self.calenderView.currentPage = selectedDate
            //self.calenderView.setCurrentPage(selectedDate, animated: true)
            self.calenderView.select(selectedDate)
        }
       
    }
    
    @IBAction func btnCancelaction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
       // self.lblDate.text = "\(date)"
        
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
            print(date)
            
        }
        
        let databaseDate = dataBaseDate(intDate: date)
        let d = dataBaseDateNew(intDate:date)
        self.dismiss(animated: true, completion: nil)
    
        self.delegate?.getDate?(databaseDate, "\(date)",date)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
