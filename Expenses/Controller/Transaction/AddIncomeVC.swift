//
//  AddIncomeVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 20/01/20.
//

import UIKit
import XLPagerTabStrip

class AddIncomeVC: UIViewController,IndicatorInfoProvider {
    
    @IBOutlet var icnCategor: UIImageView!
    @IBOutlet var lblAccount: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var lblNotesTitle: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblRepeatDay: UILabel!
    @IBOutlet weak var lblRepeat: UILabel!
    @IBOutlet weak var lblRepeatingTitle: UILabel!
    @IBOutlet weak var lblCategories: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    var objSelectCategories:CategoryData!
    var arrAccountData:[AccountData] = []
    var strDiplayDate:String!
    var strAPIDate:String!
    var accountPicker = UIPickerView()
    var backView = UIView()
    var toolBar = UIToolbar()
    var selectedIndex = 0
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
     
             return IndicatorInfo(title: "Income")
     }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       setUI()
        getAccount()
         NotificationCenter.default.addObserver(self, selector: #selector(self.saveIncome), name: Notification.Name("saveIncome"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
            self.view.backgroundColor = bgGrayColor
        self.lblTitle.textColor = catTextColor
        self.lblNotes.textColor = catTextColor
        self.lblNotesTitle.textColor = catTextColor
        self.lblEndDate.textColor = catTextColor
        self.lblRepeatDay.textColor = catTextColor
        self.lblRepeat.textColor = catTextColor
        self.lblRepeatingTitle.textColor = catTextColor
        self.lblCategories.textColor = catTextColor
        self.lblAmount.textColor = catTextColor
        self.lblDate.textColor = catTextColor
    }
    func getAccount() {
        
        DatabaseManager.shared.getAccount(withParametrs: [:]) { (success, message, arrData) in
            if success == true {
                self.arrAccountData = arrData
                self.lblAccount.text = self.arrAccountData[self.selectedIndex].name!
            }else {
                
            }
        }
    }
    
    //MARK :-  Button Action
       
       @IBAction func btnSelectRepeataction(_ sender: UIButton) {
           
       }
       
       @IBAction func btnSelectDateaction(_ sender: UIButton) {
        
        
        let next = self.storyboard?.instantiateViewController(withIdentifier: "CalenderVC") as! CalenderVC
        next.delegate = self
        self.navigationController?.present(next, animated: true, completion: nil)

       }
       
       @IBAction func btnSelectCategoriesaction(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "CategoriesSelectionVC") as! CategoriesSelectionVC
        next.delegate = self
        next.isIncome = true
        self.navigationController?.pushViewController(next, animated: true)
       }
       
       
       @IBAction func btnSelectEndDateaction(_ sender: UIButton) {
        
       }
       
       @IBAction func btnNotesaction(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
               self.navigationController?.pushViewController(next, animated: true)
       }
    
    @IBAction func btnSelectAccountaction(_ sender: UIButton) {
        pickerAccount()
    }
    func pickerAccount(){
           accountPicker = UIPickerView.init()
           backView = UIView()
           backView.frame = self.view.bounds
           backView.backgroundColor = UIColor.black
           backView.alpha = 0.2
           //backView.isUserInteractionEnabled = false
           self.view.addSubview(backView)
           accountPicker.delegate = self
           accountPicker.backgroundColor = textWhiteColor
           accountPicker.setValue(UIColor.black, forKey: "textColor")
           accountPicker.autoresizingMask = .flexibleWidth
           accountPicker.contentMode = .center
       
           accountPicker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 250, width: UIScreen.main.bounds.size.width, height: 250)
           self.view.addSubview(accountPicker)

           toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 250, width: UIScreen.main.bounds.size.width, height: 40))
           let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
           toolBar.barStyle = .default
         //  toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
        //    toolBar.items = [UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(onDoneButtonTapped))]
           let btmDone = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onDoneButtonTapped))
           
          // let btnCancel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onCancelButtonTapped))
           toolBar.items = [btmDone]
           toolBar.backgroundColor = textWhiteColor
           self.view.addSubview(toolBar)
       }
       //MARK: ToolBar Done Button Tap Action
       @objc func onDoneButtonTapped() {
           
           backView.removeFromSuperview()
           toolBar.removeFromSuperview()
           accountPicker.removeFromSuperview()
       }
       @objc func onCancelButtonTapped() {
           
           backView.removeFromSuperview()
           toolBar.removeFromSuperview()
           accountPicker.removeFromSuperview()
       }
    @objc func saveIncome() {
     
            if self.txtAmount.text?.isValidString() == false {
                ShowAlert(title: "Expense", message: "Please enter amount", in: self)
            }else if self.lblCategories.text?.isValidString() == false {
                ShowAlert(title: "Expense", message: "Please select Categories", in: self)
            }else if self.strDiplayDate == nil {
                ShowAlert(title: "Expense", message: "Please select date", in: self)
            }else {

                let catId:Int = Int(self.objSelectCategories.id!)!
                let intAmount:Double = Double(self.txtAmount.text!)!
                let accId = self.arrAccountData[self.selectedIndex].id!
                let param = ["cat_id":catId,"acc_id":accId,"transaction_date":self.strAPIDate,"amount":intAmount,"is_repeating":0,"every":0,"repeating":0,"end_date":0.0,"notes":"","type":2,"par_tra_id":0,"cat_name":objSelectCategories.name!] as [String : Any]
         
                    print(param)
                DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                    if success == true {
                        print(message)
                    }else {
                        print(message)
                    }
                }
            }
        }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddIncomeVC:SelectedDate{
    
    func getDate(_ APIDate:String,_ strDisplayDate:String) {
        self.strAPIDate = APIDate
        
        self.strDiplayDate = dipayDate(strDate: APIDate)
        self.lblDate.text = self.strDiplayDate
    }
}

extension AddIncomeVC:SelectedCategories {
    
    func getCategories(_ objCategories: CategoryData) {
        self.objSelectCategories = objCategories
        self.lblCategories.text = objCategories.name!
    }
}
extension AddIncomeVC:UITextViewDelegate {
    
}
extension AddIncomeVC: UIPickerViewDataSource, UIPickerViewDelegate {

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       
            return self.arrAccountData.count//self.arrAccount.count
       
       
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       
            return self.arrAccountData[row].name!
        
        
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       
            self.lblAccount.text = self.arrAccountData[row].name
            self.selectedIndex = row
        
    }
}
