//
//  AddExpensesVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 20/01/20.
//

import UIKit
//import XLPagerTabStrip
class AddExpensesVC: UIViewController//,IndicatorInfoProvider
{
    
    @IBOutlet var icnCategory: UIImageView!
    @IBOutlet var txtCategories: UITextField!
    @IBOutlet var lineIncome: UIView!
    @IBOutlet var lineExp: UIView!
    @IBOutlet var segment: UISegmentedControl!
    @IBOutlet var lblAccount: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var lblNotesTitle: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblRepeatDay: UILabel!
    @IBOutlet weak var lblRepeat: UILabel!
    @IBOutlet weak var lblRepeatingTitle: UILabel!
    @IBOutlet weak var lblCategories: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet var btnClearEndDate: UIButton!
    var endDateRepate:Date!
    @IBOutlet var dayViewHeight: NSLayoutConstraint!
    @IBOutlet var endDateViewHeight: NSLayoutConstraint!
    @IBOutlet var endDateBackView: UIView!
    @IBOutlet var dayBackView: UIView!
    var arrLatsTransaction:[TransactionData] = []
    var isEnddate = false
    var strEndDate = ""
    var strApiEndDate = ""
    var isRepet = 0
    @IBOutlet var repeatSwitch: UISwitch!
    var strDay = "1"
    var strMonth = "Months"
    var arrDayMonts = ["Months","Week","Day"]
    var arrRepeateDay = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"]
    
    var objSelectCategories:CategoryData!
    var arrAccountData:[AccountData] = []
    var strDiplayDate:String!
    var strAPIDate:String!
    var accountPicker = UIPickerView()
    var backView = UIView()
    var toolBar = UIToolbar()
    var selectedIndex = 0
    var isIncome = false
    var notes = ""
    var isUpdate = false
    var objData:TransactionData!
    var objParentTransaction:TransactionData!
    var isAllUpdate = false
    @IBOutlet var btnDeleteTra: UIButton!
    //    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
//
//            return IndicatorInfo(title: "Expenses")
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
         self.txtAmount.resignFirstResponder()
         setUI()
        getAccount()
       
        //NotificationCenter.default.addObserver(self, selector: #selector(self.saveExpenses), name: Notification.Name("saveExpense"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    
    func setUI() {
            self.view.backgroundColor = bgGrayColor
      //  self.lblDate.isUserInteractionEnabled
        self.lblTitle.textColor = catTextColor
        self.lblNotes.textColor = catTextColor
        self.lblNotesTitle.textColor = catTextColor
        self.lblEndDate.textColor = catTextColor
        self.lblRepeatDay.textColor = catTextColor
        self.lblRepeat.textColor = catTextColor
        self.lblRepeatingTitle.textColor = catTextColor
        self.lblCategories.textColor = catTextColor
        self.lblAmount.textColor = catTextColor
        self.lblDate.textColor = catTextColor
        self.txtCategories.textColor = catTextColor
        self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.normal)
        let font = UIFont(name:"Verdana", size: 20.0)
        
        let attributes = ([NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.white])
        self.segment.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: UIControl.State.normal)
        self.segment.backgroundColor = UIColor.clear
        self.segment.tintColor = UIColor.clear
        self.lineExp.backgroundColor = UIColor.black
        self.lineIncome.backgroundColor = UIColor.black
        if isIncome == true {
            self.segment.selectedSegmentIndex = 1
            self.lineIncome.isHidden = false
            self.lineExp.isHidden = true
             self.lblTitle.text = "Income Detail"
        }else {
            self.segment.selectedSegmentIndex = 0
            self.lineExp.isHidden = false
            self.lineIncome.isHidden = true
            self.lblTitle.text = "Expense Detail"
        }
        
        
        if isUpdate == true {
            
            self.btnDeleteTra.isHidden = false
            self.btnDeleteTra.backgroundColor = expenseRed
            self.lblDate.text = dipayDate(strDate: self.objData.transaction_date)
            self.txtCategories.text = self.objData.cat_name!
            self.txtAmount.text = "\(self.objData.amount.clean)"
            self.strAPIDate = self.objData.transaction_date!
                self.isRepet = objData.is_repeating
            self.strDay = "\(objData.every!)"
            if objData.repeating == 0 {
                strMonth = "Month"
            }else if objData.repeating == 3 {
                 strMonth = "Months"
            }else if objData.repeating == 2 {
                strMonth = "Week"
            }else if objData.repeating == 1 {
                strMonth = "Day"
            }
            self.lblRepeatDay.text = self.strDay + " " + self.strMonth
            if objData.is_repeating == 1 {
                self.endDateBackView.isHidden = false
                self.endDateViewHeight.constant = 50
                self.dayBackView.isHidden = false
                self.dayViewHeight.constant = 50
                isRepet = 1
                self.repeatSwitch.isOn = true
            }else {
                
                self.endDateBackView.isHidden = true
                self.endDateViewHeight.constant = 0
                self.dayBackView.isHidden = true
                self.dayViewHeight.constant = 0
                isRepet = 0

            }
            if objData.end_date != "" {
                self.endDateRepate = getStaringDate(strDate:objData.end_date!)
                self.lblEndDate.text = dipayDate(strDate: objData.end_date!)
                self.strApiEndDate = dataBaseDate(intDate:endDateRepate)
                self.btnClearEndDate.isHidden = false
            }else {
                self.btnClearEndDate.isHidden = true
            }
            self.notes = objData.notes!
            self.lblNotes.text = self.notes
            let arrData = arrAccountData.filter { $0.id.lowercased().contains((objData.acc_id!.lowercased()))}
            if arrData.count > 0 {
                self.lblAccount.text = arrData[0].name!
            }
            ///self.lblAccount.text = arrData[0].name
            self.icnCategory.image = UIImage(named: objData.icn_name!)
            
        }else{
            self.btnClearEndDate.isHidden = true
             self.btnDeleteTra.isHidden = true
            let date = Date()
            self.strAPIDate = dataBaseDate(intDate: date)
            self.strDiplayDate = dipayDate(strDate: strAPIDate)
            self.lblDate.text = self.strDiplayDate
            self.lblCategories.text = "Categories"
            self.endDateBackView.isHidden = true
            self.endDateViewHeight.constant = 0
            self.dayBackView.isHidden = true
            self.dayViewHeight.constant = 0
        }

    }
    func getAccount() {
        
        DatabaseManager.shared.getAccount(withParametrs: [:]) { (success, message, arrData) in
            if success == true {
                self.arrAccountData = arrData
                self.lblAccount.text = self.arrAccountData[self.selectedIndex].name!
                if self.isUpdate == true {
                    let arrData = self.arrAccountData.filter { $0.id.lowercased().contains((self.objData.acc_id!.lowercased()))}
                    self.lblAccount.text = arrData[0].name!
                }else {
                     self.lblAccount.text = self.arrAccountData[self.selectedIndex].name!
                }
            }else {
                
            }
        }
    }
    
    
    //MARK :-  Button Action
    
    @IBAction func btnClearEndDateaction(_ sender: UIButton) {
        self.txtAmount.resignFirstResponder()
        self.strEndDate = ""
        self.lblEndDate.text = "End Date"
        self.endDateRepate = nil
        self.strApiEndDate = ""
    }
    
    @IBAction func btnDeleteTraaction(_ sender: UIButton) {
        self.txtAmount.resignFirstResponder()
        if objData.is_repeating == 1 {
            var trnId = ""
            if objData.par_tra_id == "" {
                trnId = objData.id!
            }else {
                trnId = objData.par_tra_id!
            }
            let alert = UIAlertController(title: "Query", message: "This is a 'Repeating' Transaction. Choose whether you want to delete just this occurrence occurrences from this date onwards, or all occurences in the series.", preferredStyle: UIAlertController.Style.alert)

                   // add the actions (buttons)
                   alert.addAction(UIAlertAction(title: "Delete This Occurrence", style: UIAlertAction.Style.default, handler:{ action in

                       // do something like...
                    var singleId = ""
                    if self.objData.par_tra_id == "" {
                        singleId = self.objData.id!
                    }else {
                        singleId = self.objData.id!
                    }
                      DatabaseManager.shared.deleteTransaction(withParametrs: singleId) { (success, message) in
                            if success == true {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }

                   }))

                   alert.addAction(UIAlertAction(title: "Delete Future Occurrence", style: UIAlertAction.Style.default, handler: { action in

                       // do something like...
                     self.deleteRepeating(strID: trnId)
                       self.navigationController?.popViewController(animated: true)

                   }))

                   alert.addAction(UIAlertAction(title: "Delete All Occurrence", style: UIAlertAction.Style.destructive, handler: { action in
                
                    DatabaseManager.shared.deleteTransaction(withParametrs: trnId) { (success, message) in
                        if success == true {
                            self.deleteRepeating(strID: trnId)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                       // do something like...
                       

                   }))

            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { action in

    
            

               }))
                   // show the alert
            self.present(alert, animated: true, completion: nil)
        }else {
            let alert = UIAlertController(title: "Query", message: "Your are about to delete transaction. Do you wish to continue?", preferredStyle: UIAlertController.Style.alert)

                   // add the actions (buttons)
                   alert.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.default, handler:{ action in

                       // do something like...
                    
                      DatabaseManager.shared.deleteTransaction(withParametrs: self.objData.id!) { (success, message) in
                            if success == true {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }

                   }))

                   alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { action in

                       // do something like...
                

                   }))



                   // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func repeatSwitchaction(_ sender: UISwitch) {
        self.txtAmount.resignFirstResponder()
        if sender.isOn == true {
            isRepet = 1
            self.endDateBackView.isHidden = false
            self.endDateViewHeight.constant = 50
            self.dayBackView.isHidden = false
            self.dayViewHeight.constant = 50
        }else {
            self.endDateBackView.isHidden = true
            self.endDateViewHeight.constant = 0
            self.dayBackView.isHidden = true
            self.dayViewHeight.constant = 0
            isRepet = 0
        }
    }
    
    @IBAction func segmentSelectionaction(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        self.txtAmount.resignFirstResponder()
        self.icnCategory.image = UIImage(named: "ExpenseCategoryIcon")
        if sender.selectedSegmentIndex == 0 {
            self.lineExp.isHidden = false
            self.lineIncome.isHidden = true
            self.isIncome = false
            self.lblTitle.text = "Expense Detail"
            if isUpdate == true {
                if objData.type == 1 {
                    self.txtCategories.text = ""
                    self.objSelectCategories = nil
                    //objData.cat_name!
                }else {
                    self.txtCategories.text = ""
                    self.objSelectCategories = nil
                }
            }else {
                // self.lblCategories.text = "Caltegories"
                 self.txtCategories.text = ""
                if self.objSelectCategories != nil {
                    self.objSelectCategories = nil
                }else {
                    
                }
            }
           
            //self.txtCategories.text = ""
            
        }else {
            self.lineIncome.isHidden = false
            self.lineExp.isHidden = true
            self.isIncome = true
            self.lblTitle.text = "Income Detail"
            self.lblCategories.text = "Caltegories"
            if isUpdate == true {
                if objData.type == 2 {
                    self.txtCategories.text = ""
                    self.objSelectCategories = nil//objData.cat_name!
                }else {
                    self.txtCategories.text = ""
                    self.objSelectCategories = nil
                }
            }else {
                // self.lblCategories.text = "Caltegories"
                 self.txtCategories.text = ""
                if self.objSelectCategories != nil {
                    self.objSelectCategories = nil
                }else {
                    
                }
            }
        }
       
    }
    @IBAction func btnBackaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveaction(_ sender: UIButton) {
        self.txtAmount.resignFirstResponder()
        if isUpdate == true {
            if txtCategories.text != self.objData.cat_name! {
                ShowAlert(title: "Expense", message: "Please select Categories", in: self)
            }else {
                if objData.par_tra_id == "0" {
                     UpdateTran()
                }else {
                    let alert = UIAlertController(title: "Query", message: "This is a 'Repeating' Transaction. Choose whether you want to Update just this occurrence, or occurrences from this date onwards", preferredStyle: UIAlertController.Style.alert)

                                   // add the actions (buttons)
                                 

                                   alert.addAction(UIAlertAction(title: "This Occurrence Only", style: UIAlertAction.Style.default, handler: { action in

                                       // do something like...
                                    self.isAllUpdate = false
                                    self.UpdateTran()
                                   }))

                                   alert.addAction(UIAlertAction(title: "All Future Occurrence", style: UIAlertAction.Style.destructive, handler: { action in

                                    self.isAllUpdate = true
                                    self.UpdateTran()
                                       // do something like...
                                       

                                   }))

                           
                                   // show the alert
                            self.present(alert, animated: true, completion: nil)
                }
            }
            
            
           
        }else {
            if self.isIncome == true {
                self.saveIncome()
            }else {
                self.saveExpenses()
            }
        }
        
    }
    
    @IBAction func btnSelectRepeataction(_ sender: UIButton) {
        self.txtAmount.resignFirstResponder()
        self.pickerAccount(tag: 2)
    }
    
    @IBAction func btnSelectDateaction(_ sender: UIButton) {
        self.txtAmount.resignFirstResponder()
        let next = self.storyboard?.instantiateViewController(withIdentifier: "CalenderVC") as! CalenderVC
        next.delegate = self
        next.strSelectedDate = self.strAPIDate
        self.navigationController?.present(next, animated: true, completion: nil)
    }
    
    @IBAction func btnSelectCategoriesaction(_ sender: UIButton) {
        self.txtAmount.resignFirstResponder()
         let next = self.storyboard?.instantiateViewController(withIdentifier: "CategoriesSelectionVC") as! CategoriesSelectionVC
        next.delegate = self
        if self.isIncome == true {
            next.isIncome = true
        }else {
            next.isIncome = false
        }
        if self.isUpdate == true {
            if self.objSelectCategories != nil {
                next.selectredID = self.objSelectCategories.id!
            }else {
                if self.txtCategories.text?.isEmpty == true {
                    
                }else {
                    next.selectredID = self.objData.cat_id!
                }
                
            }
        }else {
            if self.objSelectCategories != nil {
                next.selectredID = self.objSelectCategories.id!
            }else {
                
            }
        }
        //next.objCategories = self.objSelectCategories
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    @IBAction func btnSelectAccountaction(_ sender: UIButton) {
        self.txtAmount.resignFirstResponder()
        pickerAccount(tag: 1)
        
    }
    
    @IBAction func btnSelectEndDateaction(_ sender: UIButton) {
        self.txtAmount.resignFirstResponder()
        let next = self.storyboard?.instantiateViewController(withIdentifier: "CalenderVC") as! CalenderVC
        next.delegate = self
        self.isEnddate = true
        self.navigationController?.present(next, animated: true, completion: nil)
    }
    
    @IBAction func btnNotesaction(_ sender: UIButton) {
        self.txtAmount.resignFirstResponder()
        let next = self.storyboard?.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
        next.delegate = self
        next.strNotes = self.notes
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func pickerAccount(tag:Int){
        accountPicker = UIPickerView.init()
        backView = UIView()
        backView.frame = self.view.bounds
        backView.backgroundColor = UIColor.black
        backView.alpha = 0.2
       // backView.isUserInteractionEnabled = false
        self.view.addSubview(backView)
        accountPicker.delegate = self
        accountPicker.backgroundColor = textWhiteColor
        accountPicker.setValue(UIColor.black, forKey: "textColor")
        accountPicker.autoresizingMask = .flexibleWidth
        accountPicker.contentMode = .center
        accountPicker.tag = tag
        accountPicker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 250, width: UIScreen.main.bounds.size.width, height: 250)
        self.view.addSubview(accountPicker)

        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 250, width: UIScreen.main.bounds.size.width, height: 40))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
        toolBar.barStyle = .default
      //  toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
     //    toolBar.items = [UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(onDoneButtonTapped))]
        let btmDone = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onDoneButtonTapped))
        
       // let btnCancel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onCancelButtonTapped))
        toolBar.items = [btmDone]
        toolBar.backgroundColor = textWhiteColor
        self.view.addSubview(toolBar)
    }
    //MARK: ToolBar Done Button Tap Action
    @objc func onDoneButtonTapped() {
        
        if accountPicker.tag == 1 {
            
            let index:Int = accountPicker.selectedRow(inComponent: 0)
                self.lblAccount.text = self.arrAccountData[index].name
                if isUpdate == true {
                    self.objData.acc_id = self.arrAccountData[index].id!
                }
                self.selectedIndex = index
            
        }else if accountPicker.tag == 2 {
            let index:Int = accountPicker.selectedRow(inComponent: 0)
                self.strDay = self.arrRepeateDay[index]
            
            let index2:Int = accountPicker.selectedRow(inComponent: 1)
                self.strMonth = self.arrDayMonts[index2]
            
            self.lblRepeatDay.text = self.strDay + " " + self.strMonth
        }
        backView.removeFromSuperview()
        toolBar.removeFromSuperview()
        accountPicker.removeFromSuperview()
        
        
    }
    @objc func onCancelButtonTapped() {
        
        backView.removeFromSuperview()
        toolBar.removeFromSuperview()
        accountPicker.removeFromSuperview()
    }
    
    func saveExpenses() {
        
        if self.txtAmount.text?.isValidString() == false {
            ShowAlert(title: "Expense", message: "Please enter amount", in: self)
        }else if self.txtCategories.text?.isEmpty == true {
            ShowAlert(title: "Expense", message: "Please select Categories", in: self)
        }else if self.strDiplayDate == nil {
            ShowAlert(title: "Expense", message: "Please select date", in: self)
        }else {

            let catId = Int(self.objSelectCategories.id!)
            let catIcone = self.objSelectCategories.icn_name
            let intAmount = Double(self.txtAmount.text!)
            let accId = self.arrAccountData[self.selectedIndex].id!
            var every = 0
                if self.strDay != "0" {
                       every = Int(self.strDay)!
                }
                   //"Week","Day"]
            var repeating = 0
            if self.strMonth != "Month" {
                if self.strMonth == "Months"{
                    repeating = 3
                }else if self.strMonth == "Week"{
                    repeating = 2
                }else if self.strMonth == "Day"{
                    repeating = 1
                }


                       
            }
            let param = ["cat_id":catId,"acc_id":accId,"transaction_date":self.strAPIDate,"amount":intAmount,"is_repeating":self.isRepet,"every":every,"repeating":repeating,"end_date":self.strApiEndDate,"notes":self.notes,"type":1,"par_tra_id":0,"cat_name":objSelectCategories.name!,"icn_name":catIcone] as [String : Any]
            
            print(param)
            DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                if success == true {
                    print(message)
                    if self.isRepet == 1 {
                         self.repeating(strDate:self.strAPIDate , param: param)
                    }
                    self.navigationController?.popViewController(animated: true)
                }else {
                    print(message)
                }
            }
        }
       }
    func saveIncome() {
     
     if self.txtAmount.text?.isValidString() == false {
         ShowAlert(title: "Expense", message: "Please enter amount", in: self)
     }else if self.txtCategories.text?.isEmpty == true
     {
         ShowAlert(title: "Expense", message: "Please select Categories", in: self)
     }else if self.strDiplayDate == nil {
         ShowAlert(title: "Expense", message: "Please select date", in: self)
     }else {

         let catId = Int(self.objSelectCategories.id!)
         let intAmount = Double(self.txtAmount.text!)
        let catIcn = objSelectCategories.icn_name
         let accId = self.arrAccountData[self.selectedIndex].id!
        var every = 0
        if self.strDay != "0" {
            every = Int(self.strDay)!
        }
        //"Week","Day"]
        var repeating = 0
        if self.strMonth != "Month" {
            if self.strMonth == "Months"{
                repeating = 3
            }else if self.strMonth == "Week"{
                repeating = 2
            }else if self.strMonth == "Day"{
                repeating = 1
            }


            
        }
        let param = ["cat_id":catId,"acc_id":accId,"transaction_date":self.strAPIDate,"amount":intAmount,"is_repeating":self.isRepet,"every":every,"repeating":repeating,"end_date":self.strApiEndDate,"notes":self.notes,"type":2,"par_tra_id":0,"cat_name":objSelectCategories.name!,"icn_name":catIcn] as [String : Any]
         
         print(param)
         DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
             if success == true {
                if self.isRepet == 1 {
                    self.repeating(strDate:self.strAPIDate , param: param)
                }
                 print(message)
                self.navigationController?.popViewController(animated: true)
             }else {
                 print(message)
             }
         }
     }
    }
    
    func UpdateTran() {
        
        var cat_name = ""
        var icn_name = ""
        var cat_id = ""
        if txtCategories.text != self.objData.cat_name! {
        
            cat_name = self.objSelectCategories.name!
            icn_name = self.objSelectCategories.icn_name!
            cat_id = self.objSelectCategories.id!
        }else {
            cat_name = self.objData.cat_name
            icn_name = self.objData.icn_name!
            cat_id = self.objData.cat_id!
        }
         let intAmount = Double(self.txtAmount.text!)
        let accID = objData.acc_id!
//        let obj = self.arrAccountData[selectedIndex]
//        if obj.id == objData.acc_id {
//            accID = obj.id
//        }else {
//            accID = obj.id
//        }
        if objData.is_repeating == 0 {
            if self.isRepet == 0 {
                var every = 0
                if self.strDay != "0" {
                    every = Int(self.strDay)!
                }
                var repeating = 0
                if self.strMonth != "Month" {
                    if self.strMonth == "Months"{
                        repeating = 3
                    }else if self.strMonth == "Week"{
                        repeating = 2
                    }else if self.strMonth == "Day"{
                        repeating = 1
                    }
                }
                var type = 0
                if self.isIncome == true{
                    type = 2
                }else {
                    type = 1
                }

                let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":self.strAPIDate,"amount":intAmount!,"is_repeating":self.isRepet,"every":every,"repeating":repeating,"end_date":self.strApiEndDate,"notes":self.notes,"type":type,"par_tra_id":0,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                
                DatabaseManager.shared.UpdateTransaction(withParametrs: param, strId: objData.id!) { (success, message) in
                    if success == true {
                        self.navigationController?.popViewController(animated: true)
                    }else {
                        
                    }
                }
            }else  {
                var type = 0
                if self.isIncome == true{
                    type = 2
                }else {
                    type = 1
                }
                
                var repeating = 0
                               var every = 0
                               if self.strDay != "0" {
                                  every = Int(self.strDay)!
                               }
                                var daycount = every
                               var tempDayCpont = every
                                var tempMonth = every
                                var monthcount = every
                               if self.strMonth == "Months"{
                                   repeating = 3
                                   daycount = every * 30
                                   tempDayCpont = daycount
                               }else if self.strMonth == "Week"{
                                   repeating = 2
                                   daycount = every * 7
                                   tempDayCpont = daycount
                               }else if self.strMonth == "Day"{
                                   repeating = 1
                               }
                let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":self.strAPIDate,"amount":intAmount!,"is_repeating":self.isRepet,"every":self.strDay,"repeating":repeating,"end_date":self.strApiEndDate,"notes":self.notes,"type":type,"par_tra_id":0,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                
                DatabaseManager.shared.UpdateTransaction(withParametrs: param, strId: objData.id!) { (success, message) in
                    if success == true {
                        self.navigationController?.popViewController(animated: true)
                    }else {
                        
                    }
                }

                
                
               
                let nowDate = Date()
                let strEndDateup = dataBaseDate(intDate: nowDate)
                let endDate = getEndDate(strDate: strEndDateup)
                let startDateup = getStaringDate(strDate:  self.strAPIDate)
                let dayDiff = DayBetweenDate(startDate: endDate, endDate: startDateup)
                let endLoopDay = 1826 - dayDiff
                let strday =  0 - dayDiff
                let is_repeating = 1
                let everyData = every
                let repeatingData = repeating
                let end_date = self.strApiEndDate
                
                
               
                if self.strApiEndDate != "" {
                    if repeating == 3 {
                        for i in 1..<60 {
                            if monthcount == i {
                                monthcount = monthcount + tempMonth
                                let traDate = getendNextDateMonths(day: i, stratDate: startDateup)
                                if traDate.2 <= self.endDateRepate {
                                    let traDataDate = dataBaseDate(intDate: traDate.2)
                                   // let obj = self.arrLatsTransaction.last
                                    let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":notes,"type":type,"par_tra_id":objData.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                    DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                        if success == true {
                                            
                                        }else {
                                            
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }else {
                        for i in 1..<1826 {
                            if repeating == 1 {
                                if daycount == i {
                                    daycount = daycount + tempDayCpont
                                    let traDate = getendNextDate(day: i, startDate: startDateup)
                                    if traDate.2 <= self.endDateRepate {
                                        let traDataDate = dataBaseDate(intDate: traDate.2)
                                       // let obj = self.arrLatsTransaction.last
                                        let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":notes,"type":type,"par_tra_id":objData.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                        DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                            if success == true {
                                                
                                            }else {
                                                
                                            }
                                        }
                                        
                                    }
                                }
                            }else if repeating == 2 {
                                if daycount == i {
                                    daycount = daycount + tempDayCpont
                                    let traDate = getendNextDateMonths(day: i, stratDate: startDateup)
                                    if traDate.2 <= self.endDateRepate {
                                        let traDataDate = dataBaseDate(intDate: traDate.2)
                                        
                                        let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":notes,"type":type,"par_tra_id":objData.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                        DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                            if success == true {
                                                
                                            }else {
                                                
                                            }
                                        }
                                        
                                    }
                                }

                            }
                        }
                    }
                    
                }else {
                    let lastDate = getendDate(dayof: endLoopDay)
                    
                    if repeating == 3 {
                        for i in 1..<60 {
                            if monthcount == i {
                                
                                monthcount = monthcount + tempMonth
                                
                                let traDate = getendNextDateMonths(day: i, stratDate: startDateup)
                                if traDate.2 <= lastDate.2 {
                                    let traDataDate = dataBaseDate(intDate: traDate.2)
                                   // let obj = self.arrLatsTransaction.last
                                    let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":notes,"type":type,"par_tra_id":objData.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                    DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                        if success == true {
                                            print("Yes Yse")
                                        }else {
                                            
                                        }
                                    }
                                    
                                }
                            }

                        }
                    }else {
                        for i in 1..<1826 {
                            print(i)
                            if repeating == 1 {
                                if daycount == i {
                                    daycount = daycount + tempDayCpont
                                    let traDate = getendNextDate(day: i, startDate: startDateup)
                                    if traDate.2 <= lastDate.2 {
                                        let traDataDate = dataBaseDate(intDate: traDate.2)
                                        let obj = objData
                                        let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":notes,"type":type,"par_tra_id":obj?.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                        DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                            if success == true {
                                                
                                            }else {
                                                
                                            }
                                        }
                                        
                                    }
                                }
                            }else if repeating == 2 {
                                if daycount == i {
                                    daycount = daycount + tempDayCpont
                                    let traDate = getendNextDate(day: i, startDate: startDateup)
                                    if traDate.2 <= lastDate.2 {
                                        let traDataDate = dataBaseDate(intDate: traDate.2)
                                        let obj = objData
                                        let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":notes,"type":type,"par_tra_id":obj?.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                        DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                            if success == true {
                                                
                                            }else {
                                                
                                            }
                                        }
                                        
                                    }
                                }

                            }
                        }
                    }
                    
                }
            }
        }else if self.objData.is_repeating == 1 {
            if self.isRepet == 0 {
                var type = 0
                if self.isIncome == true{
                    type = 2
                }else {
                    type = 1
                }

                let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":self.strAPIDate!,"amount":intAmount!,"is_repeating":0,"every":0,"repeating":0,"end_date":self.strApiEndDate,"notes":self.notes,"type":type,"par_tra_id":0,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                
                DatabaseManager.shared.UpdateTransaction(withParametrs: param, strId: objData.id!) { (success, message) in
                    if success == true {
                        self.deleteRepeating(strID: self.objData.id!)
                        self.navigationController?.popViewController(animated: true)
                    }else {
                        
                    }
                }

            }else {
                if self.isRepet == 1 {
                    var type = 0
                    if self.isIncome == true{
                        type = 2
                    }else {
                        type = 1
                    }
                    
                    var repeating = 0
                        var every = 0
                        if self.strDay != "0" {
                        every = Int(self.strDay)!
                    }
                    var daycount = every
                    var tempDayCpont = every
                    var tempMonth = every
                    var monthcount = every
                    if self.strMonth == "Months"{
                        repeating = 3
                        //daycount = every * 30
                        tempMonth = every
                        monthcount = 0
                    }else if self.strMonth == "Week"{
                        repeating = 2
                        daycount = every * 7
                        tempDayCpont = daycount
                        daycount = 0
                    }else if self.strMonth == "Day"{
                        repeating = 1
                        daycount = 0
                    }
                    var traId = ""
                    if objData.par_tra_id == "0" {
                        traId = objData.id!
                        daycount = tempDayCpont
                        monthcount = tempMonth
                        if every != self.objData.every || repeating != objData.repeating || self.strAPIDate != self.objData.transaction_date  || self.strApiEndDate != self.objData.end_date! {
                                               
                                               
                                               let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":self.strAPIDate!,"amount":intAmount!,"is_repeating":1,"every":every,"repeating":repeating,"end_date":self.strApiEndDate,"notes":self.notes,"type":type,"par_tra_id":0,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                               
                                               DatabaseManager.shared.UpdateTransaction(withParametrs: param, strId: traId) { (success, message) in
                                                   if success == true {
                                                       self.deleteRepeating(strID: self.objData.id!)
                                                       self.navigationController?.popViewController(animated: true)
                                                       let nowDate = Date()
                                                        let strEndDateup = dataBaseDate(intDate: nowDate)
                                                        let endDate = getEndDate(strDate: strEndDateup)
                                                       let startDateup = getStaringDate(strDate:  self.strAPIDate!)
                                                        let dayDiff = DayBetweenDate(startDate: endDate, endDate: startDateup)
                                                        let endLoopDay = 1826 - dayDiff
                                                        let strday =  0 - dayDiff
                                                        let is_repeating = 1
                                                        let everyData = every
                                                        let repeatingData = repeating
                                                       let end_date = self.strApiEndDate
                                                        
                                                        
                                                       
                                                        if self.strApiEndDate != "" {
                                                           if repeating == 3 {
                                                               for i in 1..<61 {
                                                                   if monthcount == i {
                                                                       monthcount = monthcount + tempMonth
                                                                       let traDate = getendNextDateMonths(day: i, stratDate: startDateup)
                                                                       if traDate.2 <= self.endDateRepate {
                                                                           let traDataDate = dataBaseDate(intDate: traDate.2)
                                                                          // let obj = self.arrLatsTransaction.last
                                                                          let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":traId,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                                                           DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                                               if success == true {
                                                                                   
                                                                               }else {
                                                                                   
                                                                               }
                                                                           }
                                                                           
                                                                       }
                                                                   }

                                                               }
                                                           }else {
                                                               for i in 1..<1826 {
                                                                   if repeating == 1 {
                                                                       if daycount == i {
                                                                           daycount = daycount + tempDayCpont
                                                                           let traDate = getendNextDate(day: i, startDate: startDateup)
                                                                           if traDate.2 <= self.endDateRepate {
                                                                               let traDataDate = dataBaseDate(intDate: traDate.2)
                                                                              // let obj = self.arrLatsTransaction.last
                                                                              let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":self.objData.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                                                               DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                                                   if success == true {
                                                                                       
                                                                                   }else {
                                                                                       
                                                                                   }
                                                                               }
                                                                               
                                                                           }
                                                                       }
                                                                   }else if repeating == 2 {
                                                                       if daycount == i {
                                                                           daycount = daycount + tempDayCpont
                                                                          let traDate = getendNextDate(day: i, startDate: startDateup)
                                                                           if traDate.2 <= self.endDateRepate {
                                                                               let traDataDate = dataBaseDate(intDate: traDate.2)
                                                                               
                                                                              let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":traId,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                                                               DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                                                   if success == true {
                                                                                       
                                                                                   }else {
                                                                                       
                                                                                   }
                                                                               }
                                                                               
                                                                           }
                                                                       }

                                                                   }
                                                               }
                                                           }
                                                            
                                                        }else {
                                                            let lastDate = getendDate(dayof: endLoopDay)
                                                           if repeating == 3 {
                                                               
                                                               for i in 1..<61 {
                                                                   if monthcount == i {
                                                                   monthcount = monthcount + tempMonth
                                                                       let traDate = getendNextDateMonths(day: i, stratDate: startDateup)
                                                                   if traDate.2 <= lastDate.2 {
                                                                   let traDataDate = dataBaseDate(intDate: traDate.2)
                                                                                                                      // let obj = self.arrLatsTransaction.last
                                                                   let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":traId as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                                                   DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                                           if success == true {
                                                                               print("Yes Yse")
                                                                           }else {
                                                                           }
                                                                   }
                                                                                                                       
                                                               }
                                                           }
                                                               }
                                                           }else {
                                                               for i in 1..<1826 {
                                                                   print(i)
                                                                   if repeating == 1 {
                                                                       if daycount == i {
                                                                           daycount = daycount + tempDayCpont
                                                                           let traDate = getendNextDate(day: i, startDate: startDateup)
                                                                           if traDate.2 <= lastDate.2 {
                                                                               let traDataDate = dataBaseDate(intDate: traDate.2)
                                                                              let obj = self.objData
                                                                              let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":traId,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                                                               DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                                                   if success == true {
                                                                                       
                                                                                   }else {
                                                                                       
                                                                                   }
                                                                               }
                                                                               
                                                                           }
                                                                       }
                                                                   }else if repeating == 2 {
                                                                       if daycount == i {
                                                                           daycount = daycount + tempDayCpont
                                                                          let traDate = getendNextDate(day: i, startDate: startDateup)
                                                                           if traDate.2 <= lastDate.2 {
                                                                               let traDataDate = dataBaseDate(intDate: traDate.2)
                                                                              //let obj = self.objData
                                                                              let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":traId,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                                                               DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                                                   if success == true {
                                                                                       
                                                                                   }else {}}}}}}}}
                                                           }else {
                                                       
                                                   }
                                               }

                                               
                                           }else {
                                               let pram = ["cat_id":cat_id,"acc_id":accID,"amount":intAmount!,"notes":self.notes,"type":type,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]//UpdateTransactionSpecificValue
                                               DatabaseManager.shared.UpdateTransactionSpecificValue(withParametrs: pram, strId: traId) { (success, message) in
                                                   if success == true {
                                                       DatabaseManager.shared.UpdateTransactionRepeatingSpecificValue(withParametrs: pram, strId: traId) { (success, message) in
                                                           if success == true {
                                                               
                                                           }
                                                       }
                                                       self.navigationController?.popViewController(animated: true)
                                                   }
                                               }
                                           }
                    }else {
                        traId = objData.par_tra_id!
                        if every != self.objData.every || repeating != objData.repeating || self.strAPIDate != self.objData.transaction_date  || self.strApiEndDate != self.objData.end_date! {
                            self.deleteRepeating(strID: traId)
                            self.navigationController?.popViewController(animated: true)
                            let nowDate = Date()
                             let strEndDateup = dataBaseDate(intDate: nowDate)
                             let endDate = getEndDate(strDate: strEndDateup)
                            let startDateup = getStaringDate(strDate:  self.strAPIDate!)
                             let dayDiff = DayBetweenDate(startDate: endDate, endDate: startDateup)
                             let endLoopDay = 1826 - dayDiff
                             let strday =  0 - dayDiff
                             let is_repeating = 1
                             let everyData = every
                             let repeatingData = repeating
                            let end_date = self.strApiEndDate
                            
                            if self.isAllUpdate == true {
                                if self.strApiEndDate != "" {
                                                                                          if repeating == 3 {
                                                                                              for i in 0..<61 {
                                                                                                  if monthcount == i {
                                                                                                      monthcount = monthcount + tempMonth
                                                                                                      let traDate = getendNextDateMonths(day: i, stratDate: startDateup)
                                                                                                      if traDate.2 <= self.endDateRepate {
                                                                                                          let traDataDate = dataBaseDate(intDate: traDate.2)
                                                                                                         // let obj = self.arrLatsTransaction.last
                                                                                                         let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":traId,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                                                                                          DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                                                                              if success == true {
                                                                                                                  
                                                                                                              }else {
                                                                                                                  
                                                                                                              }
                                                                                                          }
                                                                                                          
                                                                                                      }
                                                                                                  }

                                                                                              }
                                                                                          }else {
                                                                                              for i in 0..<1826 {
                                                                                                  if repeating == 1 {
                                                                                                      if daycount == i {
                                                                                                          daycount = daycount + tempDayCpont
                                                                                                          let traDate = getendNextDate(day: i, startDate: startDateup)
                                                                                                          if traDate.2 <= self.endDateRepate {
                                                                                                              let traDataDate = dataBaseDate(intDate: traDate.2)
                                                                                                             // let obj = self.arrLatsTransaction.last
                                                                                                             let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":self.objData.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                                                                                              DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                                                                                  if success == true {
                                                                                                                      
                                                                                                                  }else {
                                                                                                                      
                                                                                                                  }
                                                                                                              }
                                                                                                              
                                                                                                          }
                                                                                                      }
                                                                                                  }else if repeating == 2 {
                                                                                                      if daycount == i {
                                                                                                          daycount = daycount + tempDayCpont
                                                                                                         let traDate = getendNextDate(day: i, startDate: startDateup)
                                                                                                          if traDate.2 <= self.endDateRepate {
                                                                                                              let traDataDate = dataBaseDate(intDate: traDate.2)
                                                                                                              
                                                                                                             let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":traId,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                                                                                              DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                                                                                  if success == true {
                                                                                                                      
                                                                                                                  }else {
                                                                                                                      
                                                                                                                  }
                                                                                                              }
                                                                                                              
                                                                                                          }
                                                                                                      }

                                                                                                  }
                                                                                              }
                                                                                          }
                                                                                           
                                }else {
                                    
                                     let lastDate = getendDate(dayof: endLoopDay)
                                    if repeating == 3 {
                                        
                                        for i in 0..<61 {
                                            if monthcount == i {
                                            monthcount = monthcount + tempMonth
                                                let traDate = getendNextDateMonths(day: i, stratDate: startDateup)
                                            if traDate.2 <= lastDate.2 {
                                            let traDataDate = dataBaseDate(intDate: traDate.2)
                                                                                               // let obj = self.arrLatsTransaction.last
                                            let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":traId as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                            DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                    if success == true {
                                                        print("Yes Yse")
                                                    }else {
                                                    }
                                            }
                                                                                                
                                        }
                                    }
                                        }
                                    }else {
                                        for i in 0..<1826 {
                                            print(i)
                                            if repeating == 1 {
                                                if daycount == i {
                                                    daycount = daycount + tempDayCpont
                                                    let traDate = getendNextDate(day: i, startDate: startDateup)
                                                    if traDate.2 <= lastDate.2 {
                                                        let traDataDate = dataBaseDate(intDate: traDate.2)
                                                       let obj = self.objData
                                                       let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":traId,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                                        DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                            if success == true {
                                                                
                                                            }else {
                                                                
                                                            }
                                                        }
                                                        
                                                    }
                                                }
                                            }else if repeating == 2 {
                                                if daycount == i {
                                                    daycount = daycount + tempDayCpont
                                                   let traDate = getendNextDate(day: i, startDate: startDateup)
                                                    if traDate.2 <= lastDate.2 {
                                                        let traDataDate = dataBaseDate(intDate: traDate.2)
                                                       //let obj = self.objData
                                                       let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":traDataDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":traId,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                                        DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                            if success == true {
                                                                
                                                            }else {}}}}}}}
                                }
                            }else {
                                // MARK: SingleUpdate
                                let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":self.strAPIDate,"amount":intAmount!,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":self.notes,"type":type,"par_tra_id":traId,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                DatabaseManager.shared.UpdateTransaction(withParametrs: param, strId: objData.id) { (success, message) in
                                    if success == true {
                                        
                                    }
                                }
                            }
                            
                        
                        }else {
                             let pram = ["cat_id":cat_id,"acc_id":accID,"amount":intAmount!,"notes":self.notes,"type":type,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                            if self.isAllUpdate == true {
                                DatabaseManager.shared.UpdateTransactionRepeatingSpecificValue(withParametrs: pram, strId: traId) { (success, message) in
                                    if success == true {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }else {
                                
                                DatabaseManager.shared.UpdateTransactionSpecificValue(withParametrs: pram, strId: objData.id!) { (success, message) in
                                    if success == true {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                                

                            }
                        }
                    }
                    
                    
                   
                }else {
                    
                    var type = 0
                    if self.isIncome == true{
                        type = 2
                    }else {
                        type = 1
                    }
                    
                    var repeating = 0
                        var every = 0
                        if self.strDay != "0" {
                        every = Int(self.strDay)!
                    }
                    var daycount = every
                    var tempDayCpont = every
                    if self.strMonth == "Months"{
                        repeating = 3
                        daycount = every * 30
                        tempDayCpont = daycount
                    }else if self.strMonth == "Week"{
                        repeating = 2
                        daycount = every * 7
                        tempDayCpont = daycount
                    }else if self.strMonth == "Day"{
                        repeating = 1
                    }
                    var traId = ""
                    if objData.par_tra_id == "0" {
                        traId = objData.id
                    }else {
                        traId = objData.par_tra_id
                    }
                        let param = ["cat_id":cat_id,"acc_id":accID,"transaction_date":self.strAPIDate!,"amount":intAmount!,"is_repeating":1,"every":every,"repeating":repeating,"end_date":self.strApiEndDate,"notes":self.notes,"type":type,"par_tra_id":0,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                   DatabaseManager.shared.UpdateTransaction(withParametrs: param, strId: traId) { (success, message) in
                        if success == true {
                           
                            let param1 = ["cat_id":cat_id,"acc_id":accID,"amount":intAmount!,"is_repeating":1,"every":every,"repeating":repeating,"end_date":self.strApiEndDate,"notes":self.notes,"type":type,"par_tra_id":self.objData.id!,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                            DatabaseManager.shared.UpdateRepeatingTransaction(withParametrs: param1, strId: traId) { (success, message) in
                                if success == true {
                                    
                                }else {
                                    
                                }
                            }
                            self.navigationController?.popViewController(animated: true)
                        }else {
                            
                        }
                    }

                    /// MARK:-
                }
            }
        }
            
            
            
    
    }
    
    func deleteRepeating(strID:String) {
        DatabaseManager.shared.deleteRepeatingTransaction(withParametrs: strID) { (success, message) in
            if success == true {
                
            }
        }
    }
    // let param = ["cat_id":catId,"acc_id":accId,"transaction_date":self.strAPIDate,"amount":intAmount,"is_repeating":self.isRepet,"every":every,"repeating":repeating,"end_date":self.strApiEndDate,"notes":self.notes,"type":2,"par_tra_id":0,"cat_name":objSelectCategories.name!,"icn_name":catIcn] as [String : Any]
    func repeating(strDate:String,param:[String:Any])  {
        
        let cat_id = param["cat_id"]!
        let acc_id = param["acc_id"]!
        let amount = param["amount"]!
        
        let end_date = self.strApiEndDate
        let notes = param["notes"]!
        let type = param["type"]
        let cat_name = param["cat_name"]!
        let icn_name = param["icn_name"]!
        
        
        var repeating = 0
        var every = 0
        if self.strDay != "0" {
           every = Int(self.strDay)!
        }
         var daycount = every
        var tempDayCpont = every
        var tempMonth = every
        var monthcount = every
        if self.strMonth == "Months"{
            repeating = 3
           // daycount = every * 30
           // tempDayCpont = daycount
        }else if self.strMonth == "Week"{
            repeating = 2
            daycount = every * 7
            tempDayCpont = daycount
        }else if self.strMonth == "Day"{
            repeating = 1
        }
       let is_repeating = 1
       let everyData = every
       let repeatingData = repeating
        let nowDate = Date()
        let strEndDateup = dataBaseDate(intDate: nowDate)
        let endDate = getEndDate(strDate: strEndDateup)
        let startDateup = getStaringDate(strDate: self.strAPIDate!)
        let dayDiff = DayBetweenDate(startDate: endDate, endDate: startDateup)
        let endLoopDay = 1826 - dayDiff
        _ =  0 - dayDiff
        
        DatabaseManager.shared.getLastTransaction(withParametrs: strDate) { (success, message, arrData) in
            if success == true {
                self.arrLatsTransaction = arrData
                if self.arrLatsTransaction.count > 0 {
                    if self.strApiEndDate != "" {
                        if repeating == 3 {
                            for i in 1..<60 {
                            if monthcount == i {
                                monthcount = monthcount + tempMonth
                                let traDate = getendNextDateMonths(day: i, stratDate: startDateup)
                                if traDate.2 <= self.endDateRepate {
                                    let traDataDate = dataBaseDate(intDate: traDate.2)
                                    let obj = self.arrLatsTransaction.last
                                    let param = ["cat_id":cat_id,"acc_id":acc_id,"transaction_date":traDataDate,"amount":amount,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":notes,"type":type,"par_tra_id":obj?.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                    DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                        if success == true {
                                            
                                        }else {
                                            
                                        }
                                    }
                                    
                                }
                            }
                            }
                        }
                        for i in 1..<1826 {
                            if repeating == 1 {
                                if daycount == i {
                                    daycount = daycount + tempDayCpont
                                    let traDate = getendNextDate(day: i, startDate: startDateup)
                                    if traDate.2 <= self.endDateRepate {
                                        let traDataDate = dataBaseDate(intDate: traDate.2)
                                        let obj = self.arrLatsTransaction.last
                                        let param = ["cat_id":cat_id,"acc_id":acc_id,"transaction_date":traDataDate,"amount":amount,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":notes,"type":type,"par_tra_id":obj?.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                        DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                            if success == true {
                                                
                                            }else {
                                                
                                            }
                                        }
                                        
                                    }
                                }
                            }else if repeating == 2 {
                                if daycount == i {
                                    daycount = daycount + tempDayCpont
                                    let traDate = getendNextDate(day: i, startDate: startDateup)
                                    if traDate.2 <= self.endDateRepate {
                                        let traDataDate = dataBaseDate(intDate: traDate.2)
                                        let obj = self.arrLatsTransaction.last
                                        let param = ["cat_id":cat_id,"acc_id":acc_id,"transaction_date":traDataDate,"amount":amount,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":notes,"type":type,"par_tra_id":obj?.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                        DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                            if success == true {
                                                
                                            }else {
                                                
                                            }
                                        }
                                        
                                    }
                                }

                            }
                        }
                    }else {
                        let lastDate = getendDate(dayof: endLoopDay)
                        if repeating == 3 {
                        for i in 1..<60 {
                        if monthcount == i {
                        monthcount = monthcount + tempMonth
                            let traDate = getendNextDateMonths(day: i, stratDate: startDateup)
                        if traDate.2 <= lastDate.2{
                        let traDataDate = dataBaseDate(intDate: traDate.2)
                        let obj = self.arrLatsTransaction.last
                        let param = ["cat_id":cat_id,"acc_id":acc_id,"transaction_date":traDataDate,"amount":amount,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":notes,"type":type,"par_tra_id":obj?.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                        DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                        if success == true {
                        }else {
                        }
                            }
                        }
                          }
                            }
                        }else {
                            for i in 1..<1826 {
                                print(i)
                                if repeating == 1 {
                                    if daycount == i {
                                        daycount = daycount + tempDayCpont
                                        let traDate = getendNextDate(day: i, startDate: startDateup)
                                        if traDate.2 <= lastDate.2 {
                                            let traDataDate = dataBaseDate(intDate: traDate.2)
                                            let obj = self.arrLatsTransaction.last
                                            let param = ["cat_id":cat_id,"acc_id":acc_id,"transaction_date":traDataDate,"amount":amount,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":notes,"type":type,"par_tra_id":obj?.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                            DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                if success == true {
                                                    
                                                }else {
                                                    
                                                }
                                            }
                                            
                                        }
                                    }
                                }else if repeating == 2 {
                                    if daycount == i {
                                        daycount = daycount + tempDayCpont
                                        let traDate = getendNextDate(day: i, startDate: startDateup)
                                        if traDate.2 <= lastDate.2 {
                                            let traDataDate = dataBaseDate(intDate: traDate.2)
                                            let obj = self.arrLatsTransaction.last
                                            let param = ["cat_id":cat_id,"acc_id":acc_id,"transaction_date":traDataDate,"amount":amount,"is_repeating":is_repeating,"every":everyData,"repeating":repeatingData,"end_date":end_date,"notes":notes,"type":type,"par_tra_id":obj?.id! as Any,"cat_name":cat_name,"icn_name":icn_name] as [String : Any]
                                            DatabaseManager.shared.addTransaction(withParametrs: param) { (success, message) in
                                                if success == true {
                                                    
                                                }else {
                                                    
                                                }
                                            }
                                            
                                        }
                                    }

                                }
                            }
                        }
                        
                    }
                }
            }else {
                
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddExpensesVC:SelectedDate{
    
    func getDate(_ APIDate:String,_ strDisplayDate:String,_ endDate:Date) {
        if isEnddate == true {
            self.strApiEndDate = APIDate
            self.strEndDate = dipayDate(strDate: APIDate)
            self.lblEndDate.text = self.strEndDate
            self.isEnddate = false
            self.endDateRepate = endDate
            self.btnClearEndDate.isHidden = false
        }else {
            self.strAPIDate = APIDate
        
            self.strDiplayDate = dipayDate(strDate: APIDate)
            self.lblDate.text = self.strDiplayDate
        }
        
    }
}


extension AddExpensesVC:SelectedCategories {
    
    func getCategories(_ objCategories: CategoryData) {
        self.objSelectCategories = objCategories
        self.lblCategories.text = objCategories.name!
        self.txtCategories.text = objCategories.name!
        self.icnCategory.image = UIImage(named: objCategories.icn_name!)
    }//notes
}
extension AddExpensesVC:getNotes {
    
    func getNotes(_ notes: String) {
        //if notes != "" {
            self.notes = notes
        //}
     
        self.lblNotes.text = self.notes
       
    }//notes
}

extension AddExpensesVC: UIPickerViewDataSource, UIPickerViewDelegate {

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return self.arrAccountData.count
        }else {
            if component == 0 {
                return self.arrRepeateDay.count
            }else {
                return self.arrDayMonts.count
            }
        }
        
            //self.arrAccount.count
       
       
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerView.tag == 1 {
             return 1
        }else {
             return 2
        }
       
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       
        if pickerView.tag == 1 {
            return self.arrAccountData[row].name!
        }else {
            if component == 0 {
                return self.arrRepeateDay[row]
            }else {
                return self.arrDayMonts[row]
            }
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 2 {
            if component == 0 {
                self.strDay = self.arrRepeateDay[row]
            }else {
                self.strMonth = self.arrDayMonts[row]
            }
            self.lblRepeatDay.text = self.strDay + " " + self.strMonth
        }else {
            self.lblAccount.text = self.arrAccountData[row].name
            if isUpdate == true {
                self.objData.acc_id = self.arrAccountData[row].id!
            }
            self.selectedIndex = row
        }
            
        
    }
}
