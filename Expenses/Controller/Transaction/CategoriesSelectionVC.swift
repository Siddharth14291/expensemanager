//
//  CategoriesSelectionVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 20/01/20.
//

import UIKit

@objc protocol SelectedCategories : NSObjectProtocol
{
    @objc optional func getCategories(_ objCategories:CategoryData)
}

class CategoriesSelectionVC: UIViewController {

    @IBOutlet weak var tblCategoriesList: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchBackView: UIView!
    var delegate:SelectedCategories?
    var arrCategoriesList:[CategoryData] = []
    var objCategories:CategoryData!
    var selectredID = ""
    var isIncome = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUI()
        if isIncome == true {
            getIncomeCate()
        }else {
            getExpensesCat()
        }
        
    }
    
    func setUI() {
        self.view.backgroundColor = bgGrayColor
        self.searchBackView.backgroundColor = searchViewBGColor
        self.searchView.backgroundColor = textWhiteColor
    }
    func getExpensesCat() {
        
        DatabaseManager.shared.getExpenseCategory(withParametrs: [:]) { (success, message, arrData) in
            if success == true {
                self.arrCategoriesList = arrData
                self.tblCategoriesList.reloadData()
            }else
            {
                
            }
        }
    }
    
    func getIncomeCate() {
        
        DatabaseManager.shared.getIncomeCategory(withParametrs: [:]) { (success, message, arrData) in
            if success == true {
                self.arrCategoriesList = arrData
                self.tblCategoriesList.reloadData()
            }
        }
    }
    @IBAction func btnBackaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddNewCataction(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "AddNewCategoriesVC") as! AddNewCategoriesVC
        next.isUpdate = false
        if self.isIncome == true {
             next.catType = 2
        }else {
             next.catType = 1
        }
       
        self.navigationController?.pushViewController(next, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CategoriesSelectionVC :UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCategoriesList.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesSelectionListCell") as! CategoriesSelectionListCell
        let obj = self.arrCategoriesList[indexPath.row]
        cell.lblName.text = obj.name!
        cell.icnCategories.image = UIImage(named: obj.icn_name)
       
        
            if self.selectredID == obj.id {
                 cell.accessoryType = .checkmark
            }else {
                
            }
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.arrCategoriesList[indexPath.row]
        self.navigationController?.popViewController(animated: true)
        self.delegate?.getCategories?(obj)
    }
}

