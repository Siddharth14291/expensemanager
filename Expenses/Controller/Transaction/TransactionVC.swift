//
//  TransactionVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 21/01/20.
//

import UIKit

class TransactionVC: UIViewController {
    
    @IBOutlet var lblCategories: UILabel!
    @IBOutlet var lblSort: UILabel!
    @IBOutlet weak var accountView: UIView!
    @IBOutlet weak var calenderView: UIView!
    
    @IBOutlet weak var lblIncome: UILabel!
    @IBOutlet weak var lblExpenses: UILabel!
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var btnPre: UIButton!
    @IBOutlet weak var lblCalenderType: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnAccount: UIButton!
    @IBOutlet weak var btnCategories: UIButton!
    @IBOutlet weak var btnSort: UIButton!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var tblTransaction: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var filterBackView: UIView!
    var CurrentCount = -1
    var accountPicker = UIPickerView()
    var backView = UIView()
    var toolBar = UIToolbar()
    var arrAccount = ["Justin", "ABC"]
    var arrCategoryData:[CategoryData] = []
    var arrTransactionData:[TransactionData] = []
    var arrFilterTransaction:[TransactionData] = []
    var arrAccountData:[AccountData] = []
    var arrTempFilterTransactionData:[TransactionData] = []
    var totalExpense = 0.0
    var totalIncome = 0.0
    var isFilter = false
    var arrCategoryList:[String] = []
    var carryOverAmountFinal = 0.0
    var iscarrayOver = false
    var strCarryDate = ""
    var arrCaryOverAmount:[Double] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         self.iscarrayOver = false
         self.txtSearch.resignFirstResponder()
        getCategory()
        getAccount()
            getData()
            
    }
    func setUI() {
        self.view.backgroundColor = bgGrayColor
        self.filterBackView.backgroundColor = searchViewBGColor
        self.searchView.backgroundColor = textWhiteColor
        self.sortView.backgroundColor = textWhiteColor
       // self.accountView.backgroundColor = textWhiteColor
        
        self.txtSearch.attributedPlaceholder =  NSAttributedString(string: "Search",
               attributes: [NSAttributedString.Key.foregroundColor: catTextColor])
        //self.btnCategories.setTitleColor(catTextColor, for: .normal)
        self.lblCategories.textColor = catTextColor
        //self.btnSort.setTitleColor(catTextColor, for: .normal)
        self.lblSort.textColor = catTextColor
        self.lblCalenderType.textColor = textWhiteColor
      //  self.lblAccount.textColor = catTextColor
            // self.lblIncome.textColor = incomeGreen
       // self.lblExpenses.textColor = expenseRed
       
    }
    func getData() {
        if cuurrentCalenderType == 1 {
            getCalenderTypeDay()
        }else if cuurrentCalenderType == 2  {
            getCalenderTypetWeek()
        }else if cuurrentCalenderType == 3  {
            getCalenderTypeMoths()
        }else if cuurrentCalenderType == 4  {
            getCalenderTypetYear()
        }
    }
    func getNextCalenderData() {
        self.CurrentCount = self.CurrentCount + 1
        if cuurrentCalenderType == 1 {
            getCalenderNextDay()
        }else if cuurrentCalenderType == 2  {
            getCalenderNextWeek()
        }else if cuurrentCalenderType == 3  {
            getCalenderNextMoths()
        }else if cuurrentCalenderType == 4  {
            getCalenderNextYear()
        }
    }
    func getPreviousCalenderData() {
        self.CurrentCount = self.CurrentCount - 1
        if cuurrentCalenderType == 1 {
            getCalenderPreDay()
        }else if cuurrentCalenderType == 2  {
            getCalenderPreWeek()
        }else if cuurrentCalenderType == 3  {
            getCalenderPreMoths()
        }else if cuurrentCalenderType == 4  {
            getCalenderPreYear()
        }
    }

    func getCalenderTypeDay() {
        self.btnNext.isUserInteractionEnabled = true
        let calType = currentCalenderDay()
        self.lblCalenderType.text = calType.0
        getTrasnactionDay(strDate: calType.1)
    }
    func getCalenderTypeMoths() {
        self.btnNext.isUserInteractionEnabled = true
        let calType = currentCalenderMonth()
        self.lblCalenderType.text = calType.0
        strCarryDate = dipayDate(strDate: calType.1)
       getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
    
    }
    func getCalenderTypetWeek() {
        self.btnNext.isUserInteractionEnabled = true
        let calType = currentCalenderWeek()
        self.lblCalenderType.text = calType.0
       getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
    }
    func getCalenderTypetYear() {
        self.btnNext.isUserInteractionEnabled = true
        let calType = currentCalenderYear()
        self.lblCalenderType.text = calType.0
        getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
    }
    func getCalenderNextDay() {
        let calType = nextCalenderDay()
        if calType.2 == true {
            self.btnNext.isUserInteractionEnabled = false
        }else {
            self.btnNext.isUserInteractionEnabled = true
        }
        self.lblCalenderType.text = calType.0
        getTrasnactionDay(strDate: calType.1)
    }
    func getCalenderNextMoths() {
        let calType = nextCalenderMonth()
        if calType.3 == true {
            self.btnNext.isUserInteractionEnabled = false
        }else {
            self.btnNext.isUserInteractionEnabled = true
        }
        self.lblCalenderType.text = calType.0
        strCarryDate = dipayDate(strDate: calType.1)
       getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
    }
    func getCalenderNextWeek() {
        let calType = nextCalenderWeek()
        if calType.3 == true {
            self.btnNext.isUserInteractionEnabled = false
        }else {
            self.btnNext.isUserInteractionEnabled = true
        }
        self.lblCalenderType.text = calType.0
       getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
    }
    func getCalenderNextYear() {
        let calType = nextCalenderYear()
        self.lblCalenderType.text = calType.0
        if calType.3 == true {
             self.btnNext.isUserInteractionEnabled = false
        }else {
            self.btnNext.isUserInteractionEnabled = true
        }
        getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
    }
    func getCalenderPreDay() {
        self.btnNext.isUserInteractionEnabled = true
          let calType = previousCalenderDay()
          self.lblCalenderType.text = calType.0
          getTrasnactionDay(strDate: calType.1)
      }
      func getCalenderPreMoths() {
        self.btnNext.isUserInteractionEnabled = true
          let calType = previousCalenderMonth()
          self.lblCalenderType.text = calType.0
        strCarryDate = dipayDate(strDate: calType.1)
         getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
      }
      func getCalenderPreWeek() {
        self.btnNext.isUserInteractionEnabled = true
          let calType = previousCalenderWeek()
          self.lblCalenderType.text = calType.0
         getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
      }
      func getCalenderPreYear() {
        self.btnNext.isUserInteractionEnabled = true
          let calType = previousCalenderYear()
          self.lblCalenderType.text = calType.0
          getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
      }
    func getTrasnactionDay(strDate:String) {
        DatabaseManager.shared.getTransactionType(withParametrs:strDate, strType: "day") { (success, message,arrData) in
            if success == true {
                self.isFilter = false
                self.arrTransactionData = arrData
                self.filterData()
                //self.tblTransaction.reloadData()
            }else {
                
            }
        }
    }
    func getTransactionCaltype(strStartDate:String,strEndDate:String) {
        DatabaseManager.shared.getTransactionByCalenderType(withParametrs:strStartDate , strEndDate: strEndDate) { (success, message, arrData) in
            if success == true {
                self.isFilter = false
                self.arrTransactionData = arrData
                self.filterData()
                //self.tblTransaction.reloadData()
            }else {
                
            }
        }
    }

    func getCategory() {
        DatabaseManager.shared.getCategory(withParametrs: [:]) { (success, message, arrDate) in
            if success == true {
                self.arrCategoryData = arrDate
                let obj = CategoryData(strId: "0", strName: "All Repeating", strIconName: "", intType: 3, strChartColor: "")
                let obj1 = CategoryData(strId: "0", strName: "All Income", strIconName: "", intType: 3, strChartColor: "")
                let obj2 = CategoryData(strId: "0", strName: "All Expense", strIconName: "", intType: 3, strChartColor: "")
                let obj3 = CategoryData(strId: "0", strName: "All Categories", strIconName: "", intType: 3, strChartColor: "")
                self.arrCategoryData.insert(obj, at: 0)
                self.arrCategoryData.insert(obj1, at: 0)
                self.arrCategoryData.insert(obj2, at: 0)
                self.arrCategoryData.insert(obj3, at: 0)
            }else {
                
            }
        }
    }
    func getAccount() {
    
        DatabaseManager.shared.getAccount(withParametrs: [:]) { (success, message,arrData) in
            if success == true {
                self.arrAccountData = arrData
                let obj = AccountData(strId: "0", strName: "All Acount", strChartColor: "", intHideFuturetra: 0, intTimePeriod: 0, intIsCarryoveron: 0, intIsPositiveonly: 0, intIsBudgeton: 0, doAmount: 0.0, intIsIncludeincome: 0)
                self.arrAccountData.insert(obj, at: 0)
            }else {
                
            }
        }
    }
    
   
  

    
    func filterData() {
        
        switch currentSelectedAccount {
        case "0":
            self.arrFilterTransaction = self.arrTransactionData
            break
            
        default:
            self.arrFilterTransaction = arrTransactionData.filter { $0.acc_id.lowercased().contains((currentSelectedAccount.lowercased()))}
            
       //     self.btnCategories.setTitle(currentSelectCatbyFilter, for: .normal)
            self.lblCategories.text = currentSelectCatbyFilter
//            let arrData = arrAccountData.filter { $0.id.lowercased().contains((currentSelectedAccount.lowercased()))}
//            if arrData.count > 0 {
//                let obj = arrData[0]
//                if obj.hide_futuretra == 1 {
//                    self.btnNext.isUserInteractionEnabled = false
//                }
//            }
            break
            
        }
        getCarryOverData()
        switch currentSelectCatbyFilter {
        case "All Categories":
            let arrData = self.arrFilterTransaction
            self.arrFilterTransaction = arrData
           // self.btnCategories.setTitle(currentSelectCatbyFilter, for: .normal)
            self.lblCategories.text = currentSelectCatbyFilter
            self.iscarrayOver = true
            
        case "All Expense":
            self.arrFilterTransaction = self.arrFilterTransaction.filter({$0.type == 1})
            //self.btnCategories.setTitle(currentSelectCatbyFilter, for: .normal)
            self.lblCategories.text = currentSelectCatbyFilter
            self.iscarrayOver = false
            break
        case "All Income" :
            self.arrFilterTransaction = self.arrFilterTransaction.filter({$0.type == 2})
            //self.btnCategories.setTitle(currentSelectCatbyFilter, for: .normal)
            self.lblCategories.text = currentSelectCatbyFilter
            self.iscarrayOver = false
            break
        case "All Repeating":
            self.arrFilterTransaction = self.arrFilterTransaction.filter({$0.is_repeating == 1})
            //self.btnCategories.setTitle(currentSelectCatbyFilter, for: .normal)
            self.lblCategories.text = currentSelectCatbyFilter
            self.iscarrayOver = false
            break
        default:
            self.arrFilterTransaction = arrFilterTransaction.filter { $0.cat_name.lowercased().contains((currentSelectCatbyFilter.lowercased()))}
            
            self.iscarrayOver = false
           // self.btnCategories.setTitle(currentSelectCatbyFilter, for: .normal)
            self.lblCategories.text = currentSelectCatbyFilter
        }
        
        
        switch currentSortType {
        case 1:
            let arrdata = self.arrFilterTransaction.sorted(by: { (Obj1, Obj2) -> Bool in
                    let Obj1_Name = Obj1.transaction_date ?? ""
                    let Obj2_Name = Obj2.transaction_date ?? ""
                return (Obj1_Name.localizedCaseInsensitiveCompare(Obj2_Name) == .orderedDescending)
                })
           // self.btnSort.setTitle("Newest Date", for: .normal)
            self.lblSort.text = "Newest Date"
                self.arrFilterTransaction = arrdata
                
        case 2:
            
            let arrdata = self.arrFilterTransaction.sorted(by: { (Obj1, Obj2) -> Bool in
               let Obj1_Name = Obj1.transaction_date ?? ""
               let Obj2_Name = Obj2.transaction_date ?? ""
               return (Obj1_Name.localizedCaseInsensitiveCompare(Obj2_Name) == .orderedAscending)
            })
            self.arrFilterTransaction = arrdata
             //self.btnSort.setTitle("Oldest Date", for: .normal)
             self.lblSort.text = "Oldest Date"
            break
        case 3:
            self.arrFilterTransaction.sort(by:{$0.amount > $1.amount})
            // self.btnSort.setTitle("Highest Amount", for: .normal)
             self.lblSort.text = "Highest Amount"
            break
            
        case 4:
            self.arrFilterTransaction.sort(by:{$0.amount < $1.amount})
            //self.btnSort.setTitle("Lowest Amount", for: .normal)
             self.lblSort.text = "Lowest Amount"
            break
        
        case 5:
            
            let arrdata = self.arrFilterTransaction.sorted(by: { (Obj1, Obj2) -> Bool in
               let Obj1_Name = Obj1.cat_name ?? ""
               let Obj2_Name = Obj2.cat_name ?? ""
               return (Obj1_Name.localizedCaseInsensitiveCompare(Obj2_Name) == .orderedAscending)
            })
            self.arrFilterTransaction = arrdata
           // self.btnSort.setTitle("A-Z", for: .normal)
            self.lblSort.text = "A-Z"
            break
        case 6:
            let arrdata = self.arrFilterTransaction.sorted(by: { (Obj1, Obj2) -> Bool in
               let Obj1_Name = Obj1.cat_name ?? ""
               let Obj2_Name = Obj2.cat_name ?? ""
               return (Obj1_Name.localizedCaseInsensitiveCompare(Obj2_Name) == .orderedDescending)
            })
            self.arrFilterTransaction = arrdata
            // self.btnSort.setTitle("Z-A", for: .normal)
            self.lblSort.text = "Z-A"
            break
            
            
        default:
            break
        }
        self.totalExpense = 0.0
        self.totalIncome = 0.0
        if tranactionSearchtext != "" {
            self.arrTempFilterTransactionData = arrFilterTransaction.filter { $0.cat_name.lowercased().contains((tranactionSearchtext.lowercased()))}
            
            self.isFilter = true
            
            for i in self.arrTempFilterTransactionData {
                if i.type == 1 {
                    self.totalExpense = self.totalExpense + i.amount
                }else {
                    self.totalIncome = self.totalIncome + i.amount
                }
            }
        }else {
             self.isFilter = false
            
            for i in self.arrFilterTransaction {
                if i.type == 1 {
                    self.totalExpense = self.totalExpense + i.amount
                }else {
                    self.totalIncome = self.totalIncome + i.amount
                }
            }
        }
        
       // self.tblTransaction.reloadData()
       
    
        
        self.tblTransaction.reloadData()
        
//        if currentSelectedAccount == "0" {
//            let isOn = UserDefaults.standard.value(forKey: strAllAccountHide) as! Int
//            if isOn == 1 {
//               if currentCalender > 0 {
//                   self.btnNext.isUserInteractionEnabled = false
//               }else {
//                   self.btnNext.isUserInteractionEnabled = true
//               }
//            }else {
//                self.btnNext.isUserInteractionEnabled = true
//            }
//        }else {
//             let arrData = arrAccountData.filter { $0.id.lowercased().contains((currentSelectedAccount.lowercased()))}
//            if arrData.count > 0 {
//                let obj = arrData[0]
//                if obj.hide_futuretra == 1 {
//                    if currentCalender > 0 {
//                        self.btnNext.isUserInteractionEnabled = false
//                    }else {
//                        self.btnNext.isUserInteractionEnabled = true
//                    }
//                }else {
//                    self.btnNext.isUserInteractionEnabled = true
//                }
//            }else {
//                self.btnNext.isUserInteractionEnabled = true
//            }
//        }
    }
    
    
    func getCarryOverData() {
        if cuurrentCalenderType == 1 {
            
        }else if cuurrentCalenderType == 2 {
            
        }else if cuurrentCalenderType == 3 {
            if currentSelectedAccount == "0" {
                let calType = carrayOverDateMonth(day: self.CurrentCount)
                getCartyDataDataBase(strDate: calType)
            }else {
                let calType = carrayOverDateMonth(day: self.CurrentCount)
                getCarryOneAccount(strDate:calType, strAccId: currentSelectedAccount)
            }
        }else if cuurrentCalenderType == 4 {
            
        }
        
    }
    
    func getCartyDataDataBase(strDate:String) {
       
        var tempIncome = 0.0
        var tempExpense = 0.0
        var carryOverAmount = 0.0
        if self.arrCaryOverAmount.count > 0 {
            self.arrCaryOverAmount.removeAll()
        }else {
            
        }
        for i in arrAccountData{
            if i.id != "0" {
                
                if i.is_carryoveron == 1 {
                    self.iscarrayOver = true
                    if i.is_positiveonly == 1 {
                        
                        DatabaseManager.shared.getTotalExpenseByCarryOver(withParametrs: "", strEndDate: strDate, type: "2", strAccId: i.id!) { (success, message, amount) in
                            if success == true {
                                
                                
                                if i.is_budgeton == 1 {
                                    if i.is_includeincome == 1 {
                                        
                                        let bgAmount = i.amount
                                        if self.CurrentCount > 0 {
                                            let count = self.CurrentCount + 1
                                            let finalBGAmount = (bgAmount! * Double(count))
                                            tempIncome = tempIncome + amount + finalBGAmount
                                        }else {
                                              tempIncome = tempIncome + amount + i.amount
                                        }
                                      
                                    }else {
                                        tempIncome = tempIncome + i.amount
                                    }
                                }else {
                                    tempIncome = tempIncome + amount
                                }
                            }
                        }
                        DatabaseManager.shared.getTotalExpenseByCarryOver(withParametrs: "", strEndDate: strDate, type: "1", strAccId: i.id!) { (success, message, amount) in
                            if success == true {
                                tempExpense = tempExpense + amount
                            }
                        }
                        let reAmount = tempIncome - tempExpense
                        if tempIncome > tempExpense {
                             carryOverAmount = tempIncome - tempExpense
                        }else {
                            carryOverAmount = carryOverAmount + 0.0
                        }
                        self.arrCaryOverAmount.append(carryOverAmount)
                        tempExpense = 0.0
                        tempIncome = 0.0
                    }else {
                        
                        DatabaseManager.shared.getTotalExpenseByCarryOver(withParametrs: "", strEndDate: strDate, type: "2", strAccId: i.id!) { (success, message, amount) in
                            if success == true {
                                tempExpense = 0.0
                                tempIncome = 0.0
                                if i.is_budgeton == 1 {
                                    if i.is_includeincome == 1 {
                                        let bgAmount = i.amount
                                        if self.CurrentCount > 0 {
                                             let count = self.CurrentCount + 1
                                            let finalBGAmount = (bgAmount! * Double(count))
                                            tempIncome = tempIncome + amount + finalBGAmount
                                        }else {
                                              tempIncome = tempIncome + amount + i.amount
                                        }
                                    }else {
                                        tempIncome = tempIncome + i.amount
                                    }
                                }else {
                                    tempIncome = tempIncome + amount
                                }
                            }
                        }
                        DatabaseManager.shared.getTotalExpenseByCarryOver(withParametrs: "", strEndDate: strDate, type: "1", strAccId: i.id!) { (success, message, amount) in
                            if success == true {
                                tempExpense = tempExpense + amount
                            }
                        }
                        carryOverAmount = tempIncome - tempExpense
                         self.arrCaryOverAmount.append(carryOverAmount)
                    }
                }
            }
        }
        self.carryOverAmountFinal = carryOverAmount
        self.tblTransaction.reloadData()
    }
    
    func getCarryOneAccount(strDate:String,strAccId:String) {
       
        let arrData = arrAccountData.filter { $0.id.lowercased().contains((strAccId.lowercased()))}
        if arrData.count > 0 {
            if self.arrCaryOverAmount.count > 0 {
                self.arrCaryOverAmount.removeAll()
            }else {
                
            }
            let i = arrData[0]
            var tempIncome = 0.0
            var tempExpense = 0.0
            var carryOverAmount = 0.0
            if i.is_carryoveron == 1 {
                self.iscarrayOver = true
            if i.is_positiveonly == 1 {
                 
                DatabaseManager.shared.getTotalExpenseByCarryOver(withParametrs: "", strEndDate: strDate, type: "2", strAccId: i.id!) { (success, message, amount) in
                    if success == true {
                        
                        
                        if i.is_budgeton == 1 {
                            if i.is_includeincome == 1 {
                                let bgAmount = i.amount
                                if self.CurrentCount > 0 {
                                    let count = self.CurrentCount + 1
                                    let finalBGAmount = (bgAmount! * Double(count))
                                    tempIncome = tempIncome + amount + finalBGAmount
                                }else {
                                      tempIncome = tempIncome + amount + i.amount
                                }
                            }else {
                                tempIncome = tempIncome + i.amount
                            }
                        }else {
                            tempIncome = tempIncome + amount
                        }
                    }
                }
                DatabaseManager.shared.getTotalExpenseByCarryOver(withParametrs: "", strEndDate: strDate, type: "1", strAccId: i.id!) { (success, message, amount) in
                    if success == true {
                        tempExpense = tempExpense + amount
                    }
                }
                let reAmount = tempIncome - tempExpense
                if tempIncome > tempExpense {
                     carryOverAmount = tempIncome - tempExpense
                }else {
                    carryOverAmount = carryOverAmount + 0.0
                }
               
                self.arrCaryOverAmount.append(carryOverAmount)
                tempExpense = 0.0
                tempIncome = 0.0
                }else {
                    DatabaseManager.shared.getTotalExpenseByCarryOver(withParametrs: "", strEndDate: strDate, type: "2", strAccId: i.id!) { (success, message, amount) in
                        if success == true {
                            if i.is_budgeton == 1 {
                                if i.is_includeincome == 1 {
                                    let bgAmount = i.amount
                                    if self.CurrentCount > 0 {
                                        let count = self.CurrentCount + 1
                                        let finalBGAmount = (bgAmount! * Double(count))
                                        tempIncome = tempIncome + amount + finalBGAmount
                                    }else {
                                          tempIncome = tempIncome + amount + i.amount
                                    }
                                }else {
                                    tempIncome = tempIncome + i.amount
                                }
                            }else {
                                tempIncome = tempIncome + amount
                            }
                        }
                    }
                    DatabaseManager.shared.getTotalExpenseByCarryOver(withParametrs: "", strEndDate: strDate, type: "1", strAccId: i.id!) { (success, message, amount) in
                        if success == true {
                            tempExpense = tempExpense + amount
                        }
                    }
                    carryOverAmount = tempIncome - tempExpense
                 self.arrCaryOverAmount.append(carryOverAmount)
                }
            }
            self.carryOverAmountFinal = carryOverAmount
            self.tblTransaction.reloadData()

        }
            }
    //@IBAction func btnPreviousaction(_ sender: UIButton) {
    
    @IBAction func btnPreviousCalenderaction(_ sender: UIButton) {
        
         self.txtSearch.resignFirstResponder()
        getPreviousCalenderData()
    }
    
    
    @IBAction func btnNextCalenderaction(_ sender: UIButton) {
        self.txtSearch.resignFirstResponder()
            getNextCalenderData()
    }
    
    
    @IBAction func btnSortactionaction(_ sender: UIButton) {
         self.txtSearch.resignFirstResponder()
        actionShhet()
    }
    
    @IBAction func btnAccountaction(_ sender: UIButton) {
         self.txtSearch.resignFirstResponder()
        pickerAccount(tag: 1)
    }
    
    @IBAction func btnCalenderTypeaction(_ sender: UIButton) {
         self.txtSearch.resignFirstResponder()
        actionCalenderType()
    }
    
    @IBAction func btnCategorySortaction(_ sender: UIButton) {
         self.txtSearch.resignFirstResponder()
        pickerAccount(tag: 2)
    }
    
    @IBAction func btnAddTransactionaction(_ sender: UIButton) {
         self.txtSearch.resignFirstResponder()
        let next = self.storyboard?.instantiateViewController(withIdentifier: "AddExpensesVC") as! AddExpensesVC
            next.isIncome = false
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    
     func actionShhet() {
            let optionMenu = UIAlertController(title: nil, message: "Sort By", preferredStyle: .actionSheet)

            let DateNewAction = UIAlertAction(title: "Date (Newest First)", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                UserDefaults.standard.set(1, forKey: strCurrentSortType)
                currentSortType = 1
                self.filterData()
               
            })

            let DateOldAction = UIAlertAction(title: "Date (Oldest First)", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                UserDefaults.standard.set(2, forKey: strCurrentSortType)
                currentSortType = 2
                self.filterData()
                
            })

            let amountHigAction = UIAlertAction(title: "Amount (Highest First)", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                UserDefaults.standard.set(3, forKey: strCurrentSortType)
                currentSortType = 3
                self.filterData()
                
               
            })
            let amountLowAction = UIAlertAction(title: "Amount (Lowest First)", style: .default, handler:
            {
                
                (alert: UIAlertAction!) -> Void in
                UserDefaults.standard.set(4, forKey: strCurrentSortType)
                currentSortType = 4
                self.filterData()
                
                 
               
            })
            let catAZAction = UIAlertAction(title: "Category (A-Z)", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                UserDefaults.standard.set(5, forKey: strCurrentSortType)
                currentSortType = 5
                self.filterData()
                
               
            })
            let catZAAction = UIAlertAction(title: "Category (Z-A)", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                UserDefaults.standard.set(6, forKey: strCurrentSortType)
                currentSortType = 6
                self.filterData()
                
               
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
            {
                (alert: UIAlertAction!) -> Void in
               
            })
            optionMenu.addAction(DateNewAction)
            optionMenu.addAction(DateOldAction)
            optionMenu.addAction(amountHigAction)
            optionMenu.addAction(amountLowAction)
            optionMenu.addAction(catAZAction)
            optionMenu.addAction(catZAAction)
            optionMenu.addAction(cancelAction)
            self.present(optionMenu, animated: true, completion: nil)
        }
    func actionCalenderType() {
           let optionMenu = UIAlertController(title: nil, message: "Show Expenses", preferredStyle: .actionSheet)

           let dailyAction = UIAlertAction(title: "Daily", style: .default, handler:
           {
               (alert: UIAlertAction!) -> Void in
            UserDefaults.standard.set(1, forKey: strCalenderType)
            UserDefaults.standard.synchronize()
            currentCalender = 0
            if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
                cuurrentCalenderType = calType
            }
            self.getData()
              
           })

           let weeklyAction = UIAlertAction(title: "Weekly", style: .default, handler:
           {
               (alert: UIAlertAction!) -> Void in
                UserDefaults.standard.set(2, forKey: strCalenderType)
                 UserDefaults.standard.synchronize()
                 currentCalender = 0
                 if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
                     cuurrentCalenderType = calType
                 }
                 self.getData()
                   
                
           })

           let monthlyAction = UIAlertAction(title: "Monthly", style: .default, handler:
           {
               (alert: UIAlertAction!) -> Void in
                UserDefaults.standard.set(3, forKey: strCalenderType)
                UserDefaults.standard.synchronize()
                currentCalender = 0
                if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
                    cuurrentCalenderType = calType
                }
                self.getData()
              
           })
           let yearlyAction = UIAlertAction(title: "Yearly", style: .default, handler:
           {
               (alert: UIAlertAction!) -> Void in
                UserDefaults.standard.set(4, forKey: strCalenderType)
                UserDefaults.standard.synchronize()
                currentCalender = 0
                if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
                    cuurrentCalenderType = calType
                }
                self.getData()
              
           })
           let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
           {
               (alert: UIAlertAction!) -> Void in
              
           })
           optionMenu.addAction(dailyAction)
           optionMenu.addAction(weeklyAction)
           optionMenu.addAction(monthlyAction)
           optionMenu.addAction(yearlyAction)
           optionMenu.addAction(cancelAction)
           self.present(optionMenu, animated: true, completion: nil)
       }
    func pickerAccount(tag:Int){
        accountPicker = UIPickerView.init()
        backView = UIView()
        backView.frame = self.view.bounds
        backView.backgroundColor = UIColor.black
        backView.alpha = 0.2
       // backView.isUserInteractionEnabled = false
        self.view.addSubview(backView)
        accountPicker.delegate = self
        accountPicker.backgroundColor = textWhiteColor
        accountPicker.setValue(UIColor.black, forKey: "textColor")
        accountPicker.autoresizingMask = .flexibleWidth
        accountPicker.contentMode = .center
        accountPicker.tag = tag
        accountPicker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 340, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(accountPicker)

        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 340, width: UIScreen.main.bounds.size.width, height: 40))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
        toolBar.barStyle = .default
      //  toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
     //    toolBar.items = [UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(onDoneButtonTapped))]
        let btmDone = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onDoneButtonTapped))
       

        let btnCancel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onCancelButtonTapped))
        toolBar.items = [btnCancel, flexibleSpace,flexibleSpace, btmDone,]
        toolBar.backgroundColor = textWhiteColor
        self.view.addSubview(toolBar)
    }
    //MARK: ToolBar Done Button Tap Action
    @objc func onDoneButtonTapped() {
        if accountPicker.tag == 1 {
            let index:Int = accountPicker.selectedRow(inComponent: 0)
                currentSelectedAccount = self.arrAccountData[index].id
                 UserDefaults.standard.set(currentSelectedAccount, forKey: strCurrentSelectedAcoount)
                self.filterData()
            
        }else if accountPicker.tag == 2 {
           
            let index:Int = accountPicker.selectedRow(inComponent: 0)
                
                currentSelectCatbyFilter = self.arrCategoryData[index].name
                 UserDefaults.standard.set(currentSelectCatbyFilter, forKey: strCurrentSelectCat)
                self.filterData()
        }else{
            
        }
        backView.removeFromSuperview()
        toolBar.removeFromSuperview()
        accountPicker.removeFromSuperview()
    }
    @objc func onCancelButtonTapped() {
        if self.accountPicker.tag == 1 {
            
            if let acc = UserDefaults.standard.value(forKey: strCurrentSelectedAcoount) as? String {
                currentSelectedAccount =  acc
                self.filterData()
            }
        }else {
            
            if let cat = UserDefaults.standard.value(forKey: strCurrentSelectCat) as? String {
                currentSelectCatbyFilter = cat
                self.filterData()
            }
        }
        backView.removeFromSuperview()
        toolBar.removeFromSuperview()
        accountPicker.removeFromSuperview()
    }
    //MARK: ToolBar Done Button Tap Action
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension TransactionVC:UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           
        if section  == 0 {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! TransactionHeaderCell
              let finalIncome = self.totalIncome.clean//String(format:"%.0f", self.totalIncome)
              let finalExpenses = self.totalExpense.clean//String(format:"%.0f", self.totalExpense)
              headerCell.lblIncome.text = currentCurrencySymbols + " " + "\(finalIncome)"
              headerCell.lblExpense.text = currentCurrencySymbols + " " + "\(finalExpenses)"
             //let labelHeaderTitle = headerCell.viewWithTag(11) as! UILabel
             
             
            // headerCell.lblTitle.text = arrSection[section]
            // headerCell.lblSecondTitle.text = arrSectionTitle[section]
            headerCell.isUserInteractionEnabled = false
             return headerCell
        }else {
            return UITableViewCell()
        }
               
           
          
       }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 60
        }else {
            return 0
        }
             
       
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if self.isFilter == true {
                return  self.arrTempFilterTransactionData.count
            }else {
                return self.arrFilterTransaction.count
            }
        }else {
            if iscarrayOver == true {
                return self.arrCaryOverAmount.count
            }else {
                return 0
            }
        }
        
           
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionListCell") as! TransactionListCell
            if self.isFilter == true {

                let obj = self.arrTempFilterTransactionData[indexPath.row]
                cell.lblCatName.text = obj.cat_name
                cell.lblDate.text = dipayDate(strDate: obj.transaction_date)
                //self.totalExpense.clean
                cell.lblAmount.text = currentCurrencySymbols + " "+"\(obj.amount.clean)"
                cell.icnCat.image = UIImage(named: obj.icn_name)
                if obj.type == 1 {
                    cell.lblAmount.textColor = expenseRed
                }else {
                    cell.lblAmount.textColor = incomeGreen
                }
            }else {

                let obj = self.arrFilterTransaction[indexPath.row]
                cell.lblCatName.text = obj.cat_name
                cell.lblDate.text = dipayDate(strDate: obj.transaction_date)
                //        self.totalExpense.clean
                cell.lblAmount.text =  currentCurrencySymbols + " "+"\(obj.amount.clean)"
                cell.icnCat.image = UIImage(named: obj.icn_name)
                if obj.type == 1 {
                    cell.lblAmount.textColor = expenseRed
                }else {
                    cell.lblAmount.textColor = incomeGreen
                }
            }
            
            
            
            cell.selectionStyle = .none
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarrayOverTransactionCell") as! CarrayOverTransactionCell
            let obj = self.arrCaryOverAmount[indexPath.row]
            cell.lblAmount.text = "\(obj.clean)"
            cell.lblName.text = "CarrayOver"
            cell.lblDate.text = strCarryDate
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let next = self.storyboard?.instantiateViewController(withIdentifier: "AddExpensesVC") as! AddExpensesVC
            if self.isFilter == true {
                next.objData = self.arrTempFilterTransactionData[indexPath.row]
                if self.arrTempFilterTransactionData[indexPath.row].type == 1 {
                    next.isIncome = false
                }else {
                    next.isIncome = true
                }
            }else {
                 next.objData = self.arrFilterTransaction[indexPath.row]
                if self.arrFilterTransaction[indexPath.row].type == 1 {
                    next.isIncome = false
                }else {
                    next.isIncome = true
                }
            }
            next.isUpdate = true
            self.navigationController?.pushViewController(next, animated: true)
        }
        
    }
}


extension TransactionVC: UIPickerViewDataSource, UIPickerViewDelegate {

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return self.arrAccountData.count//self.arrAccount.count
        }else {
            return self.arrCategoryData.count
        }
       
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return self.arrAccountData[row].name!
        }else {
            return self.arrCategoryData[row].name!
        }
        
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            //self.lblAccount.text = self.arrAccountData[row].name
            currentSelectedAccount = self.arrAccountData[row].id
            self.filterData()

        }else {
           
            currentSelectCatbyFilter = self.arrCategoryData[row].name
            self.filterData()
           
        }
        
    }
}
extension TransactionVC:UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        self.arrTempFilterTransactionData = arrFilterTransaction.filter { $0.cat_name.lowercased().contains((textField.text!.lowercased()))}
        tranactionSearchtext = textField.text!
        self.isFilter = true
        self.totalExpense = 0.0
        self.totalIncome = 0.0
        for i in self.arrTempFilterTransactionData {
            if i.type == 1 {
                self.totalExpense = self.totalExpense + i.amount
            }else {
                self.totalIncome = self.totalIncome + i.amount
            }
        }
        self.tblTransaction.reloadData()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
        
        }else {
            tranactionSearchtext = ""
            self.isFilter = false
            self.totalExpense = 0.0
            self.totalIncome = 0.0
            for i in self.arrFilterTransaction {
                if i.type == 1 {
                    self.totalExpense = self.totalExpense + i.amount
                }else {
                    self.totalIncome = self.totalIncome + i.amount
                }
            }
            self.tblTransaction.reloadData()
        }
    }
}
