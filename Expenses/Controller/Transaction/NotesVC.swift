//
//  NotesVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 24/01/20.
//

import UIKit

@objc protocol getNotes : NSObjectProtocol
{
    @objc optional func getNotes(_ notes:String)
}

class NotesVC: UIViewController {

    @IBOutlet var txtNotes: UITextView!
    var delegate:getNotes?
    var strNotes = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = bgGrayColor
        self.txtNotes.text = strNotes
       // txtNotes.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        txtNotes.becomeFirstResponder()
    }
    
    @IBAction func btnCancelaction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDoneactionaction(_ sender: UIButton) {
        delegate?.getNotes?(txtNotes.text!)
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
