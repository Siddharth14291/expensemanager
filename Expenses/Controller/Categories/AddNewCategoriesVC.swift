//
//  AddNewCategoriesVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 21/01/20.
//

import UIKit

class AddNewCategoriesVC: UIViewController {

    @IBOutlet weak var txtCategoriesName: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCategoriesName: UITextField!
    @IBOutlet weak var icnCategories: UIImageView!
    @IBOutlet weak var lblCategoriesIcon: UILabel!
    var objCat:CategoryData!
    var catType = 1
    var isUpdate = false
    var icnName:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        if isUpdate == true {
            self.txtCategoriesName.text = objCat.name!
            self.icnName = objCat.icn_name!
            self.icnCategories.image = UIImage(named: objCat.icn_name!)
        }
        // Do any additional setup after loading the view.
    }
    
    func setUi() {
        self.view.backgroundColor = bgGrayColor
        self.lblTitle.textColor = catTextColor
        self.lblCategoriesIcon.textColor = catTextColor
        self.txtCategoriesName.textColor = catTextColor
        self.txtCategoriesName.attributedPlaceholder = NSAttributedString(string: "Name",
        attributes: [NSAttributedString.Key.foregroundColor: catTextColor])
    }

    @IBAction func btnBackaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnSaveaction(_ sender: UIButton) {
        //let param = ["name":arrIcnName[i],"icn_name":arrIcon[i],"type":1,"chart_color":""] as [String : Any]
        if self.txtCategoriesName.text?.isValidString() == false {
             ShowAlert(title: "Expense", message: "Please select name", in: self)
        }else if self.icnName == nil {
             ShowAlert(title: "Expense", message: "Please select icon", in: self)
        }else {
            if isUpdate == true {
                let param = ["name":self.txtCategoriesName.text!,"icn_name":self.icnName,"type":self.catType,"chart_color":""] as [String : Any]
                DatabaseManager.shared.UpdateCategory(withParametrs: param, strId: objCat.id) { (success, message) in
                    if success == true {
                        self.navigationController?.popViewController(animated: true)
                    }else {
                        
                    }
                }
            }else {
                let param = ["name":self.txtCategoriesName.text!,"icn_name":self.icnName,"type":self.catType,"chart_color":""] as [String : Any]
                DatabaseManager.shared.addCategory(withParametrs: param) { (success, message) in
                    if success == true  {
                        self.navigationController?.popViewController(animated: true)
                    }else {
                        
                    }
                }
            }
            
        }
    }
    
    @IBAction func btnSelectIconaction(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "CategoriesIconSeclectionVC") as! CategoriesIconSeclectionVC
        next.delegate = self
        self.navigationController?.present(next, animated: true, completion: nil)
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddNewCategoriesVC:SelectedICon {
    func selectCatIcon(_ name:String)
    {
        self.icnCategories.image = UIImage(named: name)
        icnName = name
    }
}
