//
//  CategoriesIconSeclectionVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 21/01/20.
//

import UIKit
@objc protocol SelectedICon : NSObjectProtocol
{
    @objc optional func selectCatIcon(_ name:String)
}
class CategoriesIconSeclectionVC: UIViewController {

    @IBOutlet var iconCollection: UICollectionView!
    var arrIcon = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36"]
    var delegate:SelectedICon?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.iconCollection.register(UINib(nibName: "CategoriesIconCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesIconCell")
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnCloseaction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CategoriesIconSeclectionVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrIcon.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesIconSeclectionCell", for: indexPath) as! CategoriesIconSeclectionCell
        cell.icnCategories.image = UIImage(named: self.arrIcon[indexPath.item])
        cell.backgroundColor = bgGrayColor
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 4 - 10
        return CGSize(width: width, height: width)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.selectCatIcon?(self.arrIcon[indexPath.item])
        self.dismiss(animated: true, completion: nil)
    }
}
