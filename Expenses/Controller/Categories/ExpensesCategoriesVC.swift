//
//  ExpensesCategoriesVC.swift
//  Expenses
//
//  
//

import UIKit
import XLPagerTabStrip
class ExpensesCategoriesVC: UIViewController,IndicatorInfoProvider {

    // MARK :- XLPageTabStrip Delegate method
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
    
            return IndicatorInfo(title: "Expenses")
    }
    // MARk:- Variable end Outlets
    
    @IBOutlet weak var addCategoriesBackView: UIView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var addCategoriesView: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var tblCategoriesList: UITableView!
    
    var arrCategories:[CategoryData] = []
    
    // MARK:- ViewController method
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        setUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getCategoriesList()

    }
    // MARK :- Set Color
    func setUI(){
        
        self.addCategoriesBackView.backgroundColor = searchViewBGColor
        self.view.backgroundColor = bgGrayColor
        self.lblTitle.textColor = catTextColor
    
        
    }
    
    func getCategoriesList() {
        
        DatabaseManager.shared.getExpenseCategory(withParametrs: [:]) { (success, message, arrData) in
            if success == true{
                self.arrCategories = arrData
                self.tblCategoriesList.reloadData()
                print(self.arrCategories.count)
            }else {
                
            }
        }
    }
    func deleteCategory(index:Int) -> Bool {
        
        let obj = self.arrCategories[index]
        DatabaseManager.shared.checkCategoryData(withParametrs: [:], strCatname: obj.id!) { (success, message, count) in
            if success == true {
                print(count)
            }else {
                //return false
            }
        }
        return false
    }
    
    @IBAction func btnAddNewCateaction(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "AddNewCategoriesVC") as! AddNewCategoriesVC
               next.isUpdate = false
               next.catType = 1
        self.navigationController?.pushViewController(next, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK:- TableView Delegate end DataSource Methods
extension ExpensesCategoriesVC:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCategories.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExpensesCategoriesListCell") as! ExpensesCategoriesListCell
        let obj = self.arrCategories[indexPath.row]
        cell.lblName.text = obj.name!
        cell.icnCategories.image = UIImage(named: obj.icn_name)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           let next = self.storyboard?.instantiateViewController(withIdentifier: "AddNewCategoriesVC") as! AddNewCategoriesVC
           next.isUpdate = true
           next.catType = 1
           next.objCat = self.arrCategories[indexPath.row]
           self.navigationController?.pushViewController(next, animated: true)
       }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //self.deleteCategory(index: indexPath.row)
            let obj = self.arrCategories[indexPath.row]
            DatabaseManager.shared.checkCategoryData(withParametrs: [:], strCatname: obj.id!) { (success, message, count) in
                       if success == true {
                        if count == 0 {
                            DatabaseManager.shared.deleteCategory(withParametrs: obj.id!) { (success, message) in
                                if success == true {
                                    self.arrCategories.remove(at: indexPath.row)
                                    tableView.deleteRows(at: [indexPath], with: .fade)
                                }else {
                                    
                                }
                            }
                        }else {
                            ShowAlert(title: "Expense", message: " Category is currently associated with one or more transactions", in: self)
                        }
                       }else {
                           //return false
                       }
                   }
           
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
}
