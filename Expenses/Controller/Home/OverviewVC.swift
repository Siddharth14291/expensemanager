//
//  OverviewVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 18/01/20.
//

import UIKit

class OverviewVC: UIViewController {
    
    @IBOutlet weak var accountPickerview: ToolbarPickerView!
    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var lblRemainingBalance: UILabel!
    @IBOutlet weak var lblTitleBalance: UILabel!
    @IBOutlet weak var lblCalenderType: UILabel!
    @IBOutlet weak var imgHeader: UIImageView!
    
    @IBOutlet var btnNext: UIButton!
    var accountPicker = UIPickerView()
    var backView = UIView()
    var toolBar = UIToolbar()
    var arrTransactionData:[TransactionData] = []
    var arrFilterTransaction:[TransactionData] = []
    var arrIncomeCategory:[String] = []
    var arrExpensesCategory:[String] = []
    var arrExpensesByCat:[OverviewIncomeExpenseDate] = []
    var arrIcomeByCat:[OverviewIncomeExpenseDate] = []
    var arrAccountData:[AccountData] = []
    var isBudget = false
    var totalExpense = 0.0
    var totalIncome = 0.0
      
    @IBOutlet weak var lblAccount: UILabel!
    
    var arrAccount = ["Justin", "ABC"]
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
      
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getAccount()
          getData()
       
    }
    // MARK :- Set color
    func setUI() {
       // getAccount()
        self.lblCalenderType.textColor = textWhiteColor
        self.lblRemainingBalance.textColor = textWhiteColor
        self.lblTitleBalance.textColor = textWhiteColor
        imgHeader.image = UIImage(named: "mainHeder")
        self.view.backgroundColor = bgGrayColor
        self.lblRemainingBalance.text = ""
    
    }
    // MARK:-
    // MARK: Button Action
    @IBAction func btnAddTransactionaction(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "AddExpensesVC") as! AddExpensesVC
       
            next.isIncome = true

        
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    @IBAction func btnAddExpensesaction(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "AddExpensesVC") as! AddExpensesVC
            next.isIncome = false
        self.navigationController?.pushViewController(next, animated: true)
 
    }
    
    // MARK:- Get Account
    func getAccount() {
        DatabaseManager.shared.getAccount(withParametrs: [:]) { (success, message, arrData) in
            if success == true {
                self.arrAccountData = arrData
                let obj = AccountData(strId: "0", strName: "All Account", strChartColor: "", intHideFuturetra: 0, intTimePeriod: 3, intIsCarryoveron: 0, intIsPositiveonly: 0, intIsBudgeton: 0, doAmount: 0.0, intIsIncludeincome: 0)
                self.arrAccountData.insert(obj, at: 0)
                
                if currentSelectedAccount == "0" {
                    self.lblAccount.text = "All Account"
                }else {
                    if let index = Int(currentSelectedAccount){
                        let selectaccount = self.arrAccountData.filter { $0.id.lowercased().contains((currentSelectedAccount.lowercased()))}
                        if selectaccount.count > 0 {
                             self.lblAccount.text = selectaccount[0].name!
                        }else {
                            currentSelectedAccount = "0"
                             self.lblAccount.text = "All Account"
                        }
                       //self.arrAccountData[index].name!
                        //currentSelectedAccount
                    }
                }
            }else {
                
            }
        }
    }
    // MARK:- Get Data CalenderType Like Daily Monthly Weekly Yearly
    func getData() {
        if cuurrentCalenderType == 1 {
            getCalenderTypeDay()
        }else if cuurrentCalenderType == 2  {
            getCalenderTypetWeek()
        }else if cuurrentCalenderType == 3  {
            getCalenderTypeMoths()
        }else if cuurrentCalenderType == 4  {
            getCalenderTypetYear()
        }
    }
    func getNextCalenderData() {
        if cuurrentCalenderType == 1 {
            getCalenderNextDay()
        }else if cuurrentCalenderType == 2  {
            getCalenderNextWeek()
        }else if cuurrentCalenderType == 3  {
            getCalenderNextMoths()
        }else if cuurrentCalenderType == 4  {
            getCalenderNextYear()
        }
    }
    func getPreviousCalenderData() {
        if cuurrentCalenderType == 1 {
            getCalenderPreDay()
        }else if cuurrentCalenderType == 2  {
            getCalenderPreWeek()
        }else if cuurrentCalenderType == 3  {
            getCalenderPreMoths()
        }else if cuurrentCalenderType == 4  {
            getCalenderPreYear()
        }
    }
    func getCalenderTypeDay() {
        self.btnNext.isUserInteractionEnabled = true
        let calType = currentCalenderDay()
        self.lblCalenderType.text = calType.0
        getTrasnactionDay(strDate: calType.1)
        
        
        
        if currentSelectedAccount == "0" {
            getIncomeOneDay(strDate: calType.1)
            getExpenseOneDay(strDate: calType.1)
            getTotalExpnseCategoryOneDay(strDate: calType.1)
            getTotalIncomeCategoryOneDay(strDate: calType.1)
            self.checkHideTra()
            
        }else {
            getExpenseAmountbyAccountOnDay(strDate: calType.1, strAccId: currentSelectedAccount)
            getIncomeAmountbyAccountOnDay(strDate: calType.1, strAccId: currentSelectedAccount)
            getOneDayExpensesCatByAccount(strAccId: currentSelectedAccount, strDate: calType.1)
            getOneDayIncomeCatByAccount(strAccountId: currentSelectedAccount, strDate: calType.1)
            self.checkHideTra()
            
        }
    }
    func getCalenderTypeMoths() {
        self.btnNext.isUserInteractionEnabled = true
        let calType = currentCalenderMonth()
        self.lblCalenderType.text = calType.0
        //getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
        
        //getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
        //gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
        if currentSelectedAccount == "0" {
            getTotalExpense(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncome(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
            gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
        }else {
            getExpensebyAccount(strStratDate: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getIncomeByAccount(strStaratData: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getAllExpenseCatByAccount(strAccId: currentSelectedAccount, strStartData: calType.1, strEndDate: calType.2)
            getTotalIncomeCatByAccount(strAccId:currentSelectedAccount , strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
           
        }
    }
    func getCalenderTypetWeek() {
        self.btnNext.isUserInteractionEnabled = true
        let calType = currentCalenderWeek()
        self.lblCalenderType.text = calType.0
        //getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
       
       // getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
       // gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
        if currentSelectedAccount == "0" {
            getTotalExpense(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncome(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
            gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
        }else {
            getExpensebyAccount(strStratDate: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getIncomeByAccount(strStaratData: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getAllExpenseCatByAccount(strAccId: currentSelectedAccount, strStartData: calType.1, strEndDate: calType.2)
            getTotalIncomeCatByAccount(strAccId:currentSelectedAccount , strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
        }
    }
    func getCalenderTypetYear() {
        self.btnNext.isUserInteractionEnabled = true
        let calType = currentCalenderYear()
        self.lblCalenderType.text = calType.0
         //getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
       // getTotalIncome(strStartDate: calType.1, strEndDate: calType.2)
       // getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
        gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
        if currentSelectedAccount == "0" {
            getTotalExpense(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncome(strStartDate: calType.1, strEndDate: calType.2)
             getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
             gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
          self.checkHideTra()
            
        }else {
            getExpensebyAccount(strStratDate: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getIncomeByAccount(strStaratData: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)

            getAllExpenseCatByAccount(strAccId: currentSelectedAccount, strStartData: calType.1, strEndDate: calType.2)
            getTotalIncomeCatByAccount(strAccId:currentSelectedAccount , strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
        }
    }
    func getCalenderNextDay() {
        let calType = nextCalenderDay()
        if calType.2 == true {
            self.btnNext.isUserInteractionEnabled = false
        }else {
            self.btnNext.isUserInteractionEnabled = true
        }
        self.lblCalenderType.text = calType.0
        getTrasnactionDay(strDate: calType.1)
       
       // getTotalExpnseCategoryOneDay(strDate: calType.1)
       // getTotalIncomeCategoryOneDay(strDate: calType.1)
        if currentSelectedAccount == "0" {
            getIncomeOneDay(strDate: calType.1)
            getExpenseOneDay(strDate: calType.1)
            getTotalExpnseCategoryOneDay(strDate: calType.1)
            getTotalIncomeCategoryOneDay(strDate: calType.1)
            self.checkHideTra()
        }else {
            getExpenseAmountbyAccountOnDay(strDate: calType.1, strAccId: currentSelectedAccount)
            getIncomeAmountbyAccountOnDay(strDate: calType.1, strAccId: currentSelectedAccount)
            getOneDayExpensesCatByAccount(strAccId: currentSelectedAccount, strDate: calType.1)
            getOneDayIncomeCatByAccount(strAccountId: currentSelectedAccount, strDate: calType.1)
            self.checkHideTra()
        }
    }
    func getCalenderNextMoths() {
        let calType = nextCalenderMonth()
        if calType.3 == true {
            self.btnNext.isUserInteractionEnabled = false
        }else {
            self.btnNext.isUserInteractionEnabled = true
        }
        self.lblCalenderType.text = calType.0
        //getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
       
        //getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
        //gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
        
        if currentSelectedAccount == "0" {
            getTotalExpense(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncome(strStartDate: calType.1, strEndDate: calType.2)
             getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
            gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
           
        }else {
            getExpensebyAccount(strStratDate: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getIncomeByAccount(strStaratData: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getAllExpenseCatByAccount(strAccId: currentSelectedAccount, strStartData: calType.1, strEndDate: calType.2)
            getTotalIncomeCatByAccount(strAccId:currentSelectedAccount , strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
        }
    }
    func getCalenderNextWeek() {
        let calType = nextCalenderWeek()
        if calType.3 == true {
            self.btnNext.isUserInteractionEnabled = false
        }else {
            self.btnNext.isUserInteractionEnabled = true
        }
        self.lblCalenderType.text = calType.0
        //getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
        
       // getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
       // gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
        if currentSelectedAccount == "0" {
            getTotalExpense(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncome(strStartDate: calType.1, strEndDate: calType.2)
             getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
            gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
        }else {
            getExpensebyAccount(strStratDate: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getIncomeByAccount(strStaratData: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getAllExpenseCatByAccount(strAccId: currentSelectedAccount, strStartData: calType.1, strEndDate: calType.2)
            getTotalIncomeCatByAccount(strAccId:currentSelectedAccount , strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
        }
    }
    func getCalenderNextYear() {
        let calType = nextCalenderYear()
        if calType.3 == true {
            self.btnNext.isUserInteractionEnabled = false
        }else {
            self.btnNext.isUserInteractionEnabled = true
        }
        self.lblCalenderType.text = calType.0
        //getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
      
        //getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
        //gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
        if currentSelectedAccount == "0" {
            getTotalExpense(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncome(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
            gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
        }else {
            getExpensebyAccount(strStratDate: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getIncomeByAccount(strStaratData: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getAllExpenseCatByAccount(strAccId: currentSelectedAccount, strStartData: calType.1, strEndDate: calType.2)
            getTotalIncomeCatByAccount(strAccId:currentSelectedAccount , strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
        }
    }
    func getCalenderPreDay() {
        self.btnNext.isUserInteractionEnabled = true
          let calType = previousCalenderDay()
          self.lblCalenderType.text = calType.0
          getTrasnactionDay(strDate: calType.1)
          
          //getTotalExpnseCategoryOneDay(strDate: calType.1)
          //getTotalIncomeCategoryOneDay(strDate: calType.1)
        if currentSelectedAccount == "0" {
            getIncomeOneDay(strDate: calType.1)
            getExpenseOneDay(strDate: calType.1)
            getTotalExpnseCategoryOneDay(strDate: calType.1)
            getTotalIncomeCategoryOneDay(strDate: calType.1)
            self.checkHideTra()
        }else {
            getExpenseAmountbyAccountOnDay(strDate: calType.1, strAccId: currentSelectedAccount)
            getIncomeAmountbyAccountOnDay(strDate: calType.1, strAccId: currentSelectedAccount)
            getOneDayExpensesCatByAccount(strAccId: currentSelectedAccount, strDate: calType.1)
            getOneDayIncomeCatByAccount(strAccountId: currentSelectedAccount, strDate: calType.1)
            self.checkHideTra()
            
        }
      }
      func getCalenderPreMoths() {
        self.btnNext.isUserInteractionEnabled = true
          let calType = previousCalenderMonth()
          self.lblCalenderType.text = calType.0
          //getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
          
         // getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
         // gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
        
        if currentSelectedAccount == "0" {
            getTotalExpense(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncome(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
            gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
        }else {
            getExpensebyAccount(strStratDate: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getIncomeByAccount(strStaratData: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getAllExpenseCatByAccount(strAccId: currentSelectedAccount, strStartData: calType.1, strEndDate: calType.2)
            getTotalIncomeCatByAccount(strAccId:currentSelectedAccount , strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
        }
      }
      func getCalenderPreWeek() {
            self.btnNext.isUserInteractionEnabled = true
          let calType = previousCalenderWeek()
          self.lblCalenderType.text = calType.0
          //getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
         
          //getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
          //gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
        if currentSelectedAccount == "0" {
            getTotalExpense(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncome(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
            gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
           self.checkHideTra()
        }else {
            getExpensebyAccount(strStratDate: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getIncomeByAccount(strStaratData: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getAllExpenseCatByAccount(strAccId: currentSelectedAccount, strStartData: calType.1, strEndDate: calType.2)
            getTotalIncomeCatByAccount(strAccId:currentSelectedAccount , strStartDate: calType.1, strEndDate: calType.2)
           self.checkHideTra()
        }
      }
      func getCalenderPreYear() {
         self.btnNext.isUserInteractionEnabled = true
          let calType = previousCalenderYear()
          self.lblCalenderType.text = calType.0
          //getTransactionCaltype(strStartDate: calType.1, strEndDate: calType.2)
          
         // getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
         // gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
        if currentSelectedAccount == "0" {
            getTotalExpense(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncome(strStartDate: calType.1, strEndDate: calType.2)
            getTotalIncomeCategory(strStartDate: calType.1, strEndDate: calType.2)
            gettotalExpenseCategory(strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
        }else {
            getExpensebyAccount(strStratDate: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getIncomeByAccount(strStaratData: calType.1, strEndDate: calType.2, strAccId: currentSelectedAccount)
            getAllExpenseCatByAccount(strAccId: currentSelectedAccount, strStartData: calType.1, strEndDate: calType.2)
            getTotalIncomeCatByAccount(strAccId:currentSelectedAccount , strStartDate: calType.1, strEndDate: calType.2)
            self.checkHideTra()
            
        }
      }
     func getTrasnactionDay(strDate:String) {
        DatabaseManager.shared.getTransactionType(withParametrs:strDate, strType: "day") { (success, message,arrData) in
            if success == true {
                //self.isFilter = false
                self.arrTransactionData = arrData
                //self.tblTransaction.reloadData()
            }else {
                
            }
        }
    }
    
//MARK :- Check Hide Tranaction
    func checkHideTra() {
        if currentSelectedAccount == "0" {
            let isOn = UserDefaults.standard.value(forKey: strAllAccountHide) as! Int
            if isOn == 1 {
               if currentCalender > 0 {
                   //self.btnNext.isUserInteractionEnabled = false
               }else {
                   //self.btnNext.isUserInteractionEnabled = true
               }
            }else {
                //self.btnNext.isUserInteractionEnabled = true
            }
        }else {
             let arrData = arrAccountData.filter { $0.id.lowercased().contains((currentSelectedAccount.lowercased()))}
            if arrData.count > 0 {
                let obj = arrData[0]
                if obj.hide_futuretra == 1 {
                    if currentCalender > 0 {
                        //self.btnNext.isUserInteractionEnabled = false
                    }else {
                        //self.btnNext.isUserInteractionEnabled = true
                    }
                }else {
                   // self.btnNext.isUserInteractionEnabled = true
                }
            }else {
               // self.btnNext.isUserInteractionEnabled = true
            }
        }
    }
//    func getTransactionCaltype(strStartDate:String,strEndDate:String) {
//        DatabaseManager.shared.getTransactionByCalenderType(withParametrs:strStartDate , strEndDate: strEndDate) { (success, message, arrData) in
//            if success == true {
//               // self.isFilter = false
//                self.arrTransactionData = arrData
//                //self.tblTransaction.reloadData()
//            }else {
//
//            }
//        }
//    }
    
    // MARK GetTotal Expense
    
    func getTotalExpense(strStartDate:String,strEndDate:String) {
        DatabaseManager.shared.getTotalExpense(withParametrs: strStartDate, strEndDate: strEndDate, type: "1") { (success,message , arrData) in
            if success == true {
                self.totalExpense = arrData
                self.tblData.reloadData()
                print(message)
            }else {
                print(message)
            }
        }
    }
    
    // MARK: get Total Income
    func getTotalIncome(strStartDate:String,strEndDate:String) {
//        DatabaseManager.shared.getTotalExpense(withParametrs: strStartDate, strEndDate: strEndDate, type: "2") { (success,message , arrData) in
//            if success == true {
//                self.totalIncome = arrData
//                let remaing = self.totalIncome - self.totalExpense
//                 let finalAmount = "\(remaing.clean)"
//                self.lblRemainingBalance.text = currentCurrencySymbols + " " + "\(finalAmount)"
//                self.tblData.reloadData()
//                print(message)
//            }else {
//                print(message)
//            }
//        }
        var tempIncome = 0.0

        for i in self.arrAccountData {
           if i.id != "0" {
            if i.is_budgeton == 1 {
                self.isBudget = true
                if i.is_includeincome == 1 {
                    DatabaseManager.shared.getTotalExpenseByAccount(withParametrs: strStartDate, strEndDate: strEndDate, type: "2", strAccId: i.id!) { (success, message, amount) in
                        if success == true {
                            tempIncome = tempIncome + amount + i.amount
                        }
                    }
                }else {
                    tempIncome = tempIncome + i.amount
                }
            }else {
                    DatabaseManager.shared.getTotalExpenseByAccount(withParametrs: strStartDate, strEndDate: strEndDate, type: "2", strAccId: i.id!) { (success, message, amount) in
                        if success == true {
                            tempIncome = tempIncome + amount
                        }
                    }
                }
            }
        }
        self.totalIncome = tempIncome
        let remaing = self.totalIncome - self.totalExpense
        let finalAmount = "\(remaing.clean)"
        self.lblRemainingBalance.text = currentCurrencySymbols + " " + "\(finalAmount)"
        self.tblData.reloadData()
    }
    // MARK;- OneDay Expense
    func getExpenseOneDay(strDate:String) {
        
        DatabaseManager.shared.getTotalExpenseOneDay(withParametrs: strDate, type: "1") { (success, message, amout) in
            if success == true {
                self.totalExpense = amout
                self.tblData.reloadData()
            }else {
                
            }
        }
    }
    // MARK;- OneDay Income
    func getIncomeOneDay(strDate:String) {
        
//        DatabaseManager.shared.getTotalExpenseOneDay(withParametrs: strDate, type: "2") { (success, message, amout) in
//            if success == true {
//                self.totalIncome = amout
//                let remaing = self.totalIncome - self.totalExpense
//                let finalAmount = "\(remaing.clean)"//String(format:"%.0f", remaing)
//                self.lblRemainingBalance.text = currentCurrencySymbols + " " + "\(finalAmount)"
//                self.tblData.reloadData()
//                self.tblData.reloadData()
//            }else {
//
//            }
//        }
        var tempIncome = 0.0

               for i in self.arrAccountData {
                  if i.id != "0" {
                   if i.is_budgeton == 1 {
                       self.isBudget = true
                       if i.is_includeincome == 1 {
                           DatabaseManager.shared.getTotalExpenseOneDaybyAccount(withParametrs: strDate, type: "2", strAccId: i.id) { (success, message, amount) in
                               if success == true {
                                tempIncome = tempIncome + amount + i.amount
                               }
                           }
                           }
                       }else {
                          // tempIncome = tempIncome + i.amount
                        DatabaseManager.shared.getTotalExpenseOneDaybyAccount(withParametrs: strDate, type: "2", strAccId: i.id) { (success, message, amount) in
                        if success == true {
                         tempIncome = tempIncome + amount //+ i.amount
                        }
                    }
                       }
                   }else {
                          DatabaseManager.shared.getTotalExpenseOneDaybyAccount(withParametrs: strDate, type: "2", strAccId: i.id) { (success, message, amount) in
                              if success == true {
                                   tempIncome = tempIncome + amount
                              }
                          }
                       }
                   }
            self.totalIncome = tempIncome
               let remaing = self.totalIncome - self.totalExpense
               let finalAmount = "\(remaing.clean)"//String(format:"%.0f", remaing)
               self.lblRemainingBalance.text = currentCurrencySymbols + " " + "\(finalAmount)"
               self.tblData.reloadData()
    }
    // MARK;-  get Expense
    func getExpenseAmountbyAccountOnDay(strDate:String,strAccId:String) {
        
        DatabaseManager.shared.getTotalExpenseOneDaybyAccount(withParametrs: strDate, type: "1", strAccId: strAccId) { (success, message, amount) in
            if success == true {
                self.totalExpense = amount
                self.tblData.reloadData()
            }else {
                
            }
        }
    }
    // MARK;-  get Income
    func getIncomeAmountbyAccountOnDay(strDate:String,strAccId:String) {
        var tempIncome = 0.0
        let arrData = arrAccountData.filter { $0.id.lowercased().contains((strAccId.lowercased()))}
        let obj = arrData[0]
        if obj.is_budgeton == 1 {
            self.isBudget = true
            if obj.is_includeincome == 1 {
                DatabaseManager.shared.getTotalExpenseOneDaybyAccount(withParametrs: strDate, type: "2", strAccId: strAccId) { (success, message, amount) in
                    if success == true {
                        tempIncome = tempIncome + amount + obj.amount
                    }else {
                               
                    }
                }

            }else {
                //tempIncome = tempIncome + obj.amount!
                DatabaseManager.shared.getTotalExpenseOneDaybyAccount(withParametrs: strDate, type: "2", strAccId: strAccId) { (success, message, amount) in
                    if success == true {
                        tempIncome = tempIncome + amount + obj.amount
                    }else {
                               
                    }
                }

            }
        }else {
            DatabaseManager.shared.getTotalExpenseOneDaybyAccount(withParametrs: strDate, type: "2", strAccId: strAccId) { (success, message, amount) in
                if success == true {
                    tempIncome = tempIncome + amount
                }else {
                           
                }
            }
        }
        self.totalIncome = tempIncome
        let remaing = self.totalIncome - self.totalExpense
        let finalAmount = "\(remaing.clean)"//String(format:"%.0f", remaing)
        self.lblRemainingBalance.text = currentCurrencySymbols + " " + "\(finalAmount)"
        self.tblData.reloadData()
    }
    func getExpensebyAccount(strStratDate:String,strEndDate:String,strAccId:String) {
        
        DatabaseManager.shared.getTotalExpenseByAccount(withParametrs: strStratDate, strEndDate: strEndDate, type: "1", strAccId: strAccId) { (success, message, amount) in
            if success == true {
                self.totalExpense = amount
                self.tblData.reloadData()
            }else {
                
            }
        }
        
    }
    // MARK;-  get Income
    func getIncomeByAccount(strStaratData:String,strEndDate:String,strAccId:String) {
        
        var tempIncome = 0.0
        let arrData = arrAccountData.filter { $0.id.lowercased().contains((strAccId.lowercased()))}
        let obj = arrData[0]
        
        if obj.is_budgeton == 1 {
            self.isBudget = true
            if obj.is_includeincome == 1 {
                DatabaseManager.shared.getTotalExpenseByAccount(withParametrs: strStaratData, strEndDate: strEndDate, type: "2", strAccId: strAccId) { (success, message, amount) in
                           if success == true {
                            tempIncome = tempIncome + amount + obj.amount
                              // self.tblData.reloadData()
                           }else {
                               
                           }
                       }
            }else {
                 tempIncome = tempIncome + obj.amount!
            }
        }else {
            DatabaseManager.shared.getTotalExpenseByAccount(withParametrs: strStaratData, strEndDate: strEndDate, type: "2", strAccId: strAccId) { (success, message, amount) in
                       if success == true {
                           tempIncome = tempIncome + amount
                       }else {
                           
                       }
                   }
        }
        
        self.totalIncome = tempIncome
        let remaing = self.totalIncome - self.totalExpense
        let finalAmount = "\(remaing.clean)"//String(format:"%.0f", remaing)
        self.lblRemainingBalance.text = currentCurrencySymbols + " " + "\(finalAmount)"
        self.tblData.reloadData()
       
    }
    // MARK;-  get Expense
    func gettotalExpenseCategory(strStartDate:String,strEndDate:String) {
        DatabaseManager.shared.getTotalExapenseCategory(withParametrs: strStartDate, strEndDate: strEndDate, type: "1") { (success, message, arrData) in
            if success == true {
                self.arrExpensesCategory = arrData
                self.arrExpensesByCat.removeAll()
                for i in self.arrExpensesCategory {
                    self.getTotalExpenseByCat(strDate:strStartDate , strEndate: strEndDate, strCatName: i, strAccID: "")
                }
            }else {
                self.arrExpensesByCat.removeAll()
                self.tblData.reloadData()
            }
        }
    }
    // MARK;-  get Income
    func getTotalIncomeCategory(strStartDate:String,strEndDate:String) {
        DatabaseManager.shared.getTotalExapenseCategory(withParametrs: strStartDate, strEndDate: strEndDate, type: "2") { (success, message, arrData) in
            if success == true {
                self.arrIncomeCategory = arrData
                self.arrIcomeByCat.removeAll()
                for i in self.arrIncomeCategory {
                    self.getTotalIncomeByCat(strDate:strStartDate , strEndate: strEndDate, strCatName: i, strAccID: "")
                }
            }else {
                self.arrIcomeByCat.removeAll()
                self.tblData.reloadData()
            }
        }
        
    }
    // MARK;-  get Expense
    func getTotalExpnseCategoryOneDay(strDate:String) {
        var strAccId = ""
        if currentSelectedAccount == "0" {
            
        }else {
            strAccId = currentSelectedAccount
        }
        DatabaseManager.shared.getTotalExpenseCategoryOneDay(withParametrs: strDate, type: "1", strAccID: strAccId) { (success, message, arrData) in
            if success == true {
                self.arrExpensesCategory = arrData
                self.arrExpensesByCat.removeAll()
                for i in self.arrExpensesCategory {
                    self.getTotalExpenseOnDayByCat(strDate:strDate , strCatName: i, strAccId: strAccId)
                }
                
            }else {
                 self.arrExpensesByCat.removeAll()
                
            }
        }
        self.tblData.reloadData()
    }
    // MARK;-  get Income
    func getTotalIncomeCategoryOneDay(strDate:String) {
        var strAccId = ""
        if currentSelectedAccount == "0" {
            
        }else {
            strAccId = currentSelectedAccount
        }
        DatabaseManager.shared.getTotalExpenseCategoryOneDay(withParametrs: strDate, type: "2", strAccID: strAccId) { (success, message, arrData) in
            if success == true {
                self.arrIncomeCategory = arrData
                self.arrIcomeByCat.removeAll()
                for i in self.arrIncomeCategory {
                    self.getTotalIncomeOnDayByCat(strDate:strDate , strCatName: i, strAccId: "")
                }
            }else {
                self.arrIcomeByCat.removeAll()
               
            }
        }
         self.tblData.reloadData()
        
    }
     // MARK:- get expenses category by account one day
    
    func getOneDayExpensesCatByAccount(strAccId:String,strDate:String) {
                DatabaseManager.shared.getTotalExpenseCategoryByaccountOneDay(withParametrs: strDate, accId: strAccId, type: "1") { (success, message, arrData) in
            if success == true {
                self.arrExpensesCategory = arrData
                self.arrExpensesByCat.removeAll()
                 for i in self.arrExpensesCategory {
                    self.getTotalExpenseOnDayByCat(strDate:strDate , strCatName: i, strAccId: strAccId)
                 }
            }else {
                self.arrExpensesByCat.removeAll()
                self.tblData.reloadData()
            }
        }
    }
    // MARK:- get expenses category by account
    func getAllExpenseCatByAccount(strAccId:String,strStartData:String,strEndDate:String) {
        DatabaseManager.shared.getTotalExapenseCategoryByAccount(withParametrs: strStartData, strEndDate: strEndDate, accId: strAccId, type: "1") { (success, message, arrData) in
            if success == true {
                self.arrExpensesCategory = arrData
                self.arrExpensesByCat.removeAll()
                for i in self.arrExpensesCategory {
                    self.getTotalExpenseByCat(strDate:strStartData , strEndate: strEndDate, strCatName: i, strAccID: strAccId)
                }
            }else {
                self.arrExpensesByCat.removeAll()
                self.tblData.reloadData()
            }
        }
    }
    // MARK:- get income category by account one day
    func getOneDayIncomeCatByAccount(strAccountId:String,strDate:String) {
        DatabaseManager.shared.getTotalExpenseCategoryByaccountOneDay(withParametrs: strDate, accId: strAccountId, type: "2") { (success, message, arrData) in
            if success == true {
                self.arrIncomeCategory = arrData
                self.arrIcomeByCat.removeAll()
                for i in self.arrIncomeCategory {
                    self.getTotalIncomeOnDayByCat(strDate:strDate , strCatName: i, strAccId: strAccountId)
                }
            }else {
                self.arrIcomeByCat.removeAll()
                self.tblData.reloadData()
            }
        }
    }
    
    // MARK:- get income category by account
    func getTotalIncomeCatByAccount(strAccId:String,strStartDate:String,strEndDate:String) {
        
        DatabaseManager.shared.getTotalExapenseCategoryByAccount(withParametrs: strStartDate, strEndDate: strEndDate, accId: strAccId, type: "2") { (success, message, arrData) in
            if success == true {
                self.arrIncomeCategory = arrData
                self.arrIcomeByCat.removeAll()
                for i in self.arrIncomeCategory {
                    self.getTotalIncomeByCat(strDate:strStartDate , strEndate: strEndDate, strCatName: i, strAccID: strAccId)
                }
            }else {
                self.arrIcomeByCat.removeAll()
                self.tblData.reloadData()
            }
        }
//        var tempIncome = 0.0
//
//               for i in self.arrAccountData {
//                  if i.id != "0" {
//                   if i.is_budgeton == 1 {
//                       self.isBudget = true
//                       if i.is_includeincome == 1 {
//                           DatabaseManager.shared.getTotalExpenseByAccount(withParametrs: strStartDate, strEndDate: strEndDate, type: "2", strAccId: i.id!) { (success, message, amount) in
//                               if success == true {
//                                   tempIncome = tempIncome + amount
//                               }
//                           }
//                       }else {
//                           tempIncome = tempIncome + i.amount
//                       }
//                   }else {
//                           DatabaseManager.shared.getTotalExpenseByAccount(withParametrs: strStartDate, strEndDate: strEndDate, type: "2", strAccId: i.id!) { (success, message, amount) in
//                               if success == true {
//                                   tempIncome = tempIncome + amount
//                               }
//                           }
//                       }
//                   }
//               }
//               self.totalIncome = tempIncome
//               let remaing = self.totalIncome - self.totalExpense
//               let finalAmount = "\(remaing.clean)"
//               self.lblRemainingBalance.text = currentCurrencySymbols + " " + "\(finalAmount)"
//               self.tblData.reloadData()
    }
    
    
    // MARK:-  get Income category
    func getTotalIncomeOnDayByCat(strDate:String,strCatName:String,strAccId:String) {
        
        DatabaseManager.shared.getTotalIncomeOneDaybyCategory(withParametrs: strDate, type: strCatName, strAccId: strAccId) { (success, message, amount) in
            if success == true {
                let pre = amount * 100
                let finalPre = pre / self.totalIncome
                let strFinalePre = String.localizedStringWithFormat("%.0f %@", finalPre,"")
                let obj = OverviewIncomeExpenseDate(name: strCatName, intAmount: amount, strPre: strFinalePre)
                self.arrIcomeByCat.append(obj)
                self.tblData.reloadData()
            }else {
                
            }
        }
    }
    // MARK:-  get expense category
    func getTotalExpenseOnDayByCat(strDate:String,strCatName:String,strAccId:String) {
        
        DatabaseManager.shared.getTotalExpenseOneDaybyCategory(withParametrs: strDate, type: strCatName, strAccId: strAccId) { (success, message, amount) in
            if success == true {
                let pre = amount * 100
                let finalPre = pre / self.totalExpense
                let strFinalePre = String.localizedStringWithFormat("%.0f %@", finalPre,"")
                let obj = OverviewIncomeExpenseDate(name: strCatName, intAmount: amount, strPre: strFinalePre)
                self.arrExpensesByCat.append(obj)
                self.tblData.reloadData()
            }else {
                 self.tblData.reloadData()
            }
        }
    }
    // MARK:-  get expense category
    func getTotalExpenseByCat(strDate:String,strEndate:String,strCatName:String,strAccID:String) {
        
        DatabaseManager.shared.getTotalExpenseByCategory(withParametrs: strDate, strEndDate: strEndate, type: strCatName, accId: strAccID) { (success, message, amount) in
            if success == true {
                let pre = amount * 100
                let finalPre = pre / self.totalExpense
                let strFinalePre = String.localizedStringWithFormat("%.0f %@", finalPre,"")
                let obj = OverviewIncomeExpenseDate(name: strCatName, intAmount: amount, strPre: strFinalePre)
                self.arrExpensesByCat.append(obj)
                self.tblData.reloadData()
            }else {
                 self.tblData.reloadData()
            }
        }
    }
    // MARK:-  get Income category
    func getTotalIncomeByCat(strDate:String,strEndate:String,strCatName:String,strAccID:String) {
        
        DatabaseManager.shared.getTotalIncomeByCategory(withParametrs: strDate, strEndDate: strEndate, type: strCatName, strAccID: strAccID) { (success, message, amount) in
            if success == true {
                let pre = amount * 100
                let finalPre = pre / self.totalIncome
                let strFinalePre = String.localizedStringWithFormat("%.0f %@", finalPre,"")
                let obj = OverviewIncomeExpenseDate(name: strCatName, intAmount: amount, strPre: strFinalePre)
                self.arrIcomeByCat.append(obj)
                self.tblData.reloadData()
            }else {
                
            }
        }
    }
    // MARK: Button Action
    @IBAction func btnPreviousCalenderaction(_ sender: UIButton) {
        getPreviousCalenderData()
    }
    @IBAction func btnCalenderaction(_ sender: UIButton) {
        actionShhet()
    }
    
    @IBAction func btnSeclectAccountaction(_ sender: UIButton) {
        pickerCountryCodeOpen()
    }
    
    @IBAction func btnNextCalenderAction(_ sender: UIButton) {
        getNextCalenderData()
    }
    // MARK:-  action Sheet
    func actionShhet() {
        let optionMenu = UIAlertController(title: nil, message: "Show Expenses", preferredStyle: .actionSheet)

        let dailyAction = UIAlertAction(title: "Daily", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            UserDefaults.standard.set(1, forKey: strCalenderType)
            UserDefaults.standard.synchronize()
            currentCalender = 0
            if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
                cuurrentCalenderType = calType
            }
            self.getData()
           
        })

        let weeklyAction = UIAlertAction(title: "Weekly", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            UserDefaults.standard.set(2, forKey: strCalenderType)
            UserDefaults.standard.synchronize()
            currentCalender = 0
            if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
                cuurrentCalenderType = calType
            }
            self.getData()
        })

        let monthlyAction = UIAlertAction(title: "Monthly", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            UserDefaults.standard.set(3, forKey: strCalenderType)
            UserDefaults.standard.synchronize()
            currentCalender = 0
            if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
                cuurrentCalenderType = calType
            }
            self.getData()
           
        })
        let yearlyAction = UIAlertAction(title: "Yearly", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            UserDefaults.standard.set(4, forKey: strCalenderType)
            UserDefaults.standard.synchronize()
            currentCalender = 0
            if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
                cuurrentCalenderType = calType
            }
            self.getData()
           
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
           
        })
        optionMenu.addAction(dailyAction)
        optionMenu.addAction(weeklyAction)
        optionMenu.addAction(monthlyAction)
        optionMenu.addAction(yearlyAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    //MARK:- Picker
    func pickerCountryCodeOpen(){
            accountPicker = UIPickerView.init()
            backView = UIView()
            backView.frame = self.view.bounds
            backView.backgroundColor = UIColor.black
            backView.alpha = 0.2
           // backView.isUserInteractionEnabled = false
            self.view.addSubview(backView)
            accountPicker.delegate = self
            accountPicker.backgroundColor = textWhiteColor
            accountPicker.setValue(UIColor.black, forKey: "textColor")
            accountPicker.autoresizingMask = .flexibleWidth
            accountPicker.contentMode = .center
            accountPicker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 340, width: UIScreen.main.bounds.size.width, height: 300)
            self.view.addSubview(accountPicker)

            toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 340, width: UIScreen.main.bounds.size.width, height: 40))
            toolBar.barStyle = .default
            toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
            
            toolBar.backgroundColor = textWhiteColor
            self.view.addSubview(toolBar)
        }
    
        //MARK: ToolBar Done Button Tap Action
        @objc func onDoneButtonTapped() {
            backView.removeFromSuperview()
            toolBar.removeFromSuperview()
            accountPicker.removeFromSuperview()
            let index:Int = self.accountPicker.selectedRow(inComponent: 0)
                self.lblAccount.text = self.arrAccountData[index].name!
                currentSelectedAccount = self.arrAccountData[index].id
                UserDefaults.standard.set(currentSelectedAccount, forKey: strCurrentSelectedAcoount)
                self.getData()
        }
        
       

    //MARK: PickerView Delegate and DataSource Method
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK:-  Table view
extension OverviewVC:UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView.tag == 11 {
             return 3
        }else {
            return 1
        }
       
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 11 {
            if section == 0 {
                return 1
            }else if section == 1{
                
                if self.arrIcomeByCat.count > 0 {
                    return 1
                }else {
                    return 0
                }
            }else {
                if self.arrExpensesByCat.count > 0 {
                    return 1
                }else {
                    return 0
                }
            }
        }else {
            
            if tableView.tag ==  2 {
                return self.arrExpensesByCat.count
            }else if tableView.tag == 1 {
                return self.arrIcomeByCat.count
                
            }else {
                return 0
            }
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 11 {
            if indexPath.section == 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "OverviewIncomeCell") as! OverviewIncomeCell
              //  if indexPath.row == 0 {
                if self.isBudget == true {
                     cell.lblTitle.text = "Budget"
                }else {
                     cell.lblTitle.text = "Income"
                }
                   
                    cell.lblAmount.textColor = incomeGreen
                    let finalAmount = "\(self.totalIncome.clean)" //String(format:"%.0f", self.totalIncome)
                    cell.lblAmount.text =  currentCurrencySymbols + " " + "\(finalAmount)"
                     let finalExpAmount = "\(self.totalExpense.clean)"
                cell.lblExpAmount.text = currentCurrencySymbols +  " " + "\(finalExpAmount)"
                cell.lblExpense.text = "Expenses"
                cell.lblExpAmount.textColor = expenseRed
                cell.expLine.backgroundColor = expenseRed
                cell.incomeLine.backgroundColor = incomeGreen
                let total = self.totalIncome + self.totalExpense
               
               
                if self.totalIncome == 0.0 && self.totalExpense == 0.0 {
                    cell.expHeight.constant = 100
                }else {
                    let pre = self.totalExpense * 100
                    let finalPre = pre / total
                    let finalHeight = finalPre * 2
                     cell.expHeight.constant = CGFloat(finalHeight)
                }
                
//                }else {
//                    cell.lblTitle.text = "Expenses"
//                    let finalAmount = "\(self.totalExpense.clean)"//String(format:"%.0f", self.totalExpense)
//                    cell.lblAmount.text = "$ " + "\(finalAmount)"
//                    cell.lblAmount.textColor = expenseRed
//                }
                cell.btnAdd.tag = indexPath.row
                return cell
            }else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OverviewExpensesCell") as! OverviewExpensesCell
                
                cell.tblCategory.dataSource = self
                cell.tblCategory.delegate = self
                cell.tblCategory.tag = indexPath.section
                cell.tblHeight.constant = CGFloat(self.arrIcomeByCat.count * 60)
                cell.lblTitle.text = "Income Overview"
                cell.tblCategory.reloadData()
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OverviewExpensesCell") as! OverviewExpensesCell
                
                cell.tblCategory.dataSource = self
                cell.tblCategory.delegate = self
                cell.tblCategory.tag = indexPath.section
                
                cell.tblHeight.constant = CGFloat(self.arrExpensesByCat.count * 60)
                cell.lblTitle.text = "Expense Overview"
                cell.tblCategory.reloadData()
                return cell
            }
        }else {
            if tableView.tag == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OverviewIncomeExpenseCategoryCell") as! OverviewIncomeExpenseCategoryCell
                let obj = self.arrExpensesByCat[indexPath.row]
                let progresValue = (obj.percentage as NSString).floatValue
                let finalValue = progresValue / 100
                cell.progress.setProgress(Float(finalValue), animated: true)
                let finalAmount = "\(obj.amount.clean)" //String(format:"%.0f", obj.amount)
                cell.lblCategoryName.text = obj.strName + ": " + currentCurrencySymbols + " " + "\(finalAmount)" //+ " (" + obj.percentage + "%)"
            
                cell.backView.backgroundColor = textWhiteColor
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OverviewIncomeExpenseCategoryCell") as! OverviewIncomeExpenseCategoryCell
                let obj = self.arrIcomeByCat[indexPath.row]
                let progresValue = (obj.percentage as NSString).floatValue
                let finalValue = progresValue / 100
                cell.progress.setProgress(Float(finalValue), animated: true)
                let finalAmount = "\(obj.amount.clean)" //String(format:"%.0f", obj.amount)//obj.amount.rounded(2)
                cell.lblCategoryName.text = obj.strName + ": " + currentCurrencySymbols + " " + "\(finalAmount)" //+ " (" + obj.percentage + "%)"
                cell.backView.backgroundColor = textWhiteColor
                return cell
            }
            
        }
        
    }
}
// MARK:-  PickerViewDelegate
extension OverviewVC: UIPickerViewDataSource, UIPickerViewDelegate {

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrAccountData.count
        
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        if currentSelectedAccount == "0" {
             return self.arrAccountData[row].name
//        }else {
//
//
//            let newIndex = self.arrAccountData.filter{$0.id == currentSelectedAccount}
//            let index = self.arrAccountData.firstIndex(of: newIndex[0])
//            return self.arrAccountData[index!].name
//        }
       
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.lblAccount.text = self.arrAccountData[row].name!
        currentSelectedAccount = self.arrAccountData[row].id
        UserDefaults.standard.set(currentSelectedAccount, forKey: strCurrentSelectedAcoount)
        self.getData()
    }
}


extension Double{
    var clean: String{
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
