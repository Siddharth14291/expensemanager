//
//  CategoriesVC.swift
//  Expenses
//
//
//

import UIKit
import XLPagerTabStrip

class CategoriesVC: ButtonBarPagerTabStripViewController {

    override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "ExpensesCategoriesVC") as! ExpensesCategoriesVC
        let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "IncomeCategoriesVC") as! IncomeCategoriesVC
        
        
        return [vc1, vc2]
    }
    override func viewDidLoad() {
        settings.style.buttonBarItemBackgroundColor = .clear
         settings.style.selectedBarBackgroundColor = UIColor.white
        settings.style.selectedBarBackgroundColor = UIColor.white
         
        
         //self.view.layer.insertSublayer(color , at: 0)
         //UIColor.init(displayP3Red: 9/255.0, green: 25/255.0, blue: 50/255.0, alpha: 1)
         settings.style.buttonBarItemFont = UIFont(name: "Verdana", size: 20)!
         //settings.style.buttonBarItemFont = UIFont.boldSystemFont(ofSize: 16.0)
         settings.style.selectedBarHeight = 2
         settings.style.buttonBarMinimumLineSpacing = 10
         settings.style.buttonBarItemTitleColor = textWhiteColor
         settings.style.buttonBarItemsShouldFillAvailableWidth = true
         settings.style.buttonBarLeftContentInset = 0
         settings.style.buttonBarRightContentInset = 0
         
         changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
             guard changeCurrentIndex == true else { return }
             oldCell?.label.textColor =  UIColor.white
             newCell?.label.textColor = UIColor.white
             newCell?.backgroundColor = UIColor.clear
             oldCell?.backgroundColor = UIColor.clear
             
             
             self.buttonBarView.layer.cornerRadius = 8
             
             self.buttonBarView.layer.borderWidth = 0
             self.buttonBarView.layer.borderColor = UIColor.white.cgColor
             self.buttonBarView.clipsToBounds = true
             
         }
        super.viewDidLoad()
         self.view.backgroundColor = bgGrayColor
    }
}
