//
//  SettingVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 01/02/20.
//

import UIKit

class SettingVC: UIViewController {

    @IBOutlet var btnTimePeriod: UIButton!
    @IBOutlet var btnCurrencySymbol: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = bgGrayColor

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        btnCurrencySymbol.setTitle(currentCurrencySymbols, for: .normal)
//        if cuurrentCalenderType == 1 {
//            self.btnTimePeriod.setTitle("Daily", for: .normal)
//        }else if cuurrentCalenderType == 2 {
//            self.btnTimePeriod.setTitle("Weekly", for: .normal)
//        }else if cuurrentCalenderType == 3 {
//            self.btnTimePeriod.setTitle("Monthly", for: .normal)
//        }else if cuurrentCalenderType ==  4 {
//            self.btnTimePeriod.setTitle("Yearly", for: .normal)
//        }
    }
    
    // MARK: Button Action
    @IBAction func btnTimePeriodaction(_ sender: UIButton) {
        actionShhet()
    }
    
    // MARK :- set Time Period
    func actionShhet() {
        let optionMenu = UIAlertController(title: nil, message: "Show Expenses", preferredStyle: .actionSheet)

        let dailyAction = UIAlertAction(title: "Daily", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            UserDefaults.standard.set(1, forKey: strCalenderType)
            UserDefaults.standard.synchronize()
            currentCalender = 0
            if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
                cuurrentCalenderType = calType
            }
            self.btnTimePeriod.setTitle("Daily", for: .normal)
        })

        let weeklyAction = UIAlertAction(title: "Weekly", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            UserDefaults.standard.set(2, forKey: strCalenderType)
            UserDefaults.standard.synchronize()
            currentCalender = 0
            if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
                cuurrentCalenderType = calType
            }
           self.btnTimePeriod.setTitle("Weekly", for: .normal)
        })

        let monthlyAction = UIAlertAction(title: "Monthly", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            UserDefaults.standard.set(3, forKey: strCalenderType)
            UserDefaults.standard.synchronize()
            currentCalender = 0
            if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
                cuurrentCalenderType = calType
            }
            self.btnTimePeriod.setTitle("Monthly", for: .normal)
           
        })
        let yearlyAction = UIAlertAction(title: "Yearly", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            UserDefaults.standard.set(4, forKey: strCalenderType)
            UserDefaults.standard.synchronize()
            currentCalender = 0
            if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
                cuurrentCalenderType = calType
            }
           self.btnTimePeriod.setTitle("Yearly", for: .normal)
           
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
           
        })
        optionMenu.addAction(dailyAction)
        optionMenu.addAction(weeklyAction)
        optionMenu.addAction(monthlyAction)
        optionMenu.addAction(yearlyAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
}
