//
//  HomeTabBarVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 18/01/20.
//

import UIKit

class HomeTabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if let calType = UserDefaults.standard.value(forKey: strCalenderType) as? Int {
            cuurrentCalenderType = calType
        }
        if let sortType = UserDefaults.standard.value(forKey: strCurrentSortType) as? Int {
            currentSortType = sortType
        }
        if let cat = UserDefaults.standard.value(forKey: strCurrentSelectCat) as? String {
            currentSelectCatbyFilter = cat
        }
        if let accType = UserDefaults.standard.value(forKey: strCurrentSelectedAcoount) as? String {
            currentSelectedAccount = accType
        }
        if let symbols = UserDefaults.standard.value(forKey: strCurrencySymbols)as? String {
                   currentCurrencySymbols = symbols
        }
    }
}
