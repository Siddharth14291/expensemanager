//
//  CarryOverVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 03/02/20.
//

import UIKit

class CarryOverVC: UIViewController {

    @IBOutlet var tblList: UITableView!
    var arrAccontData:[AccountData] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = bgGrayColor

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getAccount()
    }
    func getAccount() {
        DatabaseManager.shared.getAccount(withParametrs: [:]) { (success, message, arrData) in
            if success == true {
                self.arrAccontData = arrData
                self.tblList.reloadData()
            }else {
                
            }
        }
    }
    
    @IBAction func btnBackaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CarryOverVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAccontData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarryOverCell") as! CarryOverCell
        cell.selectionStyle = .none
        let obj = self.arrAccontData[indexPath.row]
        cell.lblAccountName.text = obj.name!
        if obj.is_carryoveron == 1 {
            cell.lblStatus.text = "On"
        }else {
            cell.lblStatus.text = "Off"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrAccontData[indexPath.row]
        let next = self.storyboard?.instantiateViewController(withIdentifier: "CarryOverDetailsVC") as! CarryOverDetailsVC
        next.objAccount = obj
    
        self.navigationController?.pushViewController(next, animated: true)
    }
}
