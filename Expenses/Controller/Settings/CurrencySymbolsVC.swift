//
//  CurrencySymbolsVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 01/02/20.
//

import UIKit

class CurrencySymbolsVC: UIViewController {

    @IBOutlet var tblList: UITableView!
    var arrSymbol = ["₹","$","£","¥","؋","L","دج","€","Kz","$","$","$","֏","ƒ","$","€","₼","$",".د.ب","‎৳","$","p.","€","$","CFA","$","Nu.","Bs","KM","P","R$","$","Лв.","CFA","FBu","៛","FCFA","$","$","$","FCFA","FCFA","$","¥","$","CF","FCFA","FC","₡","CFA","HRK","$","€","Kč","kr.","Fdj","$","RD$","$","ج.م","$","FCFA","Nfk","€","Br","£","$","€","FCFA","D","ლ","€","‎GH₵","£","€","$","$","Q","FG","CFA","$","G","L","HK$","Ft","ISK","₹","Rp","ع.د","﷼","€","₪","€","$","‎د.أ","₸","Ksh","د.ك","COM","₭","€",".ل.ل","R","$","ل.د","€","€","MOP$","ден","Ar","MK","RM","MVR","CFA","€","UM","Rs","$","L","€","₮","€","$","د.م.","MTn","K","$","नेरू","€","$","C$","CFA","₦","KPW","kr","﷼","Rs","₪","B/.","K","Gs.","S/.","₱","zł","€","$","﷼","lei","₽","RF","WS$","€","﷼","Db","CFA","RSD","SR","Le","$","NAf.","€","€","$","S","R","₩","£","€","‎රු","£","$","$","$","ج.س.","$","E","kr","CHF","ل.س.‏","NT$","COM","TSh","฿","CFA","T$","$","د.ت","₺","TMT","USh","₴","د.إ","$","so'm","VT","Bs.","₫","ر.ي.‏","K","US$"]
     var arrName = ["System Default","United States","United Kingdom","Japan","Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua and Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia And Herzegovina","Botswana","Brazil","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Colombia","Comoros","Congo-Brazzville","Congo-Kinshasa","Costa Rica","Côte d'Ivoire","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Fiji","Finland","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Grenada","Guam","Guatemala","Guinea","Guinea-Bissau","Guyana","Haiti","Honduras","Hong Kong(China)","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Israel","Italy","Jamaica","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Lithuania","Luxembourg","Macau(Chaina)","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar(Burma)","Namibia","Nepal","Netherlands","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palestinian Territories","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qutar","Romania","Russia","Rwanda","Samoa","San Marino","Saudi Arabia","São Tomé & Príncipe","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Sint Maarten","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St. Helena","St. Kitts & Nevis","St. Lucia","St. Vincent & Grenadines","Sudan","Suriname","Swazialand","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Uganda","Ukraine","United Arab Emirates","Uruguay","Uzbekistan","Vanuatu","Venezuela","Vietnam","Yemen","Zambia","Zimbabwe"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = bgGrayColor
        

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnBackaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CurrencySymbolsVC:UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrName.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencySymbolsCell") as! CurrencySymbolsCell
        cell.lblName.text = self.arrName[indexPath.row]
        cell.lblSymbol.text = self.arrSymbol[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.set(self.arrSymbol[indexPath.row], forKey: strCurrencySymbols)
        currentCurrencySymbols = self.arrSymbol[indexPath.row]
        self.navigationController?.popViewController(animated: true)
    }
}
