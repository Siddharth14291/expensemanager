//
//  BudgetModeDetailsVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 03/02/20.
//

import UIKit

class BudgetModeDetailsVC: UIViewController {

    @IBOutlet var modeSwitch: UISwitch!
    @IBOutlet var incomeSwitch: UISwitch!
    @IBOutlet var heightBackView: NSLayoutConstraint!
    @IBOutlet var backView: UIView!
    @IBOutlet var txtAmount: UITextField!
    var objAccount:AccountData!
    override func viewDidLoad() {
        super.viewDidLoad()

        if objAccount.is_budgeton == 1 {
            self.modeSwitch.isOn = true
            self.backView.isHidden = false
            self.heightBackView.constant = 120
            self.txtAmount.text = "\(objAccount.amount!)"
            if self.objAccount.is_includeincome == 1 {
                self.incomeSwitch.isOn = true
            }else {
                self.incomeSwitch.isOn = false
            }
        }else {
            self.modeSwitch.isOn = false
            self.backView.isHidden = true
            self.heightBackView.constant = 0
            self.incomeSwitch.isOn = false
            self.modeSwitch.isOn = false
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func modeSwitchaction(_ sender: UISwitch) {
        if sender.isOn == true {
            self.backView.isHidden = false
            self.heightBackView.constant = 120
        }else {
            self.backView.isHidden = true
            self.heightBackView.constant = 0
        }
    }
    
    @IBAction func incomeSwitchaction(_ sender: UISwitch) {
        
    }
    
    @IBAction func btnSaveaction(_ sender: UIButton) {
        if self.txtAmount.text?.isValidString() == false {
            ShowAlert(title: "Expense", message: "Please enter amount", in: self)
        }else {
            updateData()
        }
    }
    func updateData() {
        var is_budgeton = 0
        var is_includeincome = 0
        if self.modeSwitch.isOn == true {
            is_budgeton = 1
        }else {
            is_budgeton = 0
        }
        if self.incomeSwitch.isOn == true {
            is_includeincome = 1
        }else {
            is_includeincome = 0
        }
        let amount = Double(txtAmount.text!)
        let param = ["name":objAccount.name!,"chart_color":"","hide_futuretra":objAccount.hide_futuretra!,"time_period":objAccount.time_period!,"is_carryoveron":objAccount.is_carryoveron!,"is_positiveonly":objAccount.is_positiveonly!,"is_budgeton":is_budgeton,"amount":amount!,"is_includeincome":is_includeincome] as [String : Any]
        
        DatabaseManager.shared.UpdateAccount(withParametrs: param, strId: objAccount.id!) { (success, messsage) in
            if success == true {
                self.navigationController?.popViewController(animated: true)
            }else {
                
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
