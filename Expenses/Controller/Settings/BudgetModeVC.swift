//
//  BudgetModeVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 03/02/20.
//

import UIKit

class BudgetModeVC: UIViewController {

    @IBOutlet var tblList: UITableView!
    var arrAccountData:[AccountData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = bgGrayColor

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getAccountList()
    }

    func getAccountList() {
        
        DatabaseManager.shared.getAccount(withParametrs: [:]) { (success, message, arrData) in
            if success == true {
                self.arrAccountData = arrData
                self.tblList.reloadData()
            }else {
                
            }
        }
    }
    
    @IBAction func btnBackaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BudgetModeVC:UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAccountData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BudgetModeCell") as! BudgetModeCell
        cell.selectionStyle = .none
        let obj = self.arrAccountData[indexPath.row]
        if obj.is_budgeton == 1 {
            cell.lblAmount.text = "\(obj.amount!)"
        }else {
            cell.lblAmount.text = ""
        }
        cell.lblName.text = self.arrAccountData[indexPath.row].name!
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrAccountData[indexPath.row]
        let next = self.storyboard?.instantiateViewController(withIdentifier: "BudgetModeDetailsVC") as! BudgetModeDetailsVC
        next.objAccount = obj
    
        self.navigationController?.pushViewController(next, animated: true)
    }
}
