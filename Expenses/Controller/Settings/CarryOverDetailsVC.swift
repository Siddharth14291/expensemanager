//
//  CarryOverDetailsVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 03/02/20.
//

import UIKit

class CarryOverDetailsVC: UIViewController {
    
    @IBOutlet var lblPositiveOnly: UILabel!
    @IBOutlet var switchPositiveonly: UISwitch!
    @IBOutlet var switchCarryOver: UISwitch!
    var objAccount:AccountData!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = bgGrayColor
        if objAccount.is_carryoveron == 1 {
            switchCarryOver.isOn = true
            lblPositiveOnly.isHidden = false
            switchPositiveonly.isHidden = false
            
            if objAccount.is_positiveonly == 1 {
                switchPositiveonly.isOn = true
            }else {
                switchPositiveonly.isOn = false
            }
        }else {
            switchCarryOver.isOn = false
            lblPositiveOnly.isHidden = true
            switchPositiveonly.isHidden = true
            if objAccount.is_positiveonly == 1 {
                switchPositiveonly.isOn = true
            }else {
                  switchPositiveonly.isOn = false
            }
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func switchCarryOveraction(_ sender: UISwitch) {
        if sender.isOn == true {
            switchPositiveonly.isHidden = false
            self.lblPositiveOnly.isHidden = false
            var is_positiveonly = 0
            if self.switchPositiveonly.isOn == true {
                is_positiveonly = 1
            }else {
                is_positiveonly = 0
            }
            let param = ["name":objAccount.name!,"chart_color":"","hide_futuretra":objAccount.hide_futuretra!,"time_period":objAccount.time_period!,"is_carryoveron":1,"is_positiveonly":is_positiveonly,"is_budgeton":objAccount.is_budgeton!,"amount":objAccount.amount!,"is_includeincome":objAccount.is_includeincome!] as [String : Any]
            DatabaseManager.shared.UpdateAccount(withParametrs: param, strId: objAccount.id!) { (success, message) in
                if success == true {
                    
                }else {
                    
                }
            }
        }else {
            switchPositiveonly.isHidden = true
            self.lblPositiveOnly.isHidden = true
            var is_positiveonly = 0
            if self.switchPositiveonly.isOn == true {
                is_positiveonly = 1
            }else {
                is_positiveonly = 0
            }
            let param = ["name":objAccount.name!,"chart_color":"","hide_futuretra":objAccount.hide_futuretra!,"time_period":objAccount.time_period!,"is_carryoveron":0,"is_positiveonly":is_positiveonly,"is_budgeton":objAccount.is_budgeton!,"amount":objAccount.amount!,"is_includeincome":objAccount.is_includeincome!] as [String : Any]
            DatabaseManager.shared.UpdateAccount(withParametrs: param, strId: objAccount.id!) { (success, message) in
                if success == true {
                    
                }else {
                    
                }
            }
        }
    }
    
    @IBAction func switchPositiveaction(_ sender: UISwitch) {
        if sender.isOn == true {
           
            var is_positiveonly = 0
            if self.switchCarryOver.isOn == true {
                is_positiveonly = 1
            }else {
                is_positiveonly = 0
            }
            let param = ["name":objAccount.name!,"chart_color":"","hide_futuretra":objAccount.hide_futuretra!,"time_period":objAccount.time_period!,"is_carryoveron":is_positiveonly,"is_positiveonly":1,"is_budgeton":objAccount.is_budgeton!,"amount":objAccount.amount!,"is_includeincome":objAccount.is_includeincome!] as [String : Any]
            DatabaseManager.shared.UpdateAccount(withParametrs: param, strId: objAccount.id!) { (success, message) in
                if success == true {
                    
                }else {
                    
                }
            }
        }else {
            var is_positiveonly = 0
            if self.switchCarryOver.isOn == true {
                is_positiveonly = 1
            }else {
                is_positiveonly = 0
            }
            let param = ["name":objAccount.name!,"chart_color":"","hide_futuretra":objAccount.hide_futuretra!,"time_period":objAccount.time_period!,"is_carryoveron":is_positiveonly,"is_positiveonly":0,"is_budgeton":objAccount.is_budgeton!,"amount":objAccount.amount!,"is_includeincome":objAccount.is_includeincome!] as [String : Any]
            DatabaseManager.shared.UpdateAccount(withParametrs: param, strId: objAccount.id!) { (success, message) in
                if success == true {
                    
                }else {
                    
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
