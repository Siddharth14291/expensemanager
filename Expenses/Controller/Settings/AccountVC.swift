//
//  AccountVC.swift
//  Expenses
//
//  
//

import UIKit

class AccountVC: UIViewController {

    @IBOutlet weak var tblList: UITableView!
    var arrAccountData:[AccountData] = []
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         getAccountList()
    }
    func getAccountList(){
        
        DatabaseManager.shared.getAccount(withParametrs: [:]) { (success, message, arrdata) in
            if success == true {
                self.arrAccountData = arrdata
                self.tblList.reloadData()
            }
        }
    }
    
    @IBAction func btnBackaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AccountVC:UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAccountData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountListCell") as! AccountListCell
        cell.selectionStyle = .none
        cell.lblAccountName.text = self.arrAccountData[indexPath.row].name!
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrAccountData[indexPath.row]
        let next = self.storyboard?.instantiateViewController(withIdentifier: "AddAccount") as! AddAccount
        next.objAccount = obj
        next.isUPdate = true
        self.navigationController?.pushViewController(next, animated: true)
    }
}
