//
//  AddAccount.swift
//  Expenses
//
//  
//

import UIKit

class AddAccount: UIViewController {

    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var btnDelete: UIButton!
    var isUPdate = false
    var objAccount:AccountData!
    override func viewDidLoad() {
        super.viewDidLoad()

       // txtAccount.backgroundColor = expenseRed
        self.view.backgroundColor = bgGrayColor
        if isUPdate == true {
            self.btnDelete.isHidden = false
            self.btnDelete.backgroundColor = expenseRed

            self.txtAccount.text = self.objAccount.name!
        }else {
            self.btnDelete.isHidden = true
            
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnDeleteaction(_ sender: UIButton) {
        if objAccount.id == "1" {
            ShowAlert(title: "Expense", message: "This is the 'Default Account. It canot be deleteed.", in: self)
        }else {
            DatabaseManager.shared.checkAccountData(withParametrs: [:], strCatname: objAccount.id!, completion: {(success, message, count)
                in
                if success == true {
                    if count == 0 {
                        DatabaseManager.shared.deleteAccount(withParametrs: self.objAccount.id!) { (success, message) in
                            if success == true {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }else {
                        ShowAlert(title: "Expense", message: " Account is currently associated with one or more transactions", in: self)
                    }
                }
            })
        }
        
    }
    
    @IBAction func btnAccountName(_ sender: UIButton) {
       
      if self.txtAccount.text?.isValidString() == false {
             ShowAlert(title: "Expense", message: "Please enter name", in: self)
        }else {
        if isUPdate == true   {
            updateAccount()
        }else {
             addAccount()
        }
           
        }
    }
    
    @IBAction func btnBackaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func addAccount() {
        let param = ["name":txtAccount.text!,"chart_color":"","hide_futuretra":0,"time_period":3,"is_carryoveron":0,"is_positiveonly":0,"is_budgeton":0,"amount":0,"is_includeincome":0] as [String : Any]
        
        DatabaseManager.shared.addAccount(withParametrs: param) { (success, message) in
            if success == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        
                  
    }
    func updateAccount() {
        let param = ["name":txtAccount.text!,"chart_color":"","hide_futuretra":objAccount.hide_futuretra!,"time_period":objAccount.time_period!,"is_carryoveron":objAccount.is_carryoveron!,"is_positiveonly":objAccount.is_positiveonly!,"is_budgeton":objAccount.is_budgeton!,"amount":objAccount.amount!,"is_includeincome":objAccount.is_includeincome!] as [String : Any]
        DatabaseManager.shared.UpdateAccount(withParametrs: param, strId: objAccount.id!) { (success, message) in
            if success == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
