//
//  HideFutureVC.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 01/02/20.
//

import UIKit

class HideFutureVC: UIViewController {

    @IBOutlet var tblList: UITableView!
    var arrAccountData:[AccountData] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        // Do any additional setup after loading the view.
    }
    
    func getData(){
        
        DatabaseManager.shared.getAccount(withParametrs: [:]) { (success, message, arrData) in
            if success == true {
                self.arrAccountData = arrData
                self.tblList.reloadData()
            }
        }
        
    }
    @IBAction func btnBackaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func swiftchOnOffaction(_ sender: UISwitch) {
        if sender.tag == 09 {
            if sender.isOn == true {
                 UserDefaults.standard.set(1, forKey: strAllAccountHide)
            }else {
                UserDefaults.standard.set(0, forKey: strAllAccountHide)
            }
           
        }else {
            if sender.isOn == true {
                 updateData(ison: 1, objAccount: self.arrAccountData[sender.tag])
            }else {
                updateData(ison: 0, objAccount: self.arrAccountData[sender.tag])
            }
        }
        
    }
    func updateData(ison:Int,objAccount:AccountData) {
        let param = ["name":objAccount.name!,"chart_color":"","hide_futuretra":ison,"time_period":objAccount.time_period!,"is_carryoveron":objAccount.is_carryoveron!,"is_positiveonly":objAccount.is_positiveonly!,"is_budgeton":objAccount.is_budgeton!,"amount":objAccount.amount!,"is_includeincome":objAccount.is_includeincome!] as [String : Any]
        DatabaseManager.shared.UpdateAccount(withParametrs: param, strId: objAccount.id!) { (success, message) in
            if success == true {
                self.getData()
            }else {
                
            }
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HideFutureVC:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.arrAccountData.count
        }else {
            return 1
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HideFutureCell") as! HideFutureCell
                       let obj = self.arrAccountData[indexPath.row]
                       cell.lblName.text = obj.name!
                       cell.swiftchOnOff.tag = indexPath.row
                       if obj.hide_futuretra == 1 {
                           cell.swiftchOnOff.isOn = true
                       }else {
                           cell.swiftchOnOff.isOn = false
                       }
                       cell.selectionStyle = .none
                       return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HideFutureCell") as! HideFutureCell
                if let isOn = UserDefaults.standard.value(forKey: strAllAccountHide) as? Int {
                    if isOn == 1 {
                        cell.swiftchOnOff.isOn = true
                    }else {
                        cell.swiftchOnOff.isOn = false
                    }
                }
               
                   
                cell.lblName.text = "All Account"
                cell.swiftchOnOff.tag = 09
                cell.selectionStyle = .none
                return cell
            }

    }
}
