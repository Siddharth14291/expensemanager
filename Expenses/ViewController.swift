//
//  ViewController.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 13/01/20.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let next = self.storyboard?.instantiateViewController(withIdentifier: "HomeTabBarVC") as! HomeTabBarVC
        self.navigationController?.pushViewController(next, animated: true)

    }
}

