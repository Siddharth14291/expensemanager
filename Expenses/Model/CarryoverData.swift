//
//  CarryoverData.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 16/01/20.
//

import UIKit

class CarryoverData: NSObject {
   
    
    var id:String!
    var acc_id:String!
    var is_on:Int!
    var is_positiveonly:Int!
    
    init(dict:[String:Any]) {
        id = "\(dict[strCarrId] ?? "")"
        acc_id = "\(dict[strCarrAccId] ?? "")"
        is_on = dict[strCarrIsOn] as? Int
        is_positiveonly = dict[strCarrIsPositiveonly] as? Int
    }
}
