//
//  TransactionData.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 16/01/20.
//

import UIKit

class TransactionData: NSObject {
    
    
    var id:String!
    var cat_id:String!
    var acc_id:String!
    var transaction_date:String!
    var amount:Double!
    var is_repeating:Int!
    var every:Int!
    var repeating:Int!
    var end_date:String!
    var notes:String!
    var type:Int!
    var par_tra_id:String!
    var cat_name:String!
    var icn_name:String!
    
    init(strId:String,strCatID:String,strAccId:String,doTransactioDate:String,doAmount:Double,isRepeating:Int,intEvery:Int,intRepeating:Int,doEndDate:String,strNotes:String,intType:Int,intParentid:String,strCatName:String!,strCatIcon:String) {
        
            id = strId
            cat_id = strCatID
            acc_id = strAccId
            transaction_date = doTransactioDate
            amount = doAmount
            is_repeating = isRepeating
            every = intEvery
            repeating = intRepeating
            end_date = doEndDate
            notes = strNotes
            type = intType
            par_tra_id = intParentid
            cat_name = strCatName
            icn_name = strCatIcon
    }
    
}

//    init(dict:[String:Any]) {
//
//        id = "\(dict[strTraId] ?? "")"
//        cat_id = "\(dict[strTraCatId] ?? "")"
//        acc_id = "\(dict[strTraAccId] ?? "")"
//        transaction_date = dict[strTraDate] as? Double
//        amount = dict[strTraAmount] as? Double
//        is_repeating = dict[strTraIsRepeating] as? Int
//        every = dict[strTraEvery] as? Int
//        repeating = dict[strTraRepeating] as? Int
//        end_date = dict[strTraEndDate] as? Double
//        notes = "\(dict[strTraNotes] ?? "")"
//        type = dict[strTraType] as? Int
//
//    }
