//
//  AccountData.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 16/01/20.
//

import UIKit

class AccountData: NSObject {

    
    var id:String!
    var name:String!
    var chart_color:String!
    var hide_futuretra:Int!
    var time_period:Int!
    var is_carryoveron:Int!
    var is_positiveonly:Int!
    var is_budgeton:Int!
    var amount:Double!
    var is_includeincome:Int!
    
    init(strId:String,strName:String,strChartColor:String,intHideFuturetra:Int,intTimePeriod:Int,intIsCarryoveron:Int,intIsPositiveonly:Int,intIsBudgeton:Int,doAmount:Double,intIsIncludeincome:Int) {
        
            id = strId
            name = strName
            chart_color = strChartColor
            hide_futuretra = intHideFuturetra
            time_period = intTimePeriod
            is_carryoveron = intIsCarryoveron
            is_positiveonly = intIsPositiveonly
            is_budgeton = intIsBudgeton
            amount = doAmount
            is_includeincome = intIsIncludeincome
        
    }
    
//    init(dict:[String:Any]) {
//        id = "\(dict[strAccId] ?? "")"
//        name = "\(dict[strAccName] ?? "")"
//        chart_color = "\(dict[strAccChartColor] ?? "")"
//        hide_futuretra = dict[strAccHideFuturetra] as? Int
//        time_period = dict[strAccTimePeriod] as? Int
//    }
}
