//
//  OverviewIncomeExpenseDate.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 24/01/20.
//

import UIKit

class OverviewIncomeExpenseDate: NSObject {

    var strName:String!
    var amount:Double!
    var percentage:String!
    //var accountId:String!
    
    
    init(name:String!,intAmount:Double,strPre:String) {
            strName = name
            amount = intAmount
            percentage = strPre
    }
}
