//
//  CategoryData.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 13/01/20.
//

import UIKit

class CategoryData: NSObject {
    
    var id:String!
    var name:String!
    var icn_name:String!
    var type:Int!
    var chart_color:String!
    
    init(strId:String!,strName:String,strIconName:String,intType:Int,strChartColor:String) {
            id = strId
            name = strName
            icn_name = strIconName
            type = intType
            chart_color = strChartColor
    }
//    init(dict:[String:Any]) {
//        id = "\(dict[strCatId] ?? "")"
//        name = "\(dict[strCatName] ?? "")"
//        icn_name = "\(dict[strCatIcnname] ?? "")"
//        type = dict[strCatType] as? Int
//        chart_color = "\(dict[strCatchartcolor] ?? "")"
//
//    }
}
