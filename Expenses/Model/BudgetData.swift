//
//  BudgetData.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 16/01/20.
//

import UIKit

class BudgetData: NSObject {
    
    
    var id:String!
    var acc_id:String!
    var amount:Double!
    var is_includeincome:Int!
    
    init(dict:[String:Any]) {
        id = "\(dict[strBudId] ?? "")"
        acc_id = "\(dict[strBudAccID] ?? "")"
        amount = dict[strBudAmount] as? Double
        is_includeincome = dict[strBudIsIncludeincome] as? Int
    }
}
