//
//  Constant.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 18/01/20.
//

import Foundation
import UIKit

let bgGrayColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1.0)
let textWhiteColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
let titleGrayColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0)
let incomeGreen = UIColor(red: 60/255, green: 178/255, blue: 13/255, alpha: 0.85)
let expenseRed = UIColor(red: 245/255, green: 1/255, blue: 1/255, alpha: 0.85)
let catGrayColor = UIColor(red: 50/255, green: 50/255, blue: 50/255, alpha: 0.85)
let progressColor = UIColor(red: 255/255, green: 138/255, blue: 52/255, alpha: 0.85)
let blackColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
let catTextColor = UIColor(red: 97/255, green: 97/255, blue: 97/255, alpha: 1.0)
let searchViewBGColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1.0)
let dateColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
var currentCalender = 0
var cuurrentCalenderType = 0
var currentSelectCatbyFilter = ""
var currentSelectedAccount = ""
var currentCurrencySymbols = ""
var tranactionSearchtext = ""
// MARK :- Sort Type 1 = (newestdate first), Type 2 = oldest first , Type 3 = highest amount first , type 4 = oldest amount first , type 5 = a-z , type 6 = z-a
var currentSortType = 1
let calendarApp: Calendar = Calendar.current
func dipayDate(strDate:String) -> String {
    
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = "yyyy-MM-dd"
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "dd-MMM-yyyy"

    let date: NSDate? = dateFormatterGet.date(from: strDate) as NSDate?
    //print()
    return dateFormatterPrint.string(from: date as! Date)
}

func months(intDate:Double) -> String {
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "dd"
    let timeInterval = intDate
    let date = NSDate(timeIntervalSince1970: timeInterval)
    //print()
    return dateFormatterPrint.string(from: date as Date)
}
func dataBaseDate(intDate:Date) -> String {
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "yyyy-MM-dd"
   // let dateFormatterPrint
    let timeInterval = intDate
    //let date = NSDate(timeIntervalSince1970: timeInterval)
    //let d = Date(timeIntervalSince1970: TimeInterval(timeInterval))
    //print()
    return dateFormatterPrint.string(from: intDate)
}
func dataBaseDateNew(intDate:Date) -> String {
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "dd-MM-yyyy"
//    let dateFormatterGet = DateFormatter()
//    dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
//    let date = NSDate(timeIntervalSince1970: intDate)
//    let d = Date(timeIntervalSinceReferenceDate: TimeInterval(intDate))
    print(dateFormatterPrint.string(from: intDate))
    return dateFormatterPrint.string(from: intDate)
}
func currentCalenderDay() -> (String,String) {
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM"
           let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "yyyy-MM-dd"
            
            let today = Date()
            let nextDate = Calendar.current.date(byAdding: .day, value: currentCalender, to: today)
            let strDate =  dateFormatterPrint.string(from: nextDate!)
            let strAPIDate = dateFormatterAPI.string(from: nextDate!)
    return (strDate,strAPIDate)
}
func currentCalenderMonth() -> (String,String,String) {
    let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMMM"
           let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "dd-MM-yyyy"
            
            let today = Date()
            let nextDate = Calendar.current.date(byAdding: .month, value: currentCalender, to: today)
    
            let strDate =  dateFormatterPrint.string(from: nextDate!)
           // let strAPIDate = dateFormatterAPI.string(from: nextDate!)
            let startDate = calendarApp.startOfMonth(nextDate!)
            let endDate = calendarApp.endOfMonth(nextDate!)
            let strStartData = dataBaseDate(intDate: startDate)
            let strEndDate = dataBaseDate(intDate: endDate)

    return (strDate,strStartData,strEndDate)
}

func currentCalenderWeek()  -> (String,String,String) {
    var strWeek = ""
     let dateFormatterYear = DateFormatter()
    dateFormatterYear.dateFormat = "yyyy"
     let dateFormatterPrint = DateFormatter()
     dateFormatterPrint.dateFormat = "MMM"
    let dateFormatterAPI = DateFormatter()
       dateFormatterAPI.dateFormat = "dd-MM-yyyy"
    // currentCalender =  currentCalender + 1
     let today = Date()
     let nextDate = Calendar.current.date(byAdding: .weekOfMonth, value: currentCalender, to: today)

     let strDate =  dateFormatterPrint.string(from: nextDate!)
     let startDate = calendarApp.startOfWeek(nextDate!)
     let endDate = calendarApp.endOfWeek(nextDate!)
     let strStartData = dataBaseDate(intDate: startDate)
     let strEndDate = dataBaseDate(intDate: endDate)
    
     let strDay = months(intDate: startDate.timeIntervalSince1970)
     let strEnDay = months(intDate: endDate.timeIntervalSince1970)
     let currentYear = dateFormatterYear.string(from: today)
     let newYear = dateFormatterYear.string(from: nextDate!)
     if  currentYear != newYear {
            // let day = dipayDate(strDate: strDate)
          //strWeek = strDay + " - " + strEnDay + " " + strDate + " " + newYear
         let strStartMonth = dateFormatterPrint.string(from: startDate)
         let strEndMonth = dateFormatterPrint.string(from: endDate)
         if strStartMonth == strEndMonth {
              strWeek = strDay + " - " + strEnDay + " " + strDate + " " + newYear
         }else {
              strWeek = strDay + "-" + strStartMonth + " - " + strEnDay + " " + strDate + " " + newYear
         }
         
     }else {
             //strDatenow = strDate
          //strWeek = strDay + " - " + strEnDay + " " + strDate
         let strStartMonth = dateFormatterPrint.string(from: startDate)
         let strEndMonth = dateFormatterPrint.string(from: endDate)
         if strStartMonth == strEndMonth {
              strWeek = strDay + " - " + strEnDay + " " + strDate
         }else {
             // strWeek = strDay + "-" + strStartMonth + " - " + strEnDay + " " + strDate + " " + newYear
             strWeek = strDay + "-" + strStartMonth + " - " + strEnDay + " " + strEndMonth
         }
     }
    
    return (strWeek,strStartData,strEndDate)
}
func currentCalenderYear() -> (String,String,String) {
    let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "yyyy"
           let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "dd-MM-yyyy"
            
            let today = Date()
            let nextDate = Calendar.current.date(byAdding: .year, value: currentCalender, to: today)
    
            let strDate =  dateFormatterPrint.string(from: nextDate!)
            let startDate = calendarApp.startOfYear(nextDate!)
            let endDate = calendarApp.endOfYear(nextDate!)
            let strStartData = dataBaseDate(intDate: startDate)
            let strEndDate = dataBaseDate(intDate: endDate)

    return (strDate,strStartData,strEndDate)
}

func nextCalenderDay() -> (String,String,Bool) {
        var strDatenow = ""
        var isMaxLimit = false
        let dateFormatterYear = DateFormatter()
        dateFormatterYear.dateFormat = "yyyy"
        let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM"
        let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "yyyy-MM-dd"
            
        currentCalender = currentCalender + 1
        let today = Date()
        let nextDate = Calendar.current.date(byAdding: .day, value: currentCalender, to: today)
        let nextYear = Calendar.current.date(byAdding: .year, value: 5, to: today)
        let maxYear = dateFormatterYear.string(from: nextYear!)
        let strDate =  dateFormatterPrint.string(from: nextDate!)
        let strAPIDate = dateFormatterAPI.string(from: nextDate!)
        let currentYear = dateFormatterYear.string(from: today)
        let newYear = dateFormatterYear.string(from: nextDate!)
        if maxYear == newYear {
            isMaxLimit = true
        }else {
            isMaxLimit = false
        }
        if  currentYear != newYear {
            strDatenow = dipayDate(strDate: strDate)
        }else {
            strDatenow = strDate
        }
            
    
    return (strDatenow,strAPIDate,isMaxLimit)
}
func previousCalenderDay() -> (String,String) {
    var strDatenow = ""
    
        let dateFormatterYear = DateFormatter()
        dateFormatterYear.dateFormat = "yyyy"
        let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM"
        let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "yyyy-MM-dd"
            
        currentCalender = currentCalender - 1
        let today = Date()
        let nextDate = Calendar.current.date(byAdding: .day, value: currentCalender, to: today)
        let strDate =  dateFormatterPrint.string(from: nextDate!)
        let strAPIDate = dateFormatterAPI.string(from: nextDate!)
        let currentYear = dateFormatterYear.string(from: today)
        let newYear = dateFormatterYear.string(from: nextDate!)
        if  currentYear != newYear {
            strDatenow = dipayDate(strDate: strDate)
        }else {
            strDatenow = strDate
        }
            
    
    return (strDatenow,strAPIDate)

}

func nextCalenderMonth() -> (String,String,String,Bool) {
            let dateFormatterPrint = DateFormatter()
            let dateFormatterYear = DateFormatter()
            var monts = ""
            var isMaxLimit = false
            dateFormatterYear.dateFormat = "yyyy"
            dateFormatterPrint.dateFormat = "MMM"
           let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "dd-MM-yyyy"
            
            let today = Date()
            currentCalender =  currentCalender + 1
            let nextYear = Calendar.current.date(byAdding: .year, value: 5, to: today)
            let maxYear = dateFormatterYear.string(from: nextYear!)
            let nextDate = Calendar.current.date(byAdding: .month, value: currentCalender, to: today)
    
            let strDate =  dateFormatterPrint.string(from: nextDate!)
           // let strAPIDate = dateFormatterAPI.string(from: nextDate!)
            let startDate = calendarApp.startOfMonth(nextDate!)
            let endDate = calendarApp.endOfMonth(nextDate!)
            let strStartData = dataBaseDate(intDate: startDate)
            let strEndDate = dataBaseDate(intDate: endDate)
            let currentYear = dateFormatterYear.string(from: today)
            let newYear = dateFormatterYear.string(from: nextDate!)
            if maxYear == newYear {
                isMaxLimit = true
            }else {
                isMaxLimit = false
            }
            if currentYear != newYear {
                monts = strDate + " - " + newYear
            }else {
                 dateFormatterPrint.dateFormat = "MMMM"
                let strDate = dateFormatterPrint.string(from: nextDate!)
                monts = strDate
            }
    return (monts,strStartData,strEndDate,isMaxLimit)
}

func previousCalenderMonth() -> (String,String,String) {
            let dateFormatterPrint = DateFormatter()
            let dateFormatterYear = DateFormatter()
            var monts = ""
            dateFormatterYear.dateFormat = "yyyy"
            dateFormatterPrint.dateFormat = "MMM"
           let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "dd-MM-yyyy"
            
            let today = Date()
            currentCalender =  currentCalender - 1
            let nextDate = Calendar.current.date(byAdding: .month, value: currentCalender, to: today)
    
            let strDate =  dateFormatterPrint.string(from: nextDate!)
           // let strAPIDate = dateFormatterAPI.string(from: nextDate!)
            let startDate = calendarApp.startOfMonth(nextDate!)
            let endDate = calendarApp.endOfMonth(nextDate!)
            let strStartData = dataBaseDate(intDate: startDate)
            let strEndDate = dataBaseDate(intDate: endDate)
            let currentYear = dateFormatterYear.string(from: today)
            let newYear = dateFormatterYear.string(from: nextDate!)
            if currentYear != newYear {
               
                monts = strDate + " - " + newYear
            }else {
                 dateFormatterPrint.dateFormat = "MMMM"
                let strDate = dateFormatterPrint.string(from: nextDate!)
                monts = strDate
            }
    return (monts,strStartData,strEndDate)
}
func nextCalenderWeek() -> (String,String,String,Bool){
            var strWeek = ""
            var isMaxLimit = false
        
            let dateFormatterYear = DateFormatter()
           dateFormatterYear.dateFormat = "yyyy"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM"
           let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "dd-MM-yyyy"
            currentCalender =  currentCalender + 1
            let today = Date()
            let nextYear = Calendar.current.date(byAdding: .year, value: 5, to: today)
            let maxYear = dateFormatterYear.string(from: nextYear!)
            let nextDate = Calendar.current.date(byAdding: .weekOfMonth, value: currentCalender, to: today)
    

            let strDate =  dateFormatterPrint.string(from: nextDate!)
            let startDate = calendarApp.startOfWeek(nextDate!)
            let endDate = calendarApp.endOfWeek(nextDate!)
            let strStartData = dataBaseDate(intDate: startDate)
            let strEndDate = dataBaseDate(intDate: endDate)
           
            let strDay = months(intDate: startDate.timeIntervalSince1970)
            let strEnDay = months(intDate: endDate.timeIntervalSince1970)
            let currentYear = dateFormatterYear.string(from: today)
            let newYear = dateFormatterYear.string(from: nextDate!)
            if maxYear == newYear {
                isMaxLimit = true
            }else {
                isMaxLimit = false
            }
            if  currentYear != newYear {
                   // let day = dipayDate(strDate: strDate)
                 //strWeek = strDay + " - " + strEnDay + " " + strDate + " " + newYear
                let strStartMonth = dateFormatterPrint.string(from: startDate)
                let strEndMonth = dateFormatterPrint.string(from: endDate)
                if strStartMonth == strEndMonth {
                     strWeek = strDay + " - " + strEnDay + " " + strDate + " " + newYear
                }else {
                     strWeek = strDay + "-" + strStartMonth + " - " + strEnDay + " " + strDate + " " + newYear
                }
                
            }else {
                    //strDatenow = strDate
                 //strWeek = strDay + " - " + strEnDay + " " + strDate
                let strStartMonth = dateFormatterPrint.string(from: startDate)
                let strEndMonth = dateFormatterPrint.string(from: endDate)
                if strStartMonth == strEndMonth {
                     strWeek = strDay + " - " + strEnDay + " " + strDate
                }else {
                    // strWeek = strDay + "-" + strStartMonth + " - " + strEnDay + " " + strDate + " " + newYear
                    strWeek = strDay + "-" + strStartMonth + " - " + strEnDay + " " + strEndMonth
                }
            }
           
    
    return (strWeek,strStartData,strEndDate,isMaxLimit)
}
func previousCalenderWeek() -> (String,String,String){
            var strWeek = ""
            let dateFormatterYear = DateFormatter()
           dateFormatterYear.dateFormat = "yyyy"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM"
           let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "dd-MM-yyyy"
            currentCalender =  currentCalender - 1
            let today = Date()
            let nextDate = Calendar.current.date(byAdding: .weekOfMonth, value: currentCalender, to: today)

            let strDate =  dateFormatterPrint.string(from: nextDate!)
            let startDate = calendarApp.startOfWeek(nextDate!)
            let endDate = calendarApp.endOfWeek(nextDate!)
            let strStartData = dataBaseDate(intDate: startDate)
            let strEndDate = dataBaseDate(intDate: endDate)
           
            let strDay = months(intDate: startDate.timeIntervalSince1970)
            let strEnDay = months(intDate: endDate.timeIntervalSince1970)
            let currentYear = dateFormatterYear.string(from: today)
            let newYear = dateFormatterYear.string(from: nextDate!)
            if  currentYear != newYear {
                   // let day = dipayDate(strDate: strDate)
                let strStartMonth = dateFormatterPrint.string(from: startDate)
                let strEndMonth = dateFormatterPrint.string(from: endDate)
                if strStartMonth == strEndMonth {
                     strWeek = strDay + " - " + strEnDay + " " + strDate + " " + newYear
                }else {
                     strWeek = strDay + "-" + strStartMonth + " - " + strEnDay + " " + strDate + " " + newYear
                }
                
            }else {
                    let strStartMonth = dateFormatterPrint.string(from: startDate)
                    let strEndMonth = dateFormatterPrint.string(from: endDate)
                    if strStartMonth == strEndMonth {
                         strWeek = strDay + " - " + strEnDay + " " + strDate
                    }else {
                        // strWeek = strDay + "-" + strStartMonth + " - " + strEnDay + " " + strDate + " " + newYear
                        strWeek = strDay + "-" + strStartMonth + " - " + strEnDay + " " + strEndMonth
                    }
                 
            }
           
    
    return (strWeek,strStartData,strEndDate)
}
func nextCalenderYear() -> (String,String,String,Bool) {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy"
       let dateFormatterAPI = DateFormatter()
          dateFormatterAPI.dateFormat = "dd-MM-yyyy"
        
        let today = Date()
        var isMaxLimit = false
        currentCalender = currentCalender + 1
    if currentCalender == 5 {
        isMaxLimit = true
    }else {
        isMaxLimit = false
    }
        let nextDate = Calendar.current.date(byAdding: .year, value: currentCalender, to: today)

        let strDate =  dateFormatterPrint.string(from: nextDate!)
        let startDate = calendarApp.startOfYear(nextDate!)
        let endDate = calendarApp.endOfYear(nextDate!)
        let strStartData = dataBaseDate(intDate: startDate)
        let strEndDate = dataBaseDate(intDate: endDate)

    return (strDate,strStartData,strEndDate,isMaxLimit)
}
func previousCalenderYear() -> (String,String,String) {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy"
       let dateFormatterAPI = DateFormatter()
          dateFormatterAPI.dateFormat = "dd-MM-yyyy"
        
        let today = Date()
        currentCalender = currentCalender - 1
        let nextDate = Calendar.current.date(byAdding: .year, value: currentCalender, to: today)

        let strDate =  dateFormatterPrint.string(from: nextDate!)
        let startDate = calendarApp.startOfYear(nextDate!)
        let endDate = calendarApp.endOfYear(nextDate!)
        let strStartData = dataBaseDate(intDate: startDate)
        let strEndDate = dataBaseDate(intDate: endDate)

    return (strDate,strStartData,strEndDate)
}
extension Calendar {

func dayOfWeek(_ date: Date) -> Int {
    var dayOfWeek = self.component(.weekday, from: date) + 1 - self.firstWeekday

    if dayOfWeek <= 0 {
        dayOfWeek += 7
    }

    return dayOfWeek
}

func startOfWeek(_ date: Date) -> Date {
    return self.date(byAdding: DateComponents(day: -self.dayOfWeek(date) + 1), to: date)!
}

func endOfWeek(_ date: Date) -> Date {
    return self.date(byAdding: DateComponents(day: 6), to: self.startOfWeek(date))!
}

func startOfMonth(_ date: Date) -> Date {
    return self.date(from: self.dateComponents([.year, .month], from: date))!
}

func endOfMonth(_ date: Date) -> Date {
    return self.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth(date))!
}

func startOfQuarter(_ date: Date) -> Date {
    let quarter = (self.component(.month, from: date) - 1) / 3 + 1
    return self.date(from: DateComponents(year: self.component(.year, from: date), month: (quarter - 1) * 3 + 1))!
}

func endOfQuarter(_ date: Date) -> Date {
    return self.date(byAdding: DateComponents(month: 3, day: -1), to: self.startOfQuarter(date))!
}

func startOfYear(_ date: Date) -> Date {
    return self.date(from: self.dateComponents([.year], from: date))!
}

func endOfYear(_ date: Date) -> Date {
    return self.date(from: DateComponents(year: self.component(.year, from: date), month: 12, day: 31))!
}
}
