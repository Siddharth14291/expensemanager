//
//  Utility.swift
//


import Foundation
import UIKit

func ShowAlert(title: String, message: String, in vc: UIViewController) {
    
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    
    
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
    
    vc.present(alert, animated: true, completion: nil)
}

func isValidEmail(testStr:String) -> Bool {
    // print("validate calendar: \(testStr)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}


public extension Date {
  
  func stringInUTCOfFormat(_ dateFormat: String, date: Date) -> NSString {
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(identifier: "UTC")
    dateFormatter.dateFormat = dateFormat
    return dateFormatter.string(from: date) as NSString
  }
  
  func convertDateToString (date: Date, originalFormat: String, convertFormat: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = convertFormat
    let dateObj = dateFormatter.string(from: date)
    return dateObj
  }
  
  func convertDateToDisplayFormat(strDate:String,originalFormat: String, convertFormat: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = originalFormat
    let dateObj = dateFormatter.date(from: strDate)
    dateFormatter.dateFormat = convertFormat
    if dateObj != nil {
      
      let displayDate = dateFormatter.string(from: dateObj!)
      
      return displayDate
    }
    
    return ""
  }
  
  func convertStringToDate (date: String,convertFormat: String) -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = convertFormat
    let dateObj = dateFormatter.date(from: date)
    return dateObj
  }
  
  func dateFromUTCOfFormat(_ dateFormat: String, dateString: String ,isUTC:Bool) -> Date {
    let dateFormatter = DateFormatter()
    if isUTC {
      dateFormatter.timeZone = TimeZone(identifier: "UTC")
    }
    dateFormatter.dateFormat = dateFormat
    return dateFormatter.date(from: dateString)!
  }
  
  func yearsFrom(_ date:Date) -> Int{
    return (Calendar.current as NSCalendar).components(.year, from: date, to: self, options: []).year!
  }
  func monthsFrom(_ date:Date) -> Int{
    
    return  Calendar.current.component(.month, from: date)
  }
  func weeksFrom(_ date:Date) -> Int{
    return (Calendar.current as NSCalendar).components(.weekOfYear, from: date, to: self, options: []).weekOfYear!
  }
  func daysFrom(_ date:Date) -> Int{
    return Calendar.current.component(.day, from: date)
  }
  func hoursFrom(_ date:Date) -> Int{
    return (Calendar.current as NSCalendar).components(.hour, from: date, to: self, options: []).hour!
  }
  func minutesFrom(_ date:Date) -> Int{
    return (Calendar.current as NSCalendar).components(.minute, from: date, to: self, options: []).minute!
  }
  func secondsFrom(_ date:Date) -> Int{
    return (Calendar.current as NSCalendar).components(.second, from: date, to: self, options: []).second!
  }
  
  func getMonth(_ date:Date) -> String{
    
    let arrMonth = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"]
    let month = monthsFrom(date) - 1
    var strMonth = ""
    
    if month < arrMonth.count {
      
      strMonth = arrMonth[month]
    }
    
    return strMonth
  }
  
  func offsetFrom(_ date:Date) -> String {
    if yearsFrom(date)   > 0 { return "\(yearsFrom(date)) year"   }
    if monthsFrom(date)  > 0 { return "\(monthsFrom(date)) month"  }
    if weeksFrom(date)   > 0 { return "\(weeksFrom(date)) week"   }
    if daysFrom(date)    > 0 { return "\(daysFrom(date)) day"    }
    if hoursFrom(date)   > 0 { return "\(hoursFrom(date)) hour"   }
    if minutesFrom(date) > 0 { return "\(minutesFrom(date)) min" }
    if secondsFrom(date) > 0 { return "\(secondsFrom(date)) sec" }
    return ""
  }
  
  func timesAgo(_ date:Date) -> String {
    if yearsFrom(date)   > 0 { return "\(yearsFrom(date)) year ago"   }
    if daysFrom(date)    > 2 { return "\(convertTimesAgoDate(date,formate: 1))"}
    if daysFrom(date)    == 1 { return "yesterday \(convertTimesAgoDate(date,formate: 0))"}
    if hoursFrom(date)   > 0 { return "\(hoursFrom(date)) hour ago"   }
    if minutesFrom(date) > 0 { return "\(minutesFrom(date)) min ago" }
    if secondsFrom(date) > 0 { return "\(secondsFrom(date)) sec ago" }
    return ""
  }
  
  func convertTimesAgoDate(_ date : Date,formate: Int) -> String
  {
    let dateFormat = DateFormatter()
    if formate == 0
    {
      dateFormat.dateFormat = "h:mm a"
      return "at \(dateFormat.string(from: date))"
    }
    else if formate == 1
    {
      dateFormat.dateFormat = "yyyy.MM.dd"
      let dateStr = dateFormat.string(from: date)
      dateFormat.dateFormat = "h:mm a"
      let dateStrTime = dateFormat.string(from: date)
      return "\(dateStr) at \(dateStrTime)"
    }
    
    //        dateFormatter.timeZone = NSTimeZone.systemTimeZone()
    return "at \(dateFormat.string(from: date))"
  }
  
  func convertTimeStampToTime(_ timeToBeUsed:Int) -> String
  {
    let time = Date(timeIntervalSince1970: Double(timeToBeUsed))
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "h:mm:ss a"
    dateFormatter.timeZone = TimeZone.current
    
    let timeCurrent = dateFormatter.string(from: time)
    
    return timeCurrent
  }
  
  func getTimeStamp() -> TimeInterval {
    
    return NSDate().timeIntervalSince1970 * 1000
    
  }
}

@IBDesignable
class DesignableView: UIView {
}

@IBDesignable
class DesignableButton: UIButton {
}

@IBDesignable
class DesignableLabel: UILabel {
}

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}
public extension String {
    func isValidString() -> Bool
    {
      
      let strCheck = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
      
      let isValid = (strCheck.count > 0 ? true : false)
        
      return isValid;
    }
    func isValidEmailAddress() -> Bool
    {
      //let stricterFilter = false
      //let stricterFilterString = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$"
      let laxString = "^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$"
      let emailRegex = laxString//stricterFilter ? stricterFilterString : laxString
      let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
      return emailTest.evaluate(with: self)
    }
}

