//
//  ContantRepeatingDate.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 30/01/20.
//

import Foundation
import UIKit

func getendDate(dayof:Int) -> (String,String,Date) {
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM"
           let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "yyyy-MM-dd"
            
            let today = Date()
            let nextDate = Calendar.current.date(byAdding: .day, value: dayof, to: today)
            let strDate =  dateFormatterPrint.string(from: nextDate!)
            let strAPIDate = dateFormatterAPI.string(from: nextDate!)
    return (strDate,strAPIDate,nextDate!)
}

func getendNextDate(day:Int,startDate:Date) -> (String,String,Date) {
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM"
           let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "yyyy-MM-dd"
            
            let today = Date()
            let nextDate = Calendar.current.date(byAdding: .day, value: day, to: startDate)
            let strDate =  dateFormatterPrint.string(from: nextDate!)
            let strAPIDate = dateFormatterAPI.string(from: nextDate!)
    return (strDate,strAPIDate,nextDate!)
}
func getendNextDateMonths(day:Int,stratDate:Date) -> (String,String,Date) {
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM"
           let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "yyyy-MM-dd"
            
            let today = Date()
            let nextDate = Calendar.current.date(byAdding: .month, value: day, to: stratDate)
            let strDate =  dateFormatterPrint.string(from: nextDate!)
            let strAPIDate = dateFormatterAPI.string(from: nextDate!)
    return (strDate,strAPIDate,nextDate!)
}
func DayBetweenDate(startDate: Date, endDate: Date) -> Int {

    let calendar = Calendar.current

    let components = calendar.dateComponents([.day], from: startDate, to: endDate)

    return components.day!
}
func MonthsBetweenDate(startDate: Date, endDate: Date) -> Int {

    let calendar = Calendar.current

    let components = calendar.dateComponents([.month], from: startDate, to: endDate)

    return components.day!
}

func getStaringDate(strDate:String) -> Date{
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = "yyyy-MM-dd"
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "yyyy-MM-dd"

    let date: NSDate? = dateFormatterGet.date(from: strDate) as NSDate?
    return date as! Date
}

func getEndDate(strDate:String) -> Date{
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = "YYYY-MM-DD"
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "YYYY-MM-DD"

    let date: NSDate? = dateFormatterGet.date(from: strDate) as NSDate?
    return date as! Date
}

func carrayOverDateDay(day:Int) -> String {
            let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "dd-MM-yyyy"
            
            let today = Date()
            let nextDate = Calendar.current.date(byAdding: .day, value: day, to: today)
    
           
           // let strAPIDate = dateFormatterAPI.string(from: nextDate!)
            //let startDate = calendarApp.startOfMonth(nextDate!)
           let endDate = calendarApp.endOfMonth(nextDate!)
          
    let strEndDate = dataBaseDate(intDate: nextDate!)

    return strEndDate
}
func carrayOverDateWeeky(day:Int) -> String {
            let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "dd-MM-yyyy"
            
            let today = Date()
            let nextDate = Calendar.current.date(byAdding: .weekday, value: day, to: today)
    
           
           // let strAPIDate = dateFormatterAPI.string(from: nextDate!)
            //let startDate = calendarApp.startOfMonth(nextDate!)
          
          let endDate = calendarApp.endOfWeek(nextDate!)
            let strEndDate = dataBaseDate(intDate: endDate)

    return strEndDate
}

func carrayOverDateMonth(day:Int) -> String {
            let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "dd-MM-yyyy"
            
            let today = Date()
            let nextDate = Calendar.current.date(byAdding: .month, value: day, to: today)
    
           
           // let strAPIDate = dateFormatterAPI.string(from: nextDate!)
            //let startDate = calendarApp.startOfMonth(nextDate!)
             let endDate = calendarApp.endOfMonth(nextDate!)
          
            let strEndDate = dataBaseDate(intDate: endDate)

    return strEndDate
}
func carrayOverDateYearly(day:Int) -> String {
            let dateFormatterAPI = DateFormatter()
              dateFormatterAPI.dateFormat = "dd-MM-yyyy"
            
            let today = Date()
            let nextDate = Calendar.current.date(byAdding: .year, value: day, to: today)
    
           
           // let strAPIDate = dateFormatterAPI.string(from: nextDate!)
            //let startDate = calendarApp.startOfMonth(nextDate!)
           let endDate = calendarApp.endOfYear(nextDate!)
          
            let strEndDate = dataBaseDate(intDate: endDate)

    return strEndDate
}
