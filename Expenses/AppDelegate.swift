//
//  AppDelegate.swift
//  Expenses
//
//  Created by Siddharth Adhvaryu on 13/01/20.
//

import UIKit
import IQKeyboardManagerSwift
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if #available(iOS 13.0, *){
        }
        else {
            
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyboard.instantiateViewController(withIdentifier: "HomeTabBarVC") as! HomeTabBarVC
                (self.window?.rootViewController as? UINavigationController)?.pushViewController(objVC, animated: false)
            }
        DatabaseManager.shared.createDatabse()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    
        if UserDefaults.standard.value(forKey: strCalenderType) != nil {
            
        }else {
            UserDefaults.standard.set(3, forKey: strCalenderType)
        }
        if UserDefaults.standard.value(forKey: strCurrentSortType) != nil {
            
        }else {
            UserDefaults.standard.set(1, forKey: strCurrentSortType)
        }
        if UserDefaults.standard.value(forKey: strCurrentSelectCat) != nil {
           // UserDefaults.standard.set("All Categories", forKey: strCurrentSelectCat)
        }else {
            UserDefaults.standard.set("All Categories", forKey: strCurrentSelectCat)
        }
        if UserDefaults.standard.value(forKey: strCurrentSelectedAcoount) != nil {
            
        }else {
            UserDefaults.standard.set("0", forKey: strCurrentSelectedAcoount)

        }
        if UserDefaults.standard.value(forKey: strCurrencySymbols) != nil {
                   
        }else {
            UserDefaults.standard.set("₹", forKey: strCurrencySymbols)

        }
        // UserDefaults.value(forKey: strAllAccountHide)
        if UserDefaults.standard.value(forKey: strAllAccountHide) != nil {
            
        }else {
            UserDefaults.standard.set(0, forKey: strAllAccountHide)
        }
       // DatabaseManager.shared.deleteTran()
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

